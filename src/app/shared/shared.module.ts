import { NgModule } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import { MenuItemsExpert } from './menu-items/menu-items-expert';

import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';


@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,

   ],
  providers: [ MenuItems, MenuItemsExpert ]
})
export class SharedModule { }
