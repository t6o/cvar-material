import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}
export interface Saperator {
  name: string;
  type?: string;
}
export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  saperator?: Saperator[];
  children?: ChildrenItems[];
}

const MENUITEMS = [

  { state: '',
    name: 'Personal',
    type: 'saperator',
    icon: 'av_timer'
  },{
    state: 'dashboards',
    name: 'Switch Role',
    type: 'sub',
    icon: 'av_timer',
    children: [
      {state: 'dashboard1', name: 'Traveler'},
      {state: 'dashboard2', name: 'Expert'},
    ]
  },
    {
    state: 'material',
    name: 'Find Experts',
    type: 'sub',
    icon: 'bubble_chart',
    badge: [
      {type: 'red', value: '17'}
    ],
    children: [
      {state: 'button', name: 'Map'},

    ]
  },
  {
    state: 'apps',
    name: 'Messages',
    type: 'sub',
    icon: 'apps',
    children: [
      {state: 'chat', name: 'Chat'},

    ]
  },
    {
    state: 'forms',
    name: 'Chatbot',
    type: 'sub',
    icon: 'insert_drive_file',

    children: [
      {state: 'chat', name: 'Chatbot'},


    ]
  }/*,{
    state: 'tables',
    name: 'Tables',
    type: 'sub',
    icon: 'web',

    children: [

      {state: 'basictable', name: 'Basic Table'},
      {state: 'filterable', name: 'Filterable Table'},
      {state: 'pagination', name: 'Pagination Table'},
      {state: 'sortable', name: 'Sortable Table'},
      {state: 'mix', name: 'Mix Table'}

    ]
  },{
    state: 'datatables',
    name: 'Data Tables',
    type: 'sub',
    icon: 'border_all',

    children: [
      {state: 'basicdatatable', name: 'Basic Data Table'},
      {state: 'filter', name: 'Filterable'},
      {state: 'editing', name: 'Editing'},
    ]
  },{
    state: 'widgets',
    name: 'Widgets',
    type: 'link',
    icon: 'widgets'
  },{
    state: '',
    name: 'Extra Component',
    type: 'saperator',
    icon: 'av_timer'
  },
  {
    state: 'charts',
    name: 'Charts',
    type: 'sub',
    icon: 'insert_chart',

    children: [
      {state: 'chartjs', name: 'Chart Js'},
      {state: 'chartistjs', name: 'Chartist Js'},
      {state: 'ngxchart', name: 'Ngx Charts'}

    ]
  },{
    state: 'pages',
    name: 'Pages',
    type: 'sub',
    icon: 'content_copy',

    children: [
      {state: 'icons', name: 'Material Icons'},
      {state: 'timeline', name: 'Timeline'},
      {state: 'invoice', name: 'Invoice'},
      {state: 'pricing', name: 'Pricing'},
      {state: 'helper', name: 'Helper Classes'}
    ]
  }*/

];

@Injectable()

export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }

}
