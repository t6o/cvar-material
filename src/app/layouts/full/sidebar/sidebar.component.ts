import { ChangeDetectorRef, Component, NgZone, OnDestroy, ViewChild, HostListener, Directive, AfterViewInit } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../../shared/menu-items/menu-items';
import { MenuItemsExpert } from '../../../shared/menu-items/menu-items-expert';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: []
})
export class AppSidebarComponent {
  public config: PerfectScrollbarConfigInterface = {};
  mobileQuery: MediaQueryList;
  expert: boolean;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public menuItems: MenuItems, public menuItemsExpert: MenuItemsExpert) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    if(localStorage.getItem("expert") != ""){
      this.expert = localStorage.getItem("expert") == 'true' ? true : false;
    }

  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  getBar(){
    this.expert = localStorage.getItem("expert") == 'true' ? true : false;
    return this.expert;
  }

  roleClick(child) {
    console.log(child);
    if (child.state == "dashboard1") {
      localStorage.setItem("expert", "false");
      this.expert = false;
    } else if (child.state == "dashboard2"){
      this.expert = true;
      localStorage.setItem("expert", "true");
    }
  }

}
