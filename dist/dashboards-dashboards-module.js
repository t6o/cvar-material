(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboards-dashboards-module"],{

/***/ "./src/app/dashboards/dashboard1/dashboard1.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/dashboards/dashboard1/dashboard1.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- Colored Card row-->\n<!-- ============================================================== -->\n<div fxLayoutAlign=\"center center\">\n  <div>\n    <img src=\"assets/images/scimmia.jpg\" name=\"aboutme\" width=\"400\" height=\"400\" border=\"0\" >\n  </div>\n\n</div>\n\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n  <div fxFlex.gt-sm=\"33.33%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-header class=\"text-white\" style=\"background-color:#757677\">\n        <mat-card-title>Cerca un Esperto</mat-card-title>\n      </mat-card-header>\n      <mat-card-content>\n        <p>I nostri esperti ti aiuteranno a trovare le giuste informazioni</p>\n        <p>Contare su una persona in una citta non ha prezzo</p>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex.gt-sm=\"33.33%\">\n    <mat-card>\n      <mat-card-header class=\"text-white\" style=\"background-color:#757677\">\n        <mat-card-title>Diventa un esperto</mat-card-title>\n      </mat-card-header>\n      <mat-card-content>\n        <p>Aiutaci ad ampliare la nostra rete di esperti</p>\n        <p>Se hai vissuto in una citta e conosci tutti i segreti mettiti alla prova</p>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex.gt-sm=\"33.33%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-header class=\"text-white\" style=\"background-color:#757677\">\n        <mat-card-title>Usa il nostro Chatbot</mat-card-title>\n      </mat-card-header>\n      <mat-card-content>\n        <p>Non trovi un esperto vi è April la nostra esperta</p>\n        <p>Piu la usi piu ci aiuti a migliorare il servizio fai le domande e fatti guidare </p>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n\n<!--div fxLayout=\"row\">\n  <div fxFlex.gt-sm=\"100%\">\n    <mat-card>\n      <mat-card-content>\n        <mat-card-title>Card with title and footer <code>&lt;mat-card-title&gt;</code></mat-card-title>\n        <mat-card-subtitle>This is the subtitle <code>&lt;mat-card-subtitle&gt;</code></mat-card-subtitle>\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure\n          dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n\n  <div fxFlex.gt-sm=\"33.33%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-header>\n        <div mat-card-avatar><img src=\"assets/images/users/1.jpg\" class=\"img-fluid img-circle\" /></div>\n        <mat-card-title>Shiba Inu</mat-card-title>\n        <mat-card-subtitle>Dog Breed</mat-card-subtitle>\n      </mat-card-header>\n      <img mat-card-image src=\"assets/images/big/img4.jpg\" alt=\"Photo of a Shiba Inu\">\n      <mat-card-content>\n        <p>\n          The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.\n        </p>\n        <mat-card-actions>\n          <button mat-raised-button color=\"accent\">LIKE</button>\n          <button mat-button>SHARE</button>\n        </mat-card-actions>\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <div fxFlex.gt-sm=\"33.33%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-header>\n        <div mat-card-avatar><img src=\"assets/images/users/2.jpg\" class=\"img-fluid img-circle\" /></div>\n        <mat-card-title>Shiba Inu</mat-card-title>\n        <mat-card-subtitle>Dog Breed</mat-card-subtitle>\n      </mat-card-header>\n      <img mat-card-image src=\"assets/images/big/img3.jpg\" alt=\"Photo of a Shiba Inu\">\n      <mat-card-content>\n        <p>\n          The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.\n        </p>\n\n        <mat-card-actions>\n          <button mat-raised-button color=\"accent\">LIKE</button>\n          <button mat-button>SHARE</button>\n        </mat-card-actions>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex.gt-sm=\"33.33%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-header>\n        <div mat-card-avatar><img src=\"assets/images/users/3.jpg\" class=\"img-fluid img-circle\" /></div>\n        <mat-card-title>Shiba Inu</mat-card-title>\n        <mat-card-subtitle>Dog Breed</mat-card-subtitle>\n      </mat-card-header>\n      <img mat-card-image src=\"assets/images/big/img5.jpg\" alt=\"Photo of a Shiba Inu\">\n      <mat-card-content>\n        <p>\n          The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.\n        </p>\n\n        <mat-card-actions>\n          <button mat-raised-button color=\"accent\">LIKE</button>\n          <button mat-button>SHARE</button>\n        </mat-card-actions>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n\n<div fxLayout=\"row\">\n  <div fxFlex.gt-sm=\"50%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-content>\n        <mat-card-title>\n          <h4 class=\"m-0\">Half width Card</h4></mat-card-title>\n        <mat-card-subtitle>This is the subtitle</mat-card-subtitle>\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex.gt-sm=\"50%\" fxFlex=\"100\">\n    <mat-card>\n      <mat-card-content>\n        <mat-card-title>\n          <h4 class=\"m-0\">Half width Card</h4></mat-card-title>\n        <mat-card-subtitle>This is the subtitle</mat-card-subtitle>\n\n        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div-->\n"

/***/ }),

/***/ "./src/app/dashboards/dashboard1/dashboard1.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/dashboards/dashboard1/dashboard1.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".piechart {\n  height: 300px;\n  width: 280px;\n  margin: 0 auto; }\n"

/***/ }),

/***/ "./src/app/dashboards/dashboard1/dashboard1.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/dashboards/dashboard1/dashboard1.component.ts ***!
  \***************************************************************/
/*! exports provided: Dashboard1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dashboard1Component", function() { return Dashboard1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var data = __webpack_require__(/*! ./data.json */ "./src/app/dashboards/dashboard1/data.json");
var Dashboard1Component = /** @class */ (function () {
    function Dashboard1Component() {
        // Barchart
        this.barChart1 = {
            type: 'Bar',
            data: data['Bar'],
            options: {
                seriesBarDistance: 15,
                high: 12,
                axisX: {
                    showGrid: false, offset: 20
                },
                axisY: {
                    showGrid: true, offset: 40
                }
            },
            responsiveOptions: [
                [
                    'screen and (min-width: 640px)',
                    {
                        axisX: {
                            labelInterpolationFnc: function (value, index) {
                                return index % 1 === 0 ? "" + value : null;
                            }
                        }
                    }
                ]
            ]
        };
        // This is for the donute chart
        this.donuteChart1 = {
            type: 'Pie',
            data: data['Pie'],
            options: {
                donut: true,
                showLabel: false,
                donutWidth: 30
            }
            // events: {
            //   draw(data: any): boolean {
            //     return data;
            //   }
            // }
        };
        // This is for the line chart
        // Line chart
        this.lineChart1 = {
            type: 'Line',
            data: data['LineWithArea'],
            options: {
                low: 0,
                high: 35000,
                showArea: true,
                fullWidth: true
            }
        };
        // Timeline 
        this.mytimelines = [{
                from: 'Nirav joshi',
                time: '(5 minute ago)',
                image: 'assets/images/users/1.jpg',
                attachment: 'assets/images/big/img2.jpg',
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper'
            }, {
                from: 'Sunil joshi',
                time: '(3 minute ago)',
                image: 'assets/images/users/2.jpg',
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper',
                buttons: 'primary'
            }, {
                from: 'Vishal Bhatt',
                time: '(1 minute ago)',
                image: 'assets/images/users/3.jpg',
                attachment: 'assets/images/big/img1.jpg',
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper'
            }, {
                from: 'Dhiren Adesara',
                time: '(1 minute ago)',
                image: 'assets/images/users/4.jpg',
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper',
                buttons: 'warn'
            }];
    }
    Dashboard1Component.prototype.ngAfterViewInit = function () {
        //Sparkline chart
        var sparklineLogin = function () {
            // spark count
            $('.spark-count').sparkline([4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9, 12, 4, 9], {
                type: 'bar',
                width: '100%',
                height: '70',
                barWidth: '2',
                resize: true,
                barSpacing: '6',
                barColor: 'rgba(255, 255, 255, 0.3)'
            });
        };
        var sparkResize;
        $(window).resize(function (e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineLogin, 500);
        });
        sparklineLogin();
    };
    Dashboard1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard1.component.html */ "./src/app/dashboards/dashboard1/dashboard1.component.html"),
            styles: [__webpack_require__(/*! ./dashboard1.component.scss */ "./src/app/dashboards/dashboard1/dashboard1.component.scss")]
        })
    ], Dashboard1Component);
    return Dashboard1Component;
}());



/***/ }),

/***/ "./src/app/dashboards/dashboard1/data.json":
/*!*************************************************!*\
  !*** ./src/app/dashboards/dashboard1/data.json ***!
  \*************************************************/
/*! exports provided: Bar, LineWithArea, Pie, default */
/***/ (function(module) {

module.exports = {"Bar":{"labels":["Jan","Feb","Mar","Apr","May","Jun"],"series":[[9,4,11,7,10,12],[3,2,9,5,8,10]]},"LineWithArea":{"labels":[1,2,3,4,5,6,7,8],"series":[[0,5000,15000,8000,15000,9000,30000,0],[0,3000,5000,2000,8000,1000,5000,0]]},"Pie":{"series":[20,10,30,40]}};

/***/ }),

/***/ "./src/app/dashboards/dashboard2/dashboard2.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/dashboards/dashboard2/dashboard2.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\r\n<!-- Card Group Row -->\r\n<!-- ============================================================== -->\r\n<div class=\"card-group\">\r\n  <!-- column -->    \r\n  \r\n    <mat-card>\r\n      <mat-card-content>\r\n          <!-- column -->\r\n          <mat-icon class=\"text-info\">account_circle</mat-icon>\r\n          <h3 class=\"m-0\">386</h3>\r\n          <h5 class=\"text-muted m-t-0 m-b-10\">New Clients</h5>\r\n          <mat-progress-bar mode=\"determinate\" value=\"40\"></mat-progress-bar>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  \r\n  <!-- column -->\r\n  <!-- column -->    \r\n  \r\n    <mat-card>\r\n      <mat-card-content>\r\n          <!-- column -->\r\n          <mat-icon class=\"text-danger\">local_mall</mat-icon>\r\n          <h3 class=\"m-0\">1386</h3>\r\n          <h5 class=\"text-muted m-t-0 m-b-10\">New Projects</h5>\r\n          <mat-progress-bar color=\"warn\" mode=\"determinate\" value=\"60\"></mat-progress-bar>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  \r\n  <!-- column --> \r\n  <!-- column -->    \r\n  \r\n    <mat-card>\r\n      <mat-card-content>\r\n          <!-- column -->\r\n          <mat-icon class=\"text-purple\">stars</mat-icon>\r\n          <h3 class=\"m-0\">986</h3>\r\n          <h5 class=\"text-muted m-t-0 m-b-10\">New Items</h5>\r\n          <mat-progress-bar color=\"accent\" mode=\"determinate\" value=\"80\"></mat-progress-bar>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  \r\n  <!-- column --> \r\n  <!-- column -->    \r\n  \r\n    <mat-card>\r\n      <mat-card-content>\r\n          <!-- column -->\r\n          <mat-icon class=\"text-info\">content_paste</mat-icon>\r\n          <h3 class=\"m-0\">786</h3>\r\n          <h5 class=\"text-muted m-t-0  m-b-10\">New Invoices</h5>\r\n          <mat-progress-bar mode=\"indeterminate\" value=\"40\"></mat-progress-bar>\r\n      </mat-card-content>\r\n    </mat-card>\r\n  \r\n  <!-- column -->     \r\n</div>\r\n<!-- ============================================================== -->\r\n<!-- Chart boxes -->\r\n<!-- ============================================================== -->\r\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n  <!-- column -->    \r\n  <div fxFlex.gt-md=\"33.33\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n    <div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n        <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n            <mat-card class=\"bg-success\">\r\n              <mat-card-content>\r\n                  <!-- column -->\r\n                  <div class=\"d-flex p-t-20 p-b-20 no-block align-items-center\">\r\n                    <div class=\"stats\">\r\n                        <h3 class=\"text-white m-0\">Sales</h3>\r\n                        <h6 class=\"text-white m-t-0\">March 2018</h6>\r\n                        <h1 class=\"text-white m-0\">35487</h1>\r\n                    </div>\r\n                    <div class=\"spark-count ml-auto\"></div>\r\n                  </div>\r\n              </mat-card-content>\r\n            </mat-card>\r\n        </div>    \r\n    </div>    \r\n    <div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n        <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n            <mat-card class=\"bg-purple\">\r\n              <mat-card-content >\r\n                  <!-- column -->\r\n                  <div class=\"d-flex p-t-20 p-b-20 no-block align-items-center\">\r\n                    <div class=\"stats\">\r\n                        <h3 class=\"text-white m-0\">Purchase</h3>\r\n                        <h6 class=\"text-white m-t-0\">March 2018</h6>\r\n                        <h1 class=\"text-white m-0\">35487</h1>\r\n                    </div>\r\n                    <div class=\"spark-count ml-auto\"></div>\r\n                  </div>\r\n              </mat-card-content>\r\n            </mat-card>\r\n        </div>\r\n      </div>    \r\n  </div>\r\n  <!-- column -->\r\n  <!-- column -->    \r\n  <div fxFlex.gt-md=\"33.33\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n    <mat-card>\r\n      <mat-card-content>\r\n          <!-- column -->\r\n          <mat-card-title>Our Visitors</mat-card-title>\r\n          <mat-card-subtitle>Different Devices Used to Visit</mat-card-subtitle>\r\n          <div class=\"text-center\">\r\n          <canvas baseChart class=\"m-auto\" width=\"230\" height=\"230\" [data]=\"doughnutChartData\" [labels]=\"doughnutChartLabels\" [legend]=\"doughnutChartLegend\" [options]=\"doughnutChartOptions\" [chartType]=\"doughnutChartType\" [colors]=\"[{backgroundColor: ['#1976d2', '#26dad2', '#dadada']}]\"></canvas>\r\n        </div>      \r\n      </mat-card-content><hr>\r\n      <mat-card-content>\r\n            <ul class=\"list-inline text-center\">\r\n                <li><h6 class=\"text-muted text-success m-0\"><i class=\"fa fa-circle font-10 m-r-10 \"></i>Mobile</h6> </li>\r\n                <li><h6 class=\"text-muted  text-info  m-0\"><i class=\"fa fa-circle font-10 m-r-10\"></i>Desktop</h6> </li>\r\n                <li><h6 class=\"text-muted  text-purple  m-0\"><i class=\"fa fa-circle font-10 m-r-10\"></i>Tablet</h6> </li>\r\n            </ul>\r\n          </mat-card-content>    \r\n    </mat-card>\r\n  </div>\r\n  <!-- column --> \r\n  <!-- column -->    \r\n  <div fxFlex.gt-md=\"33.33\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n    <mat-card>\r\n      <mat-card-content>\r\n          <!-- column -->\r\n          <mat-card-title>Sales Yealry</mat-card-title>\r\n          <mat-card-subtitle>This is the simple example of bar chart</mat-card-subtitle>\r\n          <div class=\"barchrt\" style=\"height:325px;\">\r\n          <x-chartist class=\"\" [data]=\"barChart1.data\" [type]=\"barChart1.type\" [options]=\"barChart1.options\" [responsiveOptions]=\"barChart1.responsiveOptions\" [events]=\"barChart1.events\"> </x-chartist>\r\n          </div>      \r\n      </mat-card-content>\r\n    </mat-card>\r\n  </div>\r\n  <!-- column --> \r\n  \r\n  <!-- column -->     \r\n</div>\r\n<!-- ============================================================== -->\r\n<!-- Image and chart -->\r\n<!-- ============================================================== -->\r\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <!-- Column-->    \r\n    <div fxFlex.gt-lg=\"25\" fxFlex.gt-md=\"40\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card class=\"oh text-center little-profile\">\r\n          <img mat-card-image src=\"assets/images/background/profile-bg.jpg\" alt=\"Photo of a Shiba Inu\">\r\n          <mat-card-content>\r\n             <div class=\"pro-img\"><img src=\"assets/images/users/4.jpg\" width=\"100\" alt=\"user\"></div>\r\n             <h3 class=\"m-b-0\">Angela Dominic</h3>\r\n              <h6 class=\"m-t-0 \">Web Designer &amp; Developer</h6>\r\n              <mat-card-actions>\r\n                <button mat-raised-button color=\"warn\">Follow</button>\r\n              </mat-card-actions>\r\n              <div fxLayout=\"row\" fxLayoutWrap=\"wrap\" class=\"m-t-30\">\r\n                  <div fxFlex.gt-sm=\"33.33%\" fxFlex.gt-xs=\"33.33%\" fxFlex=\"100\">\r\n                        <h3 class=\"m-0 font-light\">1099</h3>\r\n                        <small>Articles</small>    \r\n                  </div>\r\n                  <div fxFlex.gt-sm=\"33.33%\" fxFlex.gt-xs=\"33.33%\" fxFlex=\"100\">\r\n                        <h3 class=\"m-0 font-light\">23,469</h3>\r\n                        <small>Followers</small>    \r\n                  </div>\r\n                  <div fxFlex.gt-sm=\"33.33%\" fxFlex.gt-xs=\"33.33%\" fxFlex=\"100\">\r\n                        <h3 class=\"m-0 font-light\">6035</h3>\r\n                        <small>Likes</small>    \r\n                  </div>\r\n              </div>      \r\n          </mat-card-content>      \r\n        </mat-card>\r\n    </div>\r\n    <!-- Column-->    \r\n    <div fxFlex.gt-lg=\"75\" fxFlex.gt-md=\"60\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n          <mat-card-content>\r\n              <mat-card-title>Contact list</mat-card-title>\r\n              <mat-card-subtitle>Overview of Newsletter Campaign</mat-card-subtitle>\r\n              <mat-table #table [dataSource]=\"dataSource\" class=\"contactlist\">\r\n                    <!--- Note that these columns can be defined in any order.\r\n                          The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n                    <!-- Position Column -->\r\n                    <ng-container matColumnDef=\"position\" >\r\n                      <mat-header-cell *matHeaderCellDef> Pic </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> <img src=\"{{element.position}}\" class=\"img-circle\" width=\"30\"/></mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Name Column -->\r\n                    <ng-container matColumnDef=\"name\">\r\n                      <mat-header-cell *matHeaderCellDef> Name </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> <h5 class=\"m-0\">{{element.name}}</h5> {{element.weight}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Weight Column -->\r\n                    <ng-container matColumnDef=\"weight\">\r\n                      <mat-header-cell *matHeaderCellDef> Weight </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.weight}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Symbol Column -->\r\n                    <ng-container matColumnDef=\"designation\">\r\n                      <mat-header-cell *matHeaderCellDef> Designation </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.designation}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n                  </mat-table>\r\n                   <mat-paginator #paginator\r\n                                 [pageSize]=\"5\"\r\n                                 [pageSizeOptions]=\"[5, 10, 20]\">\r\n                  </mat-paginator>\r\n          </mat-card-content>\r\n        </mat-card>\r\n    </div>    \r\n</div> \r\n<!-- ============================================================== -->\r\n<!-- Image Card row-->\r\n<!-- ============================================================== -->\r\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <!-- Card column -->\r\n    <div fxFlex.gt-lg=\"50\" fxFlex.gt-md=\"50\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content>\r\n            <mat-card-title>Recent Comments</mat-card-title>\r\n            <mat-card-subtitle>Latest Comments on users from Material</mat-card-subtitle>\r\n            <div class=\"comment-widgets\">\r\n                <!-- Comment Row -->\r\n                <div class=\"d-flex flex-row comment-row\" *ngFor=\"let mycomment of mycomments\">\r\n                    <div class=\"p-2\"><span class=\"round\"><img src=\"{{mycomment.profile}}\" alt=\"user\" width=\"50\"></span></div>\r\n                    <div class=\"comment-text w-100\">\r\n                        <h5 class=\"m-0\">{{mycomment.name}}</h5>\r\n                        <p class=\"m-b-5\">{{mycomment.content}}</p>\r\n                        <div class=\"comment-footer\"> \r\n                            <span class=\"text-muted pull-right\">{{mycomment.date}}</span> \r\n                            <span class=\"label label-{{mycomment.class}}\">{{mycomment.status}}</span> \r\n                            <span class=\"action-icons\">\r\n                               <a href=\"javascript:void(0)\"><i class=\"ti-pencil-alt\"></i></a>\r\n                               <a href=\"javascript:void(0)\"><i class=\"ti-check\"></i></a>\r\n                               <a href=\"javascript:void(0)\"><i class=\"ti-heart\"></i></a>    \r\n                            </span> \r\n                        </div>\r\n                    </div>\r\n               </div>\r\n            </div>\r\n            </mat-card-content>    \r\n        </mat-card>\r\n    </div>\r\n    <!-- Card column -->\r\n    <!-- Card column -->\r\n    <div fxFlex.gt-lg=\"50\" fxFlex.gt-md=\"50\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content>\r\n                <mat-card-title>Recent Messages</mat-card-title>\r\n                <mat-card-subtitle>Latest Messages on users from Material</mat-card-subtitle>\r\n                <div class=\"mailbox message-widget\">\r\n                    <div class=\"message-center\">\r\n                        <a href=\"#\" *ngFor=\"let mymessage of mymessages\">\r\n                            <div class=\"user-img\"> <img src=\"{{mymessage.useravatar}}\" alt=\"user\" class=\"img-circle\" width=\"40\"> \r\n                                <span class=\"profile-status {{mymessage.status}} pull-right\"></span> \r\n                            </div>\r\n                            <div class=\"mail-contnet\">\r\n                                <h5>{{mymessage.from}}</h5> \r\n                                <span class=\"mail-desc\">{{mymessage.subject}}</span> \r\n                                <span class=\"time\">{{mymessage.time}}</span> \r\n                            </div>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n                \r\n            </mat-card-content>    \r\n        </mat-card>\r\n    </div>    \r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboards/dashboard2/dashboard2.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/dashboards/dashboard2/dashboard2.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".saleschart {\n  width: 100%; }\n\n.contactlist .mat-row {\n  padding: 10px 15px; }\n"

/***/ }),

/***/ "./src/app/dashboards/dashboard2/dashboard2.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/dashboards/dashboard2/dashboard2.component.ts ***!
  \***************************************************************/
/*! exports provided: Dashboard2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dashboard2Component", function() { return Dashboard2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var data = __webpack_require__(/*! ./data.json */ "./src/app/dashboards/dashboard2/data.json");
var Dashboard2Component = /** @class */ (function () {
    function Dashboard2Component() {
        // Barchart
        this.barChart1 = {
            type: 'Bar',
            data: data['Bar'],
            options: {
                seriesBarDistance: 15,
                high: 12,
                axisX: {
                    showGrid: false, offset: 20
                },
                axisY: {
                    showGrid: true, offset: 40
                }
            },
            responsiveOptions: [
                [
                    'screen and (min-width: 640px)',
                    {
                        axisX: {
                            labelInterpolationFnc: function (value, index) {
                                return index % 1 === 0 ? "" + value : null;
                            }
                        }
                    }
                ]
            ]
        };
        // Doughnut
        this.doughnutChartLabels = [
            'Desktop',
            'Mobile',
            'Tablet'
        ];
        this.doughnutChartOptions = {
            responsive: false
        };
        this.doughnutChartData = [350, 450, 100];
        this.doughnutChartType = 'doughnut';
        this.doughnutChartLegend = false;
        // This is for the table
        this.displayedColumns = ['position', 'name', 'weight', 'designation'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
        // This is for the comments
        this.mycomments = [{
                name: 'James Anderson',
                content: 'Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.',
                profile: 'assets/images/users/1.jpg',
                status: 'Pending',
                class: 'info',
                date: 'April 14, 2016'
            }, {
                name: 'Michael Jorden',
                content: 'Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.',
                profile: 'assets/images/users/2.jpg',
                status: 'Approved',
                class: 'light-success',
                date: 'April 14, 2016'
            }, {
                name: 'James Anderson',
                content: 'Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.',
                profile: 'assets/images/users/3.jpg',
                status: 'Pending',
                class: 'danger',
                date: 'April 14, 2016'
            }, {
                name: 'Johnathan Doeting',
                content: 'Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has beenorem Ipsum is simply dummy text of the printing and type setting industry.',
                profile: 'assets/images/users/1.jpg',
                status: 'Pending',
                class: 'info',
                date: 'April 14, 2016'
            }];
        // This is for Mymessages
        this.mymessages = [{
                useravatar: 'assets/images/users/1.jpg',
                status: 'online',
                from: 'Pavan kumar',
                subject: 'Just see the my admin!',
                time: '9:30 AM'
            }, {
                useravatar: 'assets/images/users/2.jpg',
                status: 'busy',
                from: 'Sonu Nigam',
                subject: 'I have sung a song! See you at',
                time: '9:10 AM'
            }, {
                useravatar: 'assets/images/users/3.jpg',
                status: 'away',
                from: 'Arijit Sinh',
                subject: 'I am a singer!',
                time: '9:08 AM'
            }, {
                useravatar: 'assets/images/users/4.jpg',
                status: 'busy',
                from: 'Sonu Nigam',
                subject: 'I have sung a song! See you at',
                time: '9:10 AM'
            }, {
                useravatar: 'assets/images/users/6.jpg',
                status: 'away',
                from: 'Arijit Sinh',
                subject: 'I am a singer!',
                time: '9:08 AM'
            }, {
                useravatar: 'assets/images/users/7.jpg',
                status: 'busy',
                from: 'Sonu Nigam',
                subject: 'I have sung a song! See you at',
                time: '9:10 AM'
            }, {
                useravatar: 'assets/images/users/8.jpg',
                status: 'away',
                from: 'Arijit Sinh',
                subject: 'I am a singer!',
                time: '9:08 AM'
            }, {
                useravatar: 'assets/images/users/6.jpg',
                status: 'offline',
                from: 'Pavan kumar',
                subject: 'Just see the my admin!',
                time: '9:00 AM'
            }];
    }
    Dashboard2Component.prototype.ngAfterViewInit = function () {
        //Sparkline chart
        var sparklineLogin = function () {
            // spark count
            $('.spark-count').sparkline([4, 5, 0, 10, 9, 12, 4, 9, 4, 5, 3, 10, 9, 12, 10, 9, 12, 4, 9], {
                type: 'bar',
                width: '100%',
                height: '70',
                barWidth: '2',
                resize: true,
                barSpacing: '6',
                barColor: 'rgba(255, 255, 255, 0.3)'
            });
        };
        var sparkResize;
        $(window).resize(function (e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineLogin, 500);
        });
        sparklineLogin();
        /**
      * Set the paginator after the view init since this component will
      * be able to query its view for the initialized paginator.
      */
        this.dataSource.paginator = this.paginator;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], Dashboard2Component.prototype, "paginator", void 0);
    Dashboard2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard2',
            template: __webpack_require__(/*! ./dashboard2.component.html */ "./src/app/dashboards/dashboard2/dashboard2.component.html"),
            styles: [__webpack_require__(/*! ./dashboard2.component.scss */ "./src/app/dashboards/dashboard2/dashboard2.component.scss")],
        })
    ], Dashboard2Component);
    return Dashboard2Component;
}());

var ELEMENT_DATA = [
    { position: 'assets/images/users/1.jpg', name: 'Nirav joshi', weight: 1.0079, designation: 'H' },
    { position: 'assets/images/users/2.jpg', name: 'Sunil joshi', weight: 4.0026, designation: 'He' },
    { position: 'assets/images/users/3.jpg', name: 'Vishal Bhatt', weight: 6.941, designation: 'Li' },
    { position: 'assets/images/users/4.jpg', name: 'Beryllium Lon', weight: 9.0122, designation: 'Be' },
    { position: 'assets/images/users/5.jpg', name: 'Boron son', weight: 10.811, designation: 'B' },
    { position: 'assets/images/users/6.jpg', name: 'Carbon hryt', weight: 12.0107, designation: 'C' },
    { position: 'assets/images/users/7.jpg', name: 'Nitro oxur', weight: 14.0067, designation: 'N' },
    { position: 'assets/images/users/8.jpg', name: 'Oxyg rigch', weight: 15.9994, designation: 'O' },
];


/***/ }),

/***/ "./src/app/dashboards/dashboard2/data.json":
/*!*************************************************!*\
  !*** ./src/app/dashboards/dashboard2/data.json ***!
  \*************************************************/
/*! exports provided: Bar, LineWithArea, Pie, default */
/***/ (function(module) {

module.exports = {"Bar":{"labels":["Jan","Feb","Mar","Apr","May","Jun"],"series":[[9,4,11,7,10,12],[3,2,9,5,8,10]]},"LineWithArea":{"labels":[1,2,3,4,5,6,7,8],"series":[[0,5000,15000,8000,15000,9000,30000,0],[0,3000,5000,2000,8000,1000,5000,0]]},"Pie":{"series":[20,10,30,40]}};

/***/ }),

/***/ "./src/app/dashboards/dashboards.module.ts":
/*!*************************************************!*\
  !*** ./src/app/dashboards/dashboards.module.ts ***!
  \*************************************************/
/*! exports provided: DashboardsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardsModule", function() { return DashboardsModule; });
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _demo_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../demo-material-module */ "./src/app/demo-material-module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _dashboards_routing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboards.routing */ "./src/app/dashboards/dashboards.routing.ts");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-chartist */ "./node_modules/ng-chartist/dist/ng-chartist.js");
/* harmony import */ var ng_chartist__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng_chartist__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./dashboard1/dashboard1.component */ "./src/app/dashboards/dashboard1/dashboard1.component.ts");
/* harmony import */ var _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./dashboard2/dashboard2.component */ "./src/app/dashboards/dashboard2/dashboard2.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var DashboardsModule = /** @class */ (function () {
    function DashboardsModule() {
    }
    DashboardsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _demo_material_module__WEBPACK_IMPORTED_MODULE_4__["DemoMaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                ng_chartist__WEBPACK_IMPORTED_MODULE_7__["ChartistModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_8__["ChartsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_dashboards_routing__WEBPACK_IMPORTED_MODULE_6__["DashboardsRoutes"])
            ],
            declarations: [_dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_9__["Dashboard1Component"], _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_10__["Dashboard2Component"]]
        })
    ], DashboardsModule);
    return DashboardsModule;
}());



/***/ }),

/***/ "./src/app/dashboards/dashboards.routing.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboards/dashboards.routing.ts ***!
  \**************************************************/
/*! exports provided: DashboardsRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardsRoutes", function() { return DashboardsRoutes; });
/* harmony import */ var _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard1/dashboard1.component */ "./src/app/dashboards/dashboard1/dashboard1.component.ts");
/* harmony import */ var _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard2/dashboard2.component */ "./src/app/dashboards/dashboard2/dashboard2.component.ts");


var DashboardsRoutes = [
    {
        path: '',
        children: [{
                path: 'dashboard1',
                component: _dashboard1_dashboard1_component__WEBPACK_IMPORTED_MODULE_0__["Dashboard1Component"]
            }, {
                path: 'dashboard2',
                component: _dashboard2_dashboard2_component__WEBPACK_IMPORTED_MODULE_1__["Dashboard2Component"]
            }]
    }
];


/***/ })

}]);
//# sourceMappingURL=dashboards-dashboards-module.js.map