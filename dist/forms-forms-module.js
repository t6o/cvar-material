(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["forms-forms-module"],{

/***/ "./node_modules/ng2-file-upload/file-upload/file-drop.directive.js":
/*!*************************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-drop.directive.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var file_uploader_class_1 = __webpack_require__(/*! ./file-uploader.class */ "./node_modules/ng2-file-upload/file-upload/file-uploader.class.js");
var FileDropDirective = (function () {
    function FileDropDirective(element) {
        this.fileOver = new core_1.EventEmitter();
        this.onFileDrop = new core_1.EventEmitter();
        this.element = element;
    }
    FileDropDirective.prototype.getOptions = function () {
        return this.uploader.options;
    };
    FileDropDirective.prototype.getFilters = function () {
        return {};
    };
    FileDropDirective.prototype.onDrop = function (event) {
        var transfer = this._getTransfer(event);
        if (!transfer) {
            return;
        }
        var options = this.getOptions();
        var filters = this.getFilters();
        this._preventAndStop(event);
        this.uploader.addToQueue(transfer.files, options, filters);
        this.fileOver.emit(false);
        this.onFileDrop.emit(transfer.files);
    };
    FileDropDirective.prototype.onDragOver = function (event) {
        var transfer = this._getTransfer(event);
        if (!this._haveFiles(transfer.types)) {
            return;
        }
        transfer.dropEffect = 'copy';
        this._preventAndStop(event);
        this.fileOver.emit(true);
    };
    FileDropDirective.prototype.onDragLeave = function (event) {
        if (this.element) {
            if (event.currentTarget === this.element[0]) {
                return;
            }
        }
        this._preventAndStop(event);
        this.fileOver.emit(false);
    };
    FileDropDirective.prototype._getTransfer = function (event) {
        return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer; // jQuery fix;
    };
    FileDropDirective.prototype._preventAndStop = function (event) {
        event.preventDefault();
        event.stopPropagation();
    };
    FileDropDirective.prototype._haveFiles = function (types) {
        if (!types) {
            return false;
        }
        if (types.indexOf) {
            return types.indexOf('Files') !== -1;
        }
        else if (types.contains) {
            return types.contains('Files');
        }
        else {
            return false;
        }
    };
    return FileDropDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", file_uploader_class_1.FileUploader)
], FileDropDirective.prototype, "uploader", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], FileDropDirective.prototype, "fileOver", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], FileDropDirective.prototype, "onFileDrop", void 0);
__decorate([
    core_1.HostListener('drop', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], FileDropDirective.prototype, "onDrop", null);
__decorate([
    core_1.HostListener('dragover', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], FileDropDirective.prototype, "onDragOver", null);
__decorate([
    core_1.HostListener('dragleave', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], FileDropDirective.prototype, "onDragLeave", null);
FileDropDirective = __decorate([
    core_1.Directive({ selector: '[ng2FileDrop]' }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], FileDropDirective);
exports.FileDropDirective = FileDropDirective;


/***/ }),

/***/ "./node_modules/ng2-file-upload/file-upload/file-item.class.js":
/*!*********************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-item.class.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var file_like_object_class_1 = __webpack_require__(/*! ./file-like-object.class */ "./node_modules/ng2-file-upload/file-upload/file-like-object.class.js");
var FileItem = (function () {
    function FileItem(uploader, some, options) {
        this.url = '/';
        this.headers = [];
        this.withCredentials = true;
        this.formData = [];
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = false;
        this.isSuccess = false;
        this.isCancel = false;
        this.isError = false;
        this.progress = 0;
        this.index = void 0;
        this.uploader = uploader;
        this.some = some;
        this.options = options;
        this.file = new file_like_object_class_1.FileLikeObject(some);
        this._file = some;
        if (uploader.options) {
            this.method = uploader.options.method || 'POST';
            this.alias = uploader.options.itemAlias || 'file';
        }
        this.url = uploader.options.url;
    }
    FileItem.prototype.upload = function () {
        try {
            this.uploader.uploadItem(this);
        }
        catch (e) {
            this.uploader._onCompleteItem(this, '', 0, {});
            this.uploader._onErrorItem(this, '', 0, {});
        }
    };
    FileItem.prototype.cancel = function () {
        this.uploader.cancelItem(this);
    };
    FileItem.prototype.remove = function () {
        this.uploader.removeFromQueue(this);
    };
    FileItem.prototype.onBeforeUpload = function () {
        return void 0;
    };
    FileItem.prototype.onBuildForm = function (form) {
        return { form: form };
    };
    FileItem.prototype.onProgress = function (progress) {
        return { progress: progress };
    };
    FileItem.prototype.onSuccess = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype.onError = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype.onCancel = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype.onComplete = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype._onBeforeUpload = function () {
        this.isReady = true;
        this.isUploading = true;
        this.isUploaded = false;
        this.isSuccess = false;
        this.isCancel = false;
        this.isError = false;
        this.progress = 0;
        this.onBeforeUpload();
    };
    FileItem.prototype._onBuildForm = function (form) {
        this.onBuildForm(form);
    };
    FileItem.prototype._onProgress = function (progress) {
        this.progress = progress;
        this.onProgress(progress);
    };
    FileItem.prototype._onSuccess = function (response, status, headers) {
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = true;
        this.isSuccess = true;
        this.isCancel = false;
        this.isError = false;
        this.progress = 100;
        this.index = void 0;
        this.onSuccess(response, status, headers);
    };
    FileItem.prototype._onError = function (response, status, headers) {
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = true;
        this.isSuccess = false;
        this.isCancel = false;
        this.isError = true;
        this.progress = 0;
        this.index = void 0;
        this.onError(response, status, headers);
    };
    FileItem.prototype._onCancel = function (response, status, headers) {
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = false;
        this.isSuccess = false;
        this.isCancel = true;
        this.isError = false;
        this.progress = 0;
        this.index = void 0;
        this.onCancel(response, status, headers);
    };
    FileItem.prototype._onComplete = function (response, status, headers) {
        this.onComplete(response, status, headers);
        if (this.uploader.options.removeAfterUpload) {
            this.remove();
        }
    };
    FileItem.prototype._prepareToUploading = function () {
        this.index = this.index || ++this.uploader._nextIndex;
        this.isReady = true;
    };
    return FileItem;
}());
exports.FileItem = FileItem;


/***/ }),

/***/ "./node_modules/ng2-file-upload/file-upload/file-like-object.class.js":
/*!****************************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-like-object.class.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function isElement(node) {
    return !!(node && (node.nodeName || node.prop && node.attr && node.find));
}
var FileLikeObject = (function () {
    function FileLikeObject(fileOrInput) {
        this.rawFile = fileOrInput;
        var isInput = isElement(fileOrInput);
        var fakePathOrObject = isInput ? fileOrInput.value : fileOrInput;
        var postfix = typeof fakePathOrObject === 'string' ? 'FakePath' : 'Object';
        var method = '_createFrom' + postfix;
        this[method](fakePathOrObject);
    }
    FileLikeObject.prototype._createFromFakePath = function (path) {
        this.lastModifiedDate = void 0;
        this.size = void 0;
        this.type = 'like/' + path.slice(path.lastIndexOf('.') + 1).toLowerCase();
        this.name = path.slice(path.lastIndexOf('/') + path.lastIndexOf('\\') + 2);
    };
    FileLikeObject.prototype._createFromObject = function (object) {
        this.size = object.size;
        this.type = object.type;
        this.name = object.name;
    };
    return FileLikeObject;
}());
exports.FileLikeObject = FileLikeObject;


/***/ }),

/***/ "./node_modules/ng2-file-upload/file-upload/file-select.directive.js":
/*!***************************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-select.directive.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var file_uploader_class_1 = __webpack_require__(/*! ./file-uploader.class */ "./node_modules/ng2-file-upload/file-upload/file-uploader.class.js");
var FileSelectDirective = (function () {
    function FileSelectDirective(element) {
        this.onFileSelected = new core_1.EventEmitter();
        this.element = element;
    }
    FileSelectDirective.prototype.getOptions = function () {
        return this.uploader.options;
    };
    FileSelectDirective.prototype.getFilters = function () {
        return {};
    };
    FileSelectDirective.prototype.isEmptyAfterSelection = function () {
        return !!this.element.nativeElement.attributes.multiple;
    };
    FileSelectDirective.prototype.onChange = function () {
        var files = this.element.nativeElement.files;
        var options = this.getOptions();
        var filters = this.getFilters();
        this.uploader.addToQueue(files, options, filters);
        this.onFileSelected.emit(files);
        if (this.isEmptyAfterSelection()) {
            this.element.nativeElement.value = '';
        }
    };
    return FileSelectDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", file_uploader_class_1.FileUploader)
], FileSelectDirective.prototype, "uploader", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], FileSelectDirective.prototype, "onFileSelected", void 0);
__decorate([
    core_1.HostListener('change'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], FileSelectDirective.prototype, "onChange", null);
FileSelectDirective = __decorate([
    core_1.Directive({ selector: '[ng2FileSelect]' }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], FileSelectDirective);
exports.FileSelectDirective = FileSelectDirective;


/***/ }),

/***/ "./node_modules/ng2-file-upload/file-upload/file-type.class.js":
/*!*********************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-type.class.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var FileType = (function () {
    function FileType() {
    }
    FileType.getMimeClass = function (file) {
        var mimeClass = 'application';
        if (this.mime_psd.indexOf(file.type) !== -1) {
            mimeClass = 'image';
        }
        else if (file.type.match('image.*')) {
            mimeClass = 'image';
        }
        else if (file.type.match('video.*')) {
            mimeClass = 'video';
        }
        else if (file.type.match('audio.*')) {
            mimeClass = 'audio';
        }
        else if (file.type === 'application/pdf') {
            mimeClass = 'pdf';
        }
        else if (this.mime_compress.indexOf(file.type) !== -1) {
            mimeClass = 'compress';
        }
        else if (this.mime_doc.indexOf(file.type) !== -1) {
            mimeClass = 'doc';
        }
        else if (this.mime_xsl.indexOf(file.type) !== -1) {
            mimeClass = 'xls';
        }
        else if (this.mime_ppt.indexOf(file.type) !== -1) {
            mimeClass = 'ppt';
        }
        if (mimeClass === 'application') {
            mimeClass = this.fileTypeDetection(file.name);
        }
        return mimeClass;
    };
    FileType.fileTypeDetection = function (inputFilename) {
        var types = {
            'jpg': 'image',
            'jpeg': 'image',
            'tif': 'image',
            'psd': 'image',
            'bmp': 'image',
            'png': 'image',
            'nef': 'image',
            'tiff': 'image',
            'cr2': 'image',
            'dwg': 'image',
            'cdr': 'image',
            'ai': 'image',
            'indd': 'image',
            'pin': 'image',
            'cdp': 'image',
            'skp': 'image',
            'stp': 'image',
            '3dm': 'image',
            'mp3': 'audio',
            'wav': 'audio',
            'wma': 'audio',
            'mod': 'audio',
            'm4a': 'audio',
            'compress': 'compress',
            'zip': 'compress',
            'rar': 'compress',
            '7z': 'compress',
            'lz': 'compress',
            'z01': 'compress',
            'pdf': 'pdf',
            'xls': 'xls',
            'xlsx': 'xls',
            'ods': 'xls',
            'mp4': 'video',
            'avi': 'video',
            'wmv': 'video',
            'mpg': 'video',
            'mts': 'video',
            'flv': 'video',
            '3gp': 'video',
            'vob': 'video',
            'm4v': 'video',
            'mpeg': 'video',
            'm2ts': 'video',
            'mov': 'video',
            'doc': 'doc',
            'docx': 'doc',
            'eps': 'doc',
            'txt': 'doc',
            'odt': 'doc',
            'rtf': 'doc',
            'ppt': 'ppt',
            'pptx': 'ppt',
            'pps': 'ppt',
            'ppsx': 'ppt',
            'odp': 'ppt'
        };
        var chunks = inputFilename.split('.');
        if (chunks.length < 2) {
            return 'application';
        }
        var extension = chunks[chunks.length - 1].toLowerCase();
        if (types[extension] === undefined) {
            return 'application';
        }
        else {
            return types[extension];
        }
    };
    return FileType;
}());
/*  MS office  */
FileType.mime_doc = [
    'application/msword',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    'application/vnd.ms-word.document.macroEnabled.12',
    'application/vnd.ms-word.template.macroEnabled.12'
];
FileType.mime_xsl = [
    'application/vnd.ms-excel',
    'application/vnd.ms-excel',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
    'application/vnd.ms-excel.sheet.macroEnabled.12',
    'application/vnd.ms-excel.template.macroEnabled.12',
    'application/vnd.ms-excel.addin.macroEnabled.12',
    'application/vnd.ms-excel.sheet.binary.macroEnabled.12'
];
FileType.mime_ppt = [
    'application/vnd.ms-powerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.openxmlformats-officedocument.presentationml.template',
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    'application/vnd.ms-powerpoint.addin.macroEnabled.12',
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'
];
/* PSD */
FileType.mime_psd = [
    'image/photoshop',
    'image/x-photoshop',
    'image/psd',
    'application/photoshop',
    'application/psd',
    'zz-application/zz-winassoc-psd'
];
/* Compressed files */
FileType.mime_compress = [
    'application/x-gtar',
    'application/x-gcompress',
    'application/compress',
    'application/x-tar',
    'application/x-rar-compressed',
    'application/octet-stream'
];
exports.FileType = FileType;


/***/ }),

/***/ "./node_modules/ng2-file-upload/file-upload/file-upload.module.js":
/*!************************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-upload.module.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var file_drop_directive_1 = __webpack_require__(/*! ./file-drop.directive */ "./node_modules/ng2-file-upload/file-upload/file-drop.directive.js");
var file_select_directive_1 = __webpack_require__(/*! ./file-select.directive */ "./node_modules/ng2-file-upload/file-upload/file-select.directive.js");
var FileUploadModule = (function () {
    function FileUploadModule() {
    }
    return FileUploadModule;
}());
FileUploadModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule],
        declarations: [file_drop_directive_1.FileDropDirective, file_select_directive_1.FileSelectDirective],
        exports: [file_drop_directive_1.FileDropDirective, file_select_directive_1.FileSelectDirective]
    })
], FileUploadModule);
exports.FileUploadModule = FileUploadModule;


/***/ }),

/***/ "./node_modules/ng2-file-upload/file-upload/file-uploader.class.js":
/*!*************************************************************************!*\
  !*** ./node_modules/ng2-file-upload/file-upload/file-uploader.class.js ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var file_like_object_class_1 = __webpack_require__(/*! ./file-like-object.class */ "./node_modules/ng2-file-upload/file-upload/file-like-object.class.js");
var file_item_class_1 = __webpack_require__(/*! ./file-item.class */ "./node_modules/ng2-file-upload/file-upload/file-item.class.js");
var file_type_class_1 = __webpack_require__(/*! ./file-type.class */ "./node_modules/ng2-file-upload/file-upload/file-type.class.js");
function isFile(value) {
    return (File && value instanceof File);
}
var FileUploader = (function () {
    function FileUploader(options) {
        this.isUploading = false;
        this.queue = [];
        this.progress = 0;
        this._nextIndex = 0;
        this.options = {
            autoUpload: false,
            isHTML5: true,
            filters: [],
            removeAfterUpload: false,
            disableMultipart: false,
            formatDataFunction: function (item) { return item._file; },
            formatDataFunctionIsAsync: false
        };
        this.setOptions(options);
        this.response = new core_1.EventEmitter();
    }
    FileUploader.prototype.setOptions = function (options) {
        this.options = Object.assign(this.options, options);
        this.authToken = this.options.authToken;
        this.authTokenHeader = this.options.authTokenHeader || 'Authorization';
        this.autoUpload = this.options.autoUpload;
        this.options.filters.unshift({ name: 'queueLimit', fn: this._queueLimitFilter });
        if (this.options.maxFileSize) {
            this.options.filters.unshift({ name: 'fileSize', fn: this._fileSizeFilter });
        }
        if (this.options.allowedFileType) {
            this.options.filters.unshift({ name: 'fileType', fn: this._fileTypeFilter });
        }
        if (this.options.allowedMimeType) {
            this.options.filters.unshift({ name: 'mimeType', fn: this._mimeTypeFilter });
        }
        for (var i = 0; i < this.queue.length; i++) {
            this.queue[i].url = this.options.url;
        }
    };
    FileUploader.prototype.addToQueue = function (files, options, filters) {
        var _this = this;
        var list = [];
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            list.push(file);
        }
        var arrayOfFilters = this._getFilters(filters);
        var count = this.queue.length;
        var addedFileItems = [];
        list.map(function (some) {
            if (!options) {
                options = _this.options;
            }
            var temp = new file_like_object_class_1.FileLikeObject(some);
            if (_this._isValidFile(temp, arrayOfFilters, options)) {
                var fileItem = new file_item_class_1.FileItem(_this, some, options);
                addedFileItems.push(fileItem);
                _this.queue.push(fileItem);
                _this._onAfterAddingFile(fileItem);
            }
            else {
                var filter = arrayOfFilters[_this._failFilterIndex];
                _this._onWhenAddingFileFailed(temp, filter, options);
            }
        });
        if (this.queue.length !== count) {
            this._onAfterAddingAll(addedFileItems);
            this.progress = this._getTotalProgress();
        }
        this._render();
        if (this.options.autoUpload) {
            this.uploadAll();
        }
    };
    FileUploader.prototype.removeFromQueue = function (value) {
        var index = this.getIndexOfItem(value);
        var item = this.queue[index];
        if (item.isUploading) {
            item.cancel();
        }
        this.queue.splice(index, 1);
        this.progress = this._getTotalProgress();
    };
    FileUploader.prototype.clearQueue = function () {
        while (this.queue.length) {
            this.queue[0].remove();
        }
        this.progress = 0;
    };
    FileUploader.prototype.uploadItem = function (value) {
        var index = this.getIndexOfItem(value);
        var item = this.queue[index];
        var transport = this.options.isHTML5 ? '_xhrTransport' : '_iframeTransport';
        item._prepareToUploading();
        if (this.isUploading) {
            return;
        }
        this.isUploading = true;
        this[transport](item);
    };
    FileUploader.prototype.cancelItem = function (value) {
        var index = this.getIndexOfItem(value);
        var item = this.queue[index];
        var prop = this.options.isHTML5 ? item._xhr : item._form;
        if (item && item.isUploading) {
            prop.abort();
        }
    };
    FileUploader.prototype.uploadAll = function () {
        var items = this.getNotUploadedItems().filter(function (item) { return !item.isUploading; });
        if (!items.length) {
            return;
        }
        items.map(function (item) { return item._prepareToUploading(); });
        items[0].upload();
    };
    FileUploader.prototype.cancelAll = function () {
        var items = this.getNotUploadedItems();
        items.map(function (item) { return item.cancel(); });
    };
    FileUploader.prototype.isFile = function (value) {
        return isFile(value);
    };
    FileUploader.prototype.isFileLikeObject = function (value) {
        return value instanceof file_like_object_class_1.FileLikeObject;
    };
    FileUploader.prototype.getIndexOfItem = function (value) {
        return typeof value === 'number' ? value : this.queue.indexOf(value);
    };
    FileUploader.prototype.getNotUploadedItems = function () {
        return this.queue.filter(function (item) { return !item.isUploaded; });
    };
    FileUploader.prototype.getReadyItems = function () {
        return this.queue
            .filter(function (item) { return (item.isReady && !item.isUploading); })
            .sort(function (item1, item2) { return item1.index - item2.index; });
    };
    FileUploader.prototype.destroy = function () {
        return void 0;
    };
    FileUploader.prototype.onAfterAddingAll = function (fileItems) {
        return { fileItems: fileItems };
    };
    FileUploader.prototype.onBuildItemForm = function (fileItem, form) {
        return { fileItem: fileItem, form: form };
    };
    FileUploader.prototype.onAfterAddingFile = function (fileItem) {
        return { fileItem: fileItem };
    };
    FileUploader.prototype.onWhenAddingFileFailed = function (item, filter, options) {
        return { item: item, filter: filter, options: options };
    };
    FileUploader.prototype.onBeforeUploadItem = function (fileItem) {
        return { fileItem: fileItem };
    };
    FileUploader.prototype.onProgressItem = function (fileItem, progress) {
        return { fileItem: fileItem, progress: progress };
    };
    FileUploader.prototype.onProgressAll = function (progress) {
        return { progress: progress };
    };
    FileUploader.prototype.onSuccessItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onErrorItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onCancelItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onCompleteItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onCompleteAll = function () {
        return void 0;
    };
    FileUploader.prototype._mimeTypeFilter = function (item) {
        return !(this.options.allowedMimeType && this.options.allowedMimeType.indexOf(item.type) === -1);
    };
    FileUploader.prototype._fileSizeFilter = function (item) {
        return !(this.options.maxFileSize && item.size > this.options.maxFileSize);
    };
    FileUploader.prototype._fileTypeFilter = function (item) {
        return !(this.options.allowedFileType &&
            this.options.allowedFileType.indexOf(file_type_class_1.FileType.getMimeClass(item)) === -1);
    };
    FileUploader.prototype._onErrorItem = function (item, response, status, headers) {
        item._onError(response, status, headers);
        this.onErrorItem(item, response, status, headers);
    };
    FileUploader.prototype._onCompleteItem = function (item, response, status, headers) {
        item._onComplete(response, status, headers);
        this.onCompleteItem(item, response, status, headers);
        var nextItem = this.getReadyItems()[0];
        this.isUploading = false;
        if (nextItem) {
            nextItem.upload();
            return;
        }
        this.onCompleteAll();
        this.progress = this._getTotalProgress();
        this._render();
    };
    FileUploader.prototype._headersGetter = function (parsedHeaders) {
        return function (name) {
            if (name) {
                return parsedHeaders[name.toLowerCase()] || void 0;
            }
            return parsedHeaders;
        };
    };
    FileUploader.prototype._xhrTransport = function (item) {
        var _this = this;
        var that = this;
        var xhr = item._xhr = new XMLHttpRequest();
        var sendable;
        this._onBeforeUploadItem(item);
        if (typeof item._file.size !== 'number') {
            throw new TypeError('The file specified is no longer valid');
        }
        if (!this.options.disableMultipart) {
            sendable = new FormData();
            this._onBuildItemForm(item, sendable);
            var appendFile = function () { return sendable.append(item.alias, item._file, item.file.name); };
            if (!this.options.parametersBeforeFiles) {
                appendFile();
            }
            // For AWS, Additional Parameters must come BEFORE Files
            if (this.options.additionalParameter !== undefined) {
                Object.keys(this.options.additionalParameter).forEach(function (key) {
                    var paramVal = _this.options.additionalParameter[key];
                    // Allow an additional parameter to include the filename
                    if (typeof paramVal === 'string' && paramVal.indexOf('{{file_name}}') >= 0) {
                        paramVal = paramVal.replace('{{file_name}}', item.file.name);
                    }
                    sendable.append(key, paramVal);
                });
            }
            if (this.options.parametersBeforeFiles) {
                appendFile();
            }
        }
        else {
            sendable = this.options.formatDataFunction(item);
        }
        xhr.upload.onprogress = function (event) {
            var progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
            _this._onProgressItem(item, progress);
        };
        xhr.onload = function () {
            var headers = _this._parseHeaders(xhr.getAllResponseHeaders());
            var response = _this._transformResponse(xhr.response, headers);
            var gist = _this._isSuccessCode(xhr.status) ? 'Success' : 'Error';
            var method = '_on' + gist + 'Item';
            _this[method](item, response, xhr.status, headers);
            _this._onCompleteItem(item, response, xhr.status, headers);
        };
        xhr.onerror = function () {
            var headers = _this._parseHeaders(xhr.getAllResponseHeaders());
            var response = _this._transformResponse(xhr.response, headers);
            _this._onErrorItem(item, response, xhr.status, headers);
            _this._onCompleteItem(item, response, xhr.status, headers);
        };
        xhr.onabort = function () {
            var headers = _this._parseHeaders(xhr.getAllResponseHeaders());
            var response = _this._transformResponse(xhr.response, headers);
            _this._onCancelItem(item, response, xhr.status, headers);
            _this._onCompleteItem(item, response, xhr.status, headers);
        };
        xhr.open(item.method, item.url, true);
        xhr.withCredentials = item.withCredentials;
        if (this.options.headers) {
            for (var _i = 0, _a = this.options.headers; _i < _a.length; _i++) {
                var header = _a[_i];
                xhr.setRequestHeader(header.name, header.value);
            }
        }
        if (item.headers.length) {
            for (var _b = 0, _c = item.headers; _b < _c.length; _b++) {
                var header = _c[_b];
                xhr.setRequestHeader(header.name, header.value);
            }
        }
        if (this.authToken) {
            xhr.setRequestHeader(this.authTokenHeader, this.authToken);
        }
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                that.response.emit(xhr.responseText);
            }
        };
        if (this.options.formatDataFunctionIsAsync) {
            sendable.then(function (result) { return xhr.send(JSON.stringify(result)); });
        }
        else {
            xhr.send(sendable);
        }
        this._render();
    };
    FileUploader.prototype._getTotalProgress = function (value) {
        if (value === void 0) { value = 0; }
        if (this.options.removeAfterUpload) {
            return value;
        }
        var notUploaded = this.getNotUploadedItems().length;
        var uploaded = notUploaded ? this.queue.length - notUploaded : this.queue.length;
        var ratio = 100 / this.queue.length;
        var current = value * ratio / 100;
        return Math.round(uploaded * ratio + current);
    };
    FileUploader.prototype._getFilters = function (filters) {
        if (!filters) {
            return this.options.filters;
        }
        if (Array.isArray(filters)) {
            return filters;
        }
        if (typeof filters === 'string') {
            var names_1 = filters.match(/[^\s,]+/g);
            return this.options.filters
                .filter(function (filter) { return names_1.indexOf(filter.name) !== -1; });
        }
        return this.options.filters;
    };
    FileUploader.prototype._render = function () {
        return void 0;
    };
    FileUploader.prototype._queueLimitFilter = function () {
        return this.options.queueLimit === undefined || this.queue.length < this.options.queueLimit;
    };
    FileUploader.prototype._isValidFile = function (file, filters, options) {
        var _this = this;
        this._failFilterIndex = -1;
        return !filters.length ? true : filters.every(function (filter) {
            _this._failFilterIndex++;
            return filter.fn.call(_this, file, options);
        });
    };
    FileUploader.prototype._isSuccessCode = function (status) {
        return (status >= 200 && status < 300) || status === 304;
    };
    FileUploader.prototype._transformResponse = function (response, headers) {
        return response;
    };
    FileUploader.prototype._parseHeaders = function (headers) {
        var parsed = {};
        var key;
        var val;
        var i;
        if (!headers) {
            return parsed;
        }
        headers.split('\n').map(function (line) {
            i = line.indexOf(':');
            key = line.slice(0, i).trim().toLowerCase();
            val = line.slice(i + 1).trim();
            if (key) {
                parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
            }
        });
        return parsed;
    };
    FileUploader.prototype._onWhenAddingFileFailed = function (item, filter, options) {
        this.onWhenAddingFileFailed(item, filter, options);
    };
    FileUploader.prototype._onAfterAddingFile = function (item) {
        this.onAfterAddingFile(item);
    };
    FileUploader.prototype._onAfterAddingAll = function (items) {
        this.onAfterAddingAll(items);
    };
    FileUploader.prototype._onBeforeUploadItem = function (item) {
        item._onBeforeUpload();
        this.onBeforeUploadItem(item);
    };
    FileUploader.prototype._onBuildItemForm = function (item, form) {
        item._onBuildForm(form);
        this.onBuildItemForm(item, form);
    };
    FileUploader.prototype._onProgressItem = function (item, progress) {
        var total = this._getTotalProgress(progress);
        this.progress = total;
        item._onProgress(progress);
        this.onProgressItem(item, progress);
        this.onProgressAll(total);
        this._render();
    };
    FileUploader.prototype._onSuccessItem = function (item, response, status, headers) {
        item._onSuccess(response, status, headers);
        this.onSuccessItem(item, response, status, headers);
    };
    FileUploader.prototype._onCancelItem = function (item, response, status, headers) {
        item._onCancel(response, status, headers);
        this.onCancelItem(item, response, status, headers);
    };
    return FileUploader;
}());
exports.FileUploader = FileUploader;


/***/ }),

/***/ "./node_modules/ng2-file-upload/index.js":
/*!***********************************************!*\
  !*** ./node_modules/ng2-file-upload/index.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(/*! ./file-upload/file-select.directive */ "./node_modules/ng2-file-upload/file-upload/file-select.directive.js"));
__export(__webpack_require__(/*! ./file-upload/file-drop.directive */ "./node_modules/ng2-file-upload/file-upload/file-drop.directive.js"));
__export(__webpack_require__(/*! ./file-upload/file-uploader.class */ "./node_modules/ng2-file-upload/file-upload/file-uploader.class.js"));
__export(__webpack_require__(/*! ./file-upload/file-item.class */ "./node_modules/ng2-file-upload/file-upload/file-item.class.js"));
__export(__webpack_require__(/*! ./file-upload/file-like-object.class */ "./node_modules/ng2-file-upload/file-upload/file-like-object.class.js"));
var file_upload_module_1 = __webpack_require__(/*! ./file-upload/file-upload.module */ "./node_modules/ng2-file-upload/file-upload/file-upload.module.js");
exports.FileUploadModule = file_upload_module_1.FileUploadModule;


/***/ }),

/***/ "./node_modules/ng2-file-upload/ng2-file-upload.js":
/*!*********************************************************!*\
  !*** ./node_modules/ng2-file-upload/ng2-file-upload.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__(/*! ./index */ "./node_modules/ng2-file-upload/index.js"));


/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/map.js":
/*!*********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/map.js ***!
  \*********************************************************/
/*! exports provided: map */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "map", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"]; });


//# sourceMappingURL=map.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operators/startWith.js":
/*!***************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operators/startWith.js ***!
  \***************************************************************/
/*! exports provided: startWith */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "startWith", function() { return rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["startWith"]; });


//# sourceMappingURL=startWith.js.map

/***/ }),

/***/ "./src/app/forms/autocomplete/autocomplete.component.html":
/*!****************************************************************!*\
  !*** ./src/app/forms/autocomplete/autocomplete.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- Card Grid-->\n<!-- ============================================================== -->\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Autocomplete Example</mat-card-title>\n                <!-- ============================================================== -->\n                <!-- column -->\n                <!-- ============================================================== -->\n                <form class=\"example-form\">\n                    <mat-form-field class=\"example-full-width\">\n                        <input matInput placeholder=\"State\" aria-label=\"State\" [matAutocomplete]=\"auto\" [formControl]=\"stateCtrl\">\n                        <mat-autocomplete #auto=\"matAutocomplete\">\n                            <mat-option *ngFor=\"let state of filteredStates | async\" [value]=\"state.name\"> <img style=\"vertical-align:middle;\" aria-hidden src=\"{{state.flag}}\" height=\"25\" /> <span>{{ state.name }}</span> | <small>Population: {{state.population}}</small> </mat-option>\n                        </mat-autocomplete>\n                    </mat-form-field>\n                    <br />\n                    <mat-slide-toggle [checked]=\"stateCtrl.disabled\" (change)=\"stateCtrl.disabled ? stateCtrl.enable() : stateCtrl.disable()\"> Disable Input? </mat-slide-toggle>\n                </form>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/forms/autocomplete/autocomplete.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/forms/autocomplete/autocomplete.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 90%; }\n"

/***/ }),

/***/ "./src/app/forms/autocomplete/autocomplete.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/forms/autocomplete/autocomplete.component.ts ***!
  \**************************************************************/
/*! exports provided: State, AutocompleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "State", function() { return State; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutocompleteComponent", function() { return AutocompleteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators_startWith__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators/startWith */ "./node_modules/rxjs-compat/_esm5/operators/startWith.js");
/* harmony import */ var rxjs_operators_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators/map */ "./node_modules/rxjs-compat/_esm5/operators/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var State = /** @class */ (function () {
    function State(name, population, flag) {
        this.name = name;
        this.population = population;
        this.flag = flag;
    }
    return State;
}());

var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent() {
        var _this = this;
        this.states = [
            {
                name: 'Arkansas',
                population: '2.978M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_Arkansas.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
            },
            {
                name: 'California',
                population: '39.14M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_California.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
            },
            {
                name: 'Florida',
                population: '20.27M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_Florida.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
            },
            {
                name: 'Texas',
                population: '27.47M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_Texas.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
            }
        ];
        this.stateCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
        this.filteredStates = this.stateCtrl.valueChanges
            .pipe(Object(rxjs_operators_startWith__WEBPACK_IMPORTED_MODULE_2__["startWith"])(''), Object(rxjs_operators_map__WEBPACK_IMPORTED_MODULE_3__["map"])(function (state) { return state ? _this.filterStates(state) : _this.states.slice(); }));
    }
    AutocompleteComponent.prototype.filterStates = function (name) {
        return this.states.filter(function (state) {
            return state.name.toLowerCase().indexOf(name.toLowerCase()) === 0;
        });
    };
    AutocompleteComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-autocomplete',
            template: __webpack_require__(/*! ./autocomplete.component.html */ "./src/app/forms/autocomplete/autocomplete.component.html"),
            styles: [__webpack_require__(/*! ./autocomplete.component.scss */ "./src/app/forms/autocomplete/autocomplete.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AutocompleteComponent);
    return AutocompleteComponent;
}());



/***/ }),

/***/ "./src/app/forms/checkbox/checkbox.component.html":
/*!********************************************************!*\
  !*** ./src/app/forms/checkbox/checkbox.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n  <mat-card-content>\n    <h3 class=\"example-h2\">Checkbox configuration</h3>\n\n    <section class=\"example-section\">\n      <mat-checkbox class=\"example-margin\" [(ngModel)]=\"checked\">Checked</mat-checkbox>\n      <mat-checkbox class=\"example-margin\" [(ngModel)]=\"indeterminate\">Indeterminate</mat-checkbox>\n    </section>\n\n    <section class=\"example-section\">\n      <label class=\"example-margin\">Align:</label>\n      <mat-radio-group [(ngModel)]=\"labelPosition\">\n        <mat-radio-button class=\"example-margin\" value=\"after\">After</mat-radio-button>\n        <mat-radio-button class=\"example-margin\" value=\"before\">Before</mat-radio-button>\n      </mat-radio-group>\n    </section>\n\n    <section class=\"example-section\">\n      <mat-checkbox class=\"example-margin\" [(ngModel)]=\"disabled\">Disabled</mat-checkbox>\n    </section>\n  </mat-card-content>\n</mat-card>\n\n<mat-card class=\"result\">\n  <mat-card-content>\n    <h2 class=\"example-h2\">Result</h2>\n\n    <section class=\"example-section\">\n      <mat-checkbox\n          class=\"example-margin\"\n          [(ngModel)]=\"checked\"\n          [(indeterminate)]=\"indeterminate\"\n          [labelPosition]=\"labelPosition\"\n          [disabled]=\"disabled\">\n        I'm a checkbox\n      </mat-checkbox>\n    </section>\n  </mat-card-content>\n</mat-card>\n"

/***/ }),

/***/ "./src/app/forms/checkbox/checkbox.component.scss":
/*!********************************************************!*\
  !*** ./src/app/forms/checkbox/checkbox.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-h2 {\n  margin: 10px; }\n\n.example-section {\n  display: flex;\n  align-content: center;\n  align-items: center;\n  height: 60px; }\n\n.example-margin {\n  margin: 0 10px; }\n"

/***/ }),

/***/ "./src/app/forms/checkbox/checkbox.component.ts":
/*!******************************************************!*\
  !*** ./src/app/forms/checkbox/checkbox.component.ts ***!
  \******************************************************/
/*! exports provided: CheckboxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckboxComponent", function() { return CheckboxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CheckboxComponent = /** @class */ (function () {
    function CheckboxComponent() {
        this.checked = false;
        this.indeterminate = false;
        this.align = 'start';
        this.disabled = false;
    }
    CheckboxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-checkbox',
            template: __webpack_require__(/*! ./checkbox.component.html */ "./src/app/forms/checkbox/checkbox.component.html"),
            styles: [__webpack_require__(/*! ./checkbox.component.scss */ "./src/app/forms/checkbox/checkbox.component.scss")]
        })
    ], CheckboxComponent);
    return CheckboxComponent;
}());



/***/ }),

/***/ "./src/app/forms/datepicker/datepicker.component.html":
/*!************************************************************!*\
  !*** ./src/app/forms/datepicker/datepicker.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- Fixed height Card Grid-->\n<!-- ============================================================== -->\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Basic Datepicker</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n            <mat-form-field>\n              <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n              <mat-datepicker #picker></mat-datepicker>\n            </mat-form-field>\n\n        </mat-card-content>\n\n    </mat-card>\n    </div>\n    <!-- Grid-->\n    <div fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\" >\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Datepicker start date</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n           <mat-form-field>\n              <input matInput [matDatepicker]=\"picker2\" placeholder=\"Choose a date\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\n              <mat-datepicker #picker2 startView=\"year\" [startAt]=\"startDate\"></mat-datepicker>\n            </mat-form-field>\n\n        </mat-card-content>\n\n    </mat-card>\n    </div>\n     <!-- Grid-->\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" >\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Datepicker selected value</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n           <mat-form-field>\n              <input matInput [matDatepicker]=\"picker3\" placeholder=\"Angular forms\" [formControl]=\"date\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker3\"></mat-datepicker-toggle>\n              <mat-datepicker #picker3></mat-datepicker>\n            </mat-form-field>\n\n            <mat-form-field>\n              <input matInput [matDatepicker]=\"picker4\" placeholder=\"Angular forms (w/ deserialization)\"\n                     [formControl]=\"serializedDate\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker4\"></mat-datepicker-toggle>\n              <mat-datepicker #picker4></mat-datepicker>\n            </mat-form-field>\n\n            <mat-form-field>\n              <input matInput [matDatepicker]=\"picker5\" placeholder=\"Value binding\" [value]=\"date.value\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker5\"></mat-datepicker-toggle>\n              <mat-datepicker #picker5></mat-datepicker>\n            </mat-form-field>\n\n        </mat-card-content>\n\n    </mat-card>\n    </div>\n    <!-- Grid-->\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" >\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Datepicker input and change events</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n           <mat-form-field>\n              <input matInput [matDatepicker]=\"picker6\" placeholder=\"Input & change events\"\n                     (dateInput)=\"addEvent('input', $event)\" (dateChange)=\"addEvent('change', $event)\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker6\"></mat-datepicker-toggle>\n              <mat-datepicker #picker6></mat-datepicker>\n            </mat-form-field>\n\n            <div class=\"b-all p-20\">\n              <div *ngFor=\"let e of events\">{{e}}</div>\n            </div>\n\n        </mat-card-content>\n\n    </mat-card>\n    </div>\n    <!-- Grid-->\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" >\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Disabled datepicker</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n           <p>\n              <mat-form-field>\n                <input matInput [matDatepicker]=\"dp1\" placeholder=\"Completely disabled\" disabled>\n                <mat-datepicker-toggle matSuffix [for]=\"dp1\"></mat-datepicker-toggle>\n                <mat-datepicker #dp1></mat-datepicker>\n              </mat-form-field>\n            </p>\n\n            <p>\n              <mat-form-field>\n                <input matInput [matDatepicker]=\"dp2\" placeholder=\"Popup disabled\">\n                <mat-datepicker-toggle matSuffix [for]=\"dp2\" disabled></mat-datepicker-toggle>\n                <mat-datepicker #dp2></mat-datepicker>\n              </mat-form-field>\n            </p>\n\n            <p>\n              <mat-form-field>\n                <input matInput [matDatepicker]=\"dp3\" placeholder=\"Input disabled\" disabled>\n                <mat-datepicker-toggle matSuffix [for]=\"dp3\"></mat-datepicker-toggle>\n                <mat-datepicker #dp3 disabled=\"false\"></mat-datepicker>\n              </mat-form-field>\n            </p>\n\n        </mat-card-content>\n\n    </mat-card>\n    </div>\n    <!-- Grid-->\n    <div fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" >\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Datepicker touch UI</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n           <mat-form-field class=\"example-full-width\">\n              <input matInput [matDatepicker]=\"picker7\" placeholder=\"Choose a date\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker7\"></mat-datepicker-toggle>\n              <mat-datepicker touchUi=\"true\" #picker7></mat-datepicker>\n            </mat-form-field>\n\n        </mat-card-content>\n    </mat-card>\n    </div>\n    <!-- Grid-->\n    <div fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" >\n    <mat-card>\n        <mat-card-content>\n            <mat-card-title>Datepicker open method</mat-card-title>\n            <mat-card-subtitle>A material 2 component for datepicker</mat-card-subtitle>\n           <mat-form-field class=\"example-full-width\">\n              <input matInput [matDatepicker]=\"picker8\" placeholder=\"Choose a date\">\n              <mat-datepicker #picker8></mat-datepicker>\n            </mat-form-field>\n            <button mat-raised-button (click)=\"picker8.open()\">Open</button>\n        </mat-card-content>\n    </mat-card>\n    </div>\n    <!-- Grid-->\n    \n</div>    "

/***/ }),

/***/ "./src/app/forms/datepicker/datepicker.component.scss":
/*!************************************************************!*\
  !*** ./src/app/forms/datepicker/datepicker.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "md-calendar {\n  width: 300px; }\n"

/***/ }),

/***/ "./src/app/forms/datepicker/datepicker.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/forms/datepicker/datepicker.component.ts ***!
  \**********************************************************/
/*! exports provided: DatepickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatepickerComponent", function() { return DatepickerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DatepickerComponent = /** @class */ (function () {
    function DatepickerComponent() {
        //this is for the start date
        this.startDate = new Date(1990, 0, 1);
        //Datepicker selected value
        this.date = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](new Date());
        this.serializedDate = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]((new Date()).toISOString());
        //Datepicker input and change event
        this.events = [];
    }
    DatepickerComponent.prototype.addEvent = function (type, event) {
        this.events.push(type + ": " + event.value);
    };
    DatepickerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-datepicker',
            template: __webpack_require__(/*! ./datepicker.component.html */ "./src/app/forms/datepicker/datepicker.component.html"),
            styles: [__webpack_require__(/*! ./datepicker.component.scss */ "./src/app/forms/datepicker/datepicker.component.scss")]
        })
    ], DatepickerComponent);
    return DatepickerComponent;
}());



/***/ }),

/***/ "./src/app/forms/editor/editor.component.html":
/*!****************************************************!*\
  !*** ./src/app/forms/editor/editor.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-card>\n    <mat-card-content>\n    <mat-card-title>Default Editor</mat-card-title>\n    <mat-card-subtitle>quill editor with angular and TypeScript. ngx-quill is the new angular 2 and beyond implementation of ngQuill. <a href=\"https://github.com/KillerCodeMonkey/ngx-quill\" target=\"_blank\">Official website</a></mat-card-subtitle>\n    <quill-editor [style]=\"{height: '200px'}\"></quill-editor> \n    </mat-card-content>    \n</mat-card>\n<mat-card>\n    <mat-card-content>\n    <mat-card-title>Bubble Editor</mat-card-title>\n        <quill-editor theme=\"bubble\" placeholder=\"Bubble editor\" bounds=\".mat-drawer-content\" [style]=\"{border: '1px solid #dadada'}\"></quill-editor> \n    </mat-card-content>    \n</mat-card>\n\n"

/***/ }),

/***/ "./src/app/forms/editor/editor.component.scss":
/*!****************************************************!*\
  !*** ./src/app/forms/editor/editor.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/forms/editor/editor.component.ts":
/*!**************************************************!*\
  !*** ./src/app/forms/editor/editor.component.ts ***!
  \**************************************************/
/*! exports provided: EditorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditorComponent", function() { return EditorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EditorComponent = /** @class */ (function () {
    function EditorComponent() {
        this.subtitle = "This is some text within a card block.";
    }
    EditorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-editor',
            template: __webpack_require__(/*! ./editor.component.html */ "./src/app/forms/editor/editor.component.html"),
            styles: [__webpack_require__(/*! ./editor.component.scss */ "./src/app/forms/editor/editor.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EditorComponent);
    return EditorComponent;
}());



/***/ }),

/***/ "./src/app/forms/file-upload/upload.component.html":
/*!*********************************************************!*\
  !*** ./src/app/forms/file-upload/upload.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <mat-card-title>Angular2 File Upload</mat-card-title>\r\n        <section id=\"file-upload\">\r\n                    \r\n                        <div fxLayout=\"row\" fxLayoutWrap=\"wrap\" class=\"row\">\r\n                            <div fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" class=\"p-10\">\r\n                            <div ng2FileDrop [ngClass]=\"{'nv-file-over': hasBaseDropZoneOver}\" (fileOver)=\"fileOverBase($event)\" [uploader]=\"uploader\" class=\"py-5 mb-3 text-center font-medium-5 text-uppercase grey my-drop-zone\">\r\n                                Base dropzone\r\n                            </div>\r\n                            </div>\r\n                            <div fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"100\" fxFlex=\"100\" class=\"p-10\">\r\n                            <div ng2FileDrop [ngClass]=\"{'another-file-over-class': hasAnotherDropZoneOver}\" (fileOver)=\"fileOverAnother($event)\" [uploader]=\"uploader\" class=\"py-5 mb-3 text-center font-medium-5 text-uppercase grey my-drop-zone\">\r\n                                Another dropzone\r\n                            </div>\r\n                            </div>\r\n                        </div>\r\n                        <div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n                            <div fxFlex.gt-sm=\"25\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n                            <h4>Select files</h4>\r\n                            <div>Multiple</div>\r\n                            <label class=\"custom-file\">\r\n                                <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" multiple class=\"custom-file-input\">\r\n                                <span class=\"custom-file-control\"></span>\r\n                            </label>\r\n\r\n                            <div  class=\"m-t-30\">Single</div>\r\n                            <label class=\"custom-file\">\r\n                                <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" class=\"custom-file-input\">\r\n                                <span class=\"custom-file-control\"></span>\r\n                            </label>\r\n                            </div>\r\n\r\n                            <div fxFlex.gt-sm=\"75\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n                            <h4>Upload queue</h4>\r\n                            <p>Queue length: {{ uploader?.queue?.length }}</p>\r\n\r\n                            <table class=\"table\">\r\n                                <thead>\r\n                                <tr>\r\n                                    <th width=\"50%\">Name</th>\r\n                                    <th>Size</th>\r\n                                    <th>Progress</th>\r\n                                    <th>Status</th>\r\n                                    <th>Actions</th>\r\n                                </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                <tr *ngFor=\"let item of uploader.queue\">\r\n                                <td><strong>{{ item?.file?.name }}</strong></td>\r\n                                <td *ngIf=\"uploader.isHTML5\" nowrap>{{ item?.file?.size/1024/1024 | number:'.2' }} MB</td>\r\n                                <td *ngIf=\"uploader.isHTML5\">\r\n                                    <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                                    <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{ 'width': item.progress + '%' }\"></div>\r\n                                    </div>\r\n                                </td>\r\n                                <td class=\"text-center\">\r\n                                    <span *ngIf=\"item.isSuccess\"><i class=\"fa fa-ok\"></i></span>\r\n                                    <span *ngIf=\"item.isCancel\"><i class=\"fa fa-ban\"></i></span>\r\n                                    <span *ngIf=\"item.isError\"><i class=\"fa fa-remove\"></i></span>\r\n                                </td>\r\n                                <td nowrap>\r\n                                    <button type=\"button\" mat-raised-button color=\"primary\" (click)=\"item.upload()\" [disabled]=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                                    <span class=\"fa fa-upload\"></span> Upload\r\n                                    </button>\r\n                                    <button type=\"button\" mat-raised-button color=\"accent\" (click)=\"item.cancel()\" [disabled]=\"!item.isUploading\">\r\n                                    <span class=\"fa fa-ban\"></span> Cancel\r\n                                    </button>\r\n                                    <button type=\"button\" mat-raised-button color=\"warn\" (click)=\"item.remove()\">\r\n                                    <span class=\"fa fa-trash\"></span> Remove\r\n                                    </button>\r\n                                </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n\r\n                            <div>\r\n                                <p>Queue progress: <mat-progress-bar mode=\"determinate\" [value]=\"uploader.progress\" class=\"m-t-10\"></mat-progress-bar></p>\r\n                                <button type=\"button\" mat-raised-button color=\"primary\" (click)=\"uploader.uploadAll()\" [disabled]=\"!uploader.getNotUploadedItems().length\">\r\n                                <span class=\"fa fa-upload\"></span> Upload all\r\n                                </button>\r\n                                <button type=\"button\" mat-raised-button color=\"accent\" (click)=\"uploader.cancelAll()\" [disabled]=\"!uploader.isUploading\">\r\n                                <span class=\"fa fa-ban\"></span> Cancel all\r\n                                </button>\r\n                                <button type=\"button\" mat-raised-button color=\"warn\" (click)=\"uploader.clearQueue()\" [disabled]=\"!uploader.queue.length\">\r\n                                <span class=\"fa fa-trash\"></span> Remove all\r\n                                </button>\r\n                            </div>\r\n                            </div>\r\n                        </div>\r\n                        \r\n                    </section> \r\n    </mat-card-content>\r\n</mat-card>\r\n        \r\n                                \r\n           \r\n    "

/***/ }),

/***/ "./src/app/forms/file-upload/upload.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/forms/file-upload/upload.component.ts ***!
  \*******************************************************/
/*! exports provided: UploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadComponent", function() { return UploadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var UploadComponent = /** @class */ (function () {
    function UploadComponent() {
        this.uploader = new ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__["FileUploader"]({
            url: URL,
            isHTML5: true
        });
        this.hasBaseDropZoneOver = false;
        this.hasAnotherDropZoneOver = false;
    }
    // Angular2 File Upload
    UploadComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    UploadComponent.prototype.fileOverAnother = function (e) {
        this.hasAnotherDropZoneOver = e;
    };
    UploadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./upload.component.html */ "./src/app/forms/file-upload/upload.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./upload.scss */ "./src/app/forms/file-upload/upload.scss")]
        })
    ], UploadComponent);
    return UploadComponent;
}());



/***/ }),

/***/ "./src/app/forms/file-upload/upload.scss":
/*!***********************************************!*\
  !*** ./src/app/forms/file-upload/upload.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".my-drop-zone {\n  border: dotted 2px #dadada;\n  background-color: #fff !important;\n  min-height: 80px;\n  line-height: 80px; }\n\n.nv-file-over {\n  border: dotted 2px red; }\n\n/* Default class applied to drop zones on over */\n\n.another-file-over-class {\n  border: dotted 2px green; }\n"

/***/ }),

/***/ "./src/app/forms/form-layouts/form-layout.component.html":
/*!***************************************************************!*\
  !*** ./src/app/forms/form-layouts/form-layout.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- Card Grid-->\n<!-- ============================================================== -->\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Form Basic Layouts</mat-card-title>\n                <!-- ============================================================== -->\n                <!-- column -->\n                <!-- ============================================================== -->\n                <form class=\"basic-form\">\n                <div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n                    <!-- column -->\n                    <div fxFlex.gt-md=\"100\" fxFlex=\"100\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"Some text value\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"EmailId\" type=\"email\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"Password\" type=\"password\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field [floatLabel]=\"options.value.floatLabel\">\n                        <mat-label>Both a label and a placeholder</mat-label>\n                        <input matInput placeholder=\"Simple placeholder\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field hintLabel=\"Max 10 characters\">\n                        <input matInput #input maxlength=\"10\" placeholder=\"Enter some input\">\n                        <mat-hint align=\"end\">{{input.value?.length || 0}}/10</mat-hint>\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field>\n                        <mat-select placeholder=\"Select\">\n                          <mat-option value=\"option\">Option</mat-option>\n                          <mat-option value=\"option\">Option2</mat-option>\n                          <mat-option value=\"option\">Option3</mat-option>    \n                        </mat-select>\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\" class=\"m-b-20\">\n                      <input class=\"form-control b-b\" placeholder=\"file\" type=\"file\">\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field\n                          [hideRequiredMarker]=\"options.value.hideRequired\"\n                          [floatLabel]=\"options.value.floatLabel\">\n                        <mat-select required>\n                          <mat-option>-- None --</mat-option>\n                          <mat-option value=\"option\">Option</mat-option>\n                        </mat-select>\n                        <mat-placeholder><mat-icon>favorite</mat-icon> <b> Fancy</b> <i> placeholder</i></mat-placeholder>\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\" class=\"m-t-10 m-b-10\">\n                        <mat-checkbox color=\"primary\"  class=\"m-r-10\">Checkbox</mat-checkbox> \n                        <mat-checkbox  color=\"warn\" class=\"m-r-10\">Checkbox</mat-checkbox> \n                        <mat-checkbox  color=\"accent\">Checkbox</mat-checkbox>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\" class=\"m-t-20 m-b-20\">\n                      <mat-radio-group>\n                        <mat-radio-button color=\"primary\" value=\"auto\" class=\"m-r-10\">Auto</mat-radio-button>\n                        <mat-radio-button color=\"warn\" value=\"always\" class=\"m-r-10\">Always</mat-radio-button>\n                        <mat-radio-button color=\"accent\" value=\"never\">Never</mat-radio-button>\n                      </mat-radio-group>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <mat-form-field>\n                        <textarea matInput placeholder=\"Textarea\"></textarea>\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n                      <button mat-raised-button color=\"primary\">Submit</button>\n                    </div> \n                </div>\n                </form>      \n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Form field with error messages</mat-card-title>\n                <mat-form-field>\n                    <input matInput placeholder=\"Enter your email\" [formControl]=\"email\" required>\n                    <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error>\n                </mat-form-field>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Form field with prefix &amp; suffix</mat-card-title>\n                <mat-form-field>\n                <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\">\n                <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}}</mat-icon>\n              </mat-form-field>\n\n              <mat-form-field>\n                <input matInput placeholder=\"Amount\" type=\"number\" class=\"example-right-align\">\n                <span matPrefix>$&nbsp;</span>\n                <span matSuffix>.00</span>\n              </mat-form-field>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div> \n<!-- ============================================================== -->\n<!-- Card Grid-->\n<!-- ============================================================== -->\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Form with grid</mat-card-title>\n                <mat-card-subtitle>use this class <code>.row</code> to the fxLayout=\"row\" and <code>p-10</code> to the fxFlex div</mat-card-subtitle>\n                <!-- ============================================================== -->\n                <!-- column -->\n                <!-- ============================================================== -->\n                <form class=\"basic-form\">\n                <div fxLayout=\"row\" fxLayoutWrap=\"wrap\" fxFlexAlign=\"center\" class=\"row\">\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"25\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 25\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"25\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 25\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"25\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 25\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"25\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 25\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"33.33\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 33.33\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"33.33\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 33.33\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"33.33\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 33.33\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"50\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 50\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"50\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 50\">\n                      </mat-form-field>\n                    </div>\n                    <!-- column -->\n                    <div fxFlex.gt-sm=\"100\" fxFlex=\"100\" class=\"p-10\">\n                      <mat-form-field>\n                        <input matInput placeholder=\"grid 100\">\n                      </mat-form-field>\n                    </div>\n                </div>\n                </form>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>    \n                    "

/***/ }),

/***/ "./src/app/forms/form-layouts/form-layout.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/forms/form-layouts/form-layout.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 90%; }\n"

/***/ }),

/***/ "./src/app/forms/form-layouts/form-layout.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/forms/form-layouts/form-layout.component.ts ***!
  \*************************************************************/
/*! exports provided: FormLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormLayoutComponent", function() { return FormLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FormLayoutComponent = /** @class */ (function () {
    function FormLayoutComponent(fb) {
        //For form validator
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]);
        // Sufix and prefix
        this.hide = true;
        this.options = fb.group({
            hideRequired: false,
            floatLabel: 'auto',
        });
    }
    FormLayoutComponent.prototype.getErrorMessage = function () {
        return this.email.hasError('required') ? 'You must enter a value' :
            this.email.hasError('email') ? 'Not a valid email' :
                '';
    };
    FormLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-formlayout',
            template: __webpack_require__(/*! ./form-layout.component.html */ "./src/app/forms/form-layouts/form-layout.component.html"),
            styles: [__webpack_require__(/*! ./form-layout.component.scss */ "./src/app/forms/form-layouts/form-layout.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], FormLayoutComponent);
    return FormLayoutComponent;
}());



/***/ }),

/***/ "./src/app/forms/form-validation/form-validation.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/forms/form-validation/form-validation.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<mat-card>\n  <mat-card-content>\n  <mat-card-title>Form validation</mat-card-title>\n  <mat-card-subtitle>Angular2 custom validation here is the  <a href=\"https://github.com/yuyang041060120/ng2-validation\" target=\"_blank\">official site</a></mat-card-subtitle>\n  <form [formGroup]=\"form\">\n     \n      <div fxLayout=\"row\" fxLayoutWrap=\"wrap\" class=\"row\">\n        <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n          <mat-form-field>\n            <input matInput placeholder=\"First name\" [formControl]=\"form.controls['fname']\">\n          </mat-form-field>\n            <mat-hint *ngIf=\"form.controls['fname'].hasError('required') && form.controls['fname'].touched\" class=\"text-danger font-14\">You must include a first name.</mat-hint>\n            <mat-hint *ngIf=\"form.controls['fname'].hasError('minlength') && form.controls['fname'].touched\" class=\"text-danger font-14\">Your first name must be at least 5 characters long.</mat-hint>\n            <mat-hint *ngIf=\"form.controls['fname'].hasError('maxlength') && form.controls['fname'].touched\" class=\"text-danger font-14\">Your first name cannot exceed 10 characters.</mat-hint>\n        </div>\n      \n\n      <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field >\n          <input matInput placeholder=\"Email Id\" [formControl]=\"form.controls['email']\" type=\"email\">\n        </mat-form-field>\n        <small *ngIf=\"form.controls['email'].hasError('required') && form.controls['email'].touched\" class=\"text-danger font-14\">You must include an email address.</small>\n        <small *ngIf=\"form.controls['email'].errors?.email && form.controls['email'].touched\" class=\"text-danger font-14\">You must include a valid email address.</small>\n      </div>\n\n      <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field>\n          <input matInput placeholder=\"Website\" [formControl]=\"form.controls['url']\" type=\"url\">\n        </mat-form-field>\n        <small *ngIf=\"form.controls['url'].hasError('required') && form.controls['url'].touched\" class=\"text-danger font-14\">You must include a web address.</small>\n        <small *ngIf=\"form.controls['url'].errors?.url && form.controls['url'].touched\" class=\"text-danger font-14\">You must include a valid web address.</small>\n      </div>\n\n      <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field>\n              <input matInput [matDatepicker]=\"picker\" [formControl]=\"form.controls['date']\" placeholder=\"Choose a date\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n              <mat-datepicker #picker></mat-datepicker>\n            </mat-form-field>  \n          <mat-hint *ngIf=\"form.controls['date'].hasError('required') && form.controls['date'].touched\" class=\"text-danger font-14\">You must include a date.</mat-hint>\n          <mat-hint *ngIf=\"form.controls['date'].errors?.date && form.controls['date'].touched\" class=\"text-danger font-14\">You must include a valid date.</mat-hint>\n      </div>\n\n      <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field>\n          <input matInput placeholder=\"Number range (between 5 and 9)\" [formControl]=\"form.controls['range']\">\n        </mat-form-field>\n        <small *ngIf=\"form.controls['range'].hasError('required') && form.controls['range'].touched\" class=\"text-danger font-14\">You must enter a number.</small>\n        <small *ngIf=\"form.controls['range'].errors?.range && form.controls['range'].touched\" class=\"text-danger font-14\">Number should be between 5 and 9.</small>\n      </div>\n     <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field>\n          <input matInput placeholder=\"Phone number\" [formControl]=\"form.controls['phone']\" type=\"text\">\n        </mat-form-field>\n        <small *ngIf=\"form.controls['phone'].hasError('required') && form.controls['phone'].touched\" class=\"text-danger font-14\">You must include phone number.</small>\n        <small *ngIf=\"form.controls['phone'].errors?.phone && form.controls['phone'].touched\" class=\"text-danger font-14\">You must include a valid phone number.</small>\n      </div>\n\n    \t<div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field>\n          <input matInput placeholder=\"Password\" [formControl]=\"form.controls['password']\" type=\"password\">\n        </mat-form-field>\n    \t\t<small *ngIf=\"form.controls['password'].hasError('required') && form.controls['password'].touched\" class=\"text-danger font-14\">You must include password.</small>\n      </div>\n\n      <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <mat-form-field>\n          <input matInput placeholder=\"Confirm Password\" [formControl]=\"form.controls['confirmPassword']\" type=\"password\">\n        </mat-form-field>\n    \t\t<small *ngIf=\"form.controls['confirmPassword'].hasError('required') && form.controls['confirmPassword'].touched\" class=\"text-danger font-14\">You must include confirm password.</small>\n    \t\t<small *ngIf=\"form.controls['confirmPassword'].errors?.equalTo\" class=\"text-danger font-14\">Passwords do not math.</small>\n    \t</div>\n\n      <div class=\"p-10\" fxFlex.gt-sm=\"50\" fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <label class=\"m-r-20\">Gender :</label>\n    \t  <mat-radio-group [formControl]=\"form.controls['gender']\">\n      \t  <mat-radio-button value=\"male\" class=\"m-r-10\">Male</mat-radio-button>\n      \t  <mat-radio-button value=\"female\">Female</mat-radio-button>\n      \t</mat-radio-group>\n        <small *ngIf=\"!form.controls['gender'].valid && form.controls['gender'].touched\" class=\"mat-text-warn\">You must select a gender.</small>\n      </div>\n    </div>\n    \n    <mat-card-actions>\n      <button mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"!form.valid\">Submit</button>\n    </mat-card-actions>\n  </form>\n    </mat-card-content>    \n</mat-card>"

/***/ }),

/***/ "./src/app/forms/form-validation/form-validation.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/forms/form-validation/form-validation.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/forms/form-validation/form-validation.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/forms/form-validation/form-validation.component.ts ***!
  \********************************************************************/
/*! exports provided: FormValidationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormValidationComponent", function() { return FormValidationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
var confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].equalTo(password));
var FormValidationComponent = /** @class */ (function () {
    function FormValidationComponent(fb) {
        this.fb = fb;
    }
    FormValidationComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            fname: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(10)])],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].email])],
            range: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].range([5, 9])])],
            url: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].url])],
            date: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].date])],
            phone: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].phone('IN')])],
            gender: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: password,
            confirmPassword: confirmPassword
        });
    };
    FormValidationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-form-validation',
            template: __webpack_require__(/*! ./form-validation.component.html */ "./src/app/forms/form-validation/form-validation.component.html"),
            styles: [__webpack_require__(/*! ./form-validation.component.scss */ "./src/app/forms/form-validation/form-validation.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], FormValidationComponent);
    return FormValidationComponent;
}());



/***/ }),

/***/ "./src/app/forms/forms.module.ts":
/*!***************************************!*\
  !*** ./src/app/forms/forms.module.ts ***!
  \***************************************/
/*! exports provided: FormModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormModule", function() { return FormModule; });
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _demo_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../demo-material-module */ "./src/app/demo-material-module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _forms_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./forms.routing */ "./src/app/forms/forms.routing.ts");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/index.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./autocomplete/autocomplete.component */ "./src/app/forms/autocomplete/autocomplete.component.ts");
/* harmony import */ var _checkbox_checkbox_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./checkbox/checkbox.component */ "./src/app/forms/checkbox/checkbox.component.ts");
/* harmony import */ var _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./datepicker/datepicker.component */ "./src/app/forms/datepicker/datepicker.component.ts");
/* harmony import */ var _form_layouts_form_layout_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./form-layouts/form-layout.component */ "./src/app/forms/form-layouts/form-layout.component.ts");
/* harmony import */ var _editor_editor_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./editor/editor.component */ "./src/app/forms/editor/editor.component.ts");
/* harmony import */ var _form_validation_form_validation_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./form-validation/form-validation.component */ "./src/app/forms/form-validation/form-validation.component.ts");
/* harmony import */ var _file_upload_upload_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./file-upload/upload.component */ "./src/app/forms/file-upload/upload.component.ts");
/* harmony import */ var _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./wizard/wizard.component */ "./src/app/forms/wizard/wizard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var FormModule = /** @class */ (function () {
    function FormModule() {
    }
    FormModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_forms_routing__WEBPACK_IMPORTED_MODULE_7__["FormRoutes"]),
                _demo_material_module__WEBPACK_IMPORTED_MODULE_4__["DemoMaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_8__["QuillModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_9__["FileUploadModule"]
            ],
            declarations: [
                _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_10__["AutocompleteComponent"],
                _checkbox_checkbox_component__WEBPACK_IMPORTED_MODULE_11__["CheckboxComponent"],
                _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_12__["DatepickerComponent"],
                _form_layouts_form_layout_component__WEBPACK_IMPORTED_MODULE_13__["FormLayoutComponent"],
                _editor_editor_component__WEBPACK_IMPORTED_MODULE_14__["EditorComponent"],
                _form_validation_form_validation_component__WEBPACK_IMPORTED_MODULE_15__["FormValidationComponent"],
                _file_upload_upload_component__WEBPACK_IMPORTED_MODULE_16__["UploadComponent"],
                _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_17__["WizardComponent"]
            ],
        })
    ], FormModule);
    return FormModule;
}());



/***/ }),

/***/ "./src/app/forms/forms.routing.ts":
/*!****************************************!*\
  !*** ./src/app/forms/forms.routing.ts ***!
  \****************************************/
/*! exports provided: FormRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormRoutes", function() { return FormRoutes; });
/* harmony import */ var _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./autocomplete/autocomplete.component */ "./src/app/forms/autocomplete/autocomplete.component.ts");
/* harmony import */ var _checkbox_checkbox_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./checkbox/checkbox.component */ "./src/app/forms/checkbox/checkbox.component.ts");
/* harmony import */ var _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./datepicker/datepicker.component */ "./src/app/forms/datepicker/datepicker.component.ts");
/* harmony import */ var _form_layouts_form_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./form-layouts/form-layout.component */ "./src/app/forms/form-layouts/form-layout.component.ts");
/* harmony import */ var _editor_editor_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./editor/editor.component */ "./src/app/forms/editor/editor.component.ts");
/* harmony import */ var _form_validation_form_validation_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./form-validation/form-validation.component */ "./src/app/forms/form-validation/form-validation.component.ts");
/* harmony import */ var _file_upload_upload_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./file-upload/upload.component */ "./src/app/forms/file-upload/upload.component.ts");
/* harmony import */ var _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wizard/wizard.component */ "./src/app/forms/wizard/wizard.component.ts");








var FormRoutes = [
    {
        path: '',
        children: [{
                path: 'autocomplete',
                component: _autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_0__["AutocompleteComponent"]
            }, {
                path: 'checkbox',
                component: _checkbox_checkbox_component__WEBPACK_IMPORTED_MODULE_1__["CheckboxComponent"]
            }, {
                path: 'datepicker',
                component: _datepicker_datepicker_component__WEBPACK_IMPORTED_MODULE_2__["DatepickerComponent"]
            }, {
                path: 'form-layout',
                component: _form_layouts_form_layout_component__WEBPACK_IMPORTED_MODULE_3__["FormLayoutComponent"]
            }, {
                path: 'editor',
                component: _editor_editor_component__WEBPACK_IMPORTED_MODULE_4__["EditorComponent"]
            }, {
                path: 'form-validation',
                component: _form_validation_form_validation_component__WEBPACK_IMPORTED_MODULE_5__["FormValidationComponent"]
            }, {
                path: 'file-upload',
                component: _file_upload_upload_component__WEBPACK_IMPORTED_MODULE_6__["UploadComponent"]
            }, {
                path: 'wizard',
                component: _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_7__["WizardComponent"]
            }]
    }
];


/***/ }),

/***/ "./src/app/forms/wizard/wizard.component.html":
/*!****************************************************!*\
  !*** ./src/app/forms/wizard/wizard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- ============================================================== -->\n<!-- row -->\n<!-- ============================================================== -->\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <!-- Card column -->\n    <div fxFlex.gt-sm=\"100%\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Horizontal wizard</mat-card-title>\n                <button mat-raised-button (click)=\"isLinear = true\" id=\"toggle-linear\">Enable linear mode</button>\n                <mat-horizontal-stepper [linear]=\"isLinear\">\n                  <mat-step [stepControl]=\"firstFormGroup\">\n                    <form [formGroup]=\"firstFormGroup\">\n                      <ng-template matStepLabel>Fill out your name</ng-template>\n                      <mat-form-field>\n                        <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\n                      </mat-form-field>\n                      <div>\n                        <button mat-raised-button color=\"warn\" matStepperNext>Next</button>\n                      </div>\n                    </form>\n                  </mat-step>\n                  <mat-step [stepControl]=\"secondFormGroup\">\n                    <form [formGroup]=\"secondFormGroup\">\n                      <ng-template matStepLabel>Fill out your address</ng-template>\n                      <mat-form-field>\n                        <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\n                      </mat-form-field>\n                      <div>\n                        <button mat-raised-button color=\"accent\" matStepperPrevious>Back</button>\n                        <button mat-raised-button color=\"warn\" matStepperNext>Next</button>\n                      </div>\n                    </form>\n                  </mat-step>\n                  <mat-step>\n                    <ng-template matStepLabel>Done</ng-template>\n                    You are now done.\n                    <div>\n                      <button mat-raised-button color=\"accent\" matStepperPrevious>Back</button>\n                    </div>\n                  </mat-step>\n                </mat-horizontal-stepper>\n            \n            </mat-card-content>\n             \n        </mat-card>    \n    </div>\n</div> \n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <!-- Card column -->\n    <div fxFlex.gt-sm=\"100%\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Vertical wizard</mat-card-title>\n                <button mat-raised-button (click)=\"isLinear = true\" id=\"toggle-linear\">Enable linear mode</button>\n                <mat-vertical-stepper [linear]=\"isLinear\">\n                  <mat-step [stepControl]=\"firstFormGroup\">\n                    <form [formGroup]=\"firstFormGroup\">\n                      <ng-template matStepLabel>Fill out your name</ng-template>\n                      <mat-form-field>\n                        <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\n                      </mat-form-field>\n                      <div>\n                        <button mat-raised-button color=\"warn\" matStepperNext>Next</button>\n                      </div>\n                    </form>\n                  </mat-step>\n                  <mat-step [stepControl]=\"secondFormGroup\">\n                    <form [formGroup]=\"secondFormGroup\">\n                      <ng-template matStepLabel>Fill out your address</ng-template>\n                      <mat-form-field>\n                        <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\n                      </mat-form-field>\n                      <div>\n                        <button mat-raised-button color=\"accent\" matStepperPrevious>Back</button>\n                        <button mat-raised-button color=\"warn\" matStepperNext>Next</button>\n                      </div>\n                    </form>\n                  </mat-step>\n                  <mat-step>\n                    <ng-template matStepLabel>Done</ng-template>\n                    You are now done.\n                    <div>\n                      <button mat-raised-button color=\"accent\" matStepperPrevious>Back</button>\n                    </div>\n                  </mat-step>\n                </mat-vertical-stepper>\n            \n            </mat-card-content>\n             \n        </mat-card>    \n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/forms/wizard/wizard.component.scss":
/*!****************************************************!*\
  !*** ./src/app/forms/wizard/wizard.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/forms/wizard/wizard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/forms/wizard/wizard.component.ts ***!
  \**************************************************/
/*! exports provided: WizardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WizardComponent", function() { return WizardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WizardComponent = /** @class */ (function () {
    function WizardComponent(_formBuilder) {
        this._formBuilder = _formBuilder;
        this.isLinear = false;
    }
    WizardComponent.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    WizardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wizard',
            template: __webpack_require__(/*! ./wizard.component.html */ "./src/app/forms/wizard/wizard.component.html"),
            styles: [__webpack_require__(/*! ./wizard.component.scss */ "./src/app/forms/wizard/wizard.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], WizardComponent);
    return WizardComponent;
}());



/***/ })

}]);
//# sourceMappingURL=forms-forms-module.js.map