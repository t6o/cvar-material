(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pages-module"],{

/***/ "./src/app/pages/helper-classes/helper.component.html":
/*!************************************************************!*\
  !*** ./src/app/pages/helper-classes/helper.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-md=\"50\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Text Color Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.text-muted</code></mat-list-item>\n                    <mat-list-item><code>.text-primary</code></mat-list-item>\n                    <mat-list-item><code>.text-success</code></mat-list-item>\n                    <mat-list-item><code>.text-info</code></mat-list-item>\n                    <mat-list-item><code>.text-warning</code></mat-list-item>\n                    <mat-list-item><code>.text-danger</code></mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Border Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.b-all</code> : Border all side</mat-list-item>\n                    <mat-list-item><code>.b-0</code> : Border none all side</mat-list-item>\n                    <mat-list-item><code>.b-t</code> : Border top</mat-list-item>\n                    <mat-list-item><code>.b-b</code> : Border bottom</mat-list-item>\n                    <mat-list-item><code>.b-l</code> : Border left</mat-list-item>\n                    <mat-list-item><code>.b-r</code> : Border right</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Background Colors </mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.bg-primary</code></mat-list-item>\n                    <mat-list-item><code>.bg-success</code></mat-list-item>\n                    <mat-list-item><code>.bg-warning</code></mat-list-item>\n                    <mat-list-item><code>.bg-danger</code></mat-list-item>\n                    <mat-list-item><code>.bg-inverse</code></mat-list-item>\n                    <mat-list-item><code>.bg-info</code></mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Extra Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.oh</code> : overflow Hidden</mat-list-item>\n                    <mat-list-item><code>.relative</code> : Position Relative</mat-list-item>\n                    <mat-list-item><code>.absolute</code>: Position Absolute</mat-list-item>\n                    <mat-list-item><code>.d-flex</code>: Display flex</mat-list-item>\n                    <mat-list-item><code>.db</code> : Display Blcok</mat-list-item>\n                    <mat-list-item><code>.dl</code> : Display Inline block</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-md=\"50\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Margin Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.m-0</code>Margin zero to all</mat-list-item>\n                    <mat-list-item><code>.m-t-5</code> Only Margin top will be 5px.</mat-list-item>\n                    <mat-list-item><code>.m-t-10</code> Only Margin top will be 10px.</mat-list-item>\n                    <mat-list-item><code>.m-t-15</code> Only Margin top will be 15px.</mat-list-item>\n                    <mat-list-item><code>.m-t-20</code> Only Margin top will be 20px.</mat-list-item>\n                    <mat-list-item><code>.m-t-30</code> Only Margin top will be 30px.</mat-list-item>\n                    <mat-list-item><code>.m-t-40</code> Only Margin top will be 40px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Margin Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.m-b-5</code> Only Margin Bottom will be 5px.</mat-list-item>\n                    <mat-list-item><code>.m-b-10</code> Only Margin Bottom will be 10px.</mat-list-item>\n                    <mat-list-item><code>.m-b-15</code> Only Margin Bottom will be 15px.</mat-list-item>\n                    <mat-list-item><code>.m-b-20</code> Only Margin Bottom will be 20px.</mat-list-item>\n                    <mat-list-item><code>.m-b-30</code> Only Margin Bottom will be 30px.</mat-list-item>\n                    <mat-list-item><code>.m-t-40</code> Only Margin top will be 40px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Margin Classes </mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.m-r-5</code> Only Margin Right will be 5px.</mat-list-item>\n                    <mat-list-item><code>.m-r-10</code> Only Margin Right will be 10px.</mat-list-item>\n                    <mat-list-item><code>.m-r-15</code> Only Margin Right will be 15px.</mat-list-item>\n                    <mat-list-item><code>.m-r-20</code> Only Margin Right will be 20px.</mat-list-item>\n                    <mat-list-item><code>.m-r-30</code> Only Margin Right will be 30px.</mat-list-item>\n                    <mat-list-item><code>.m-r-40</code> Only Margin Right will be 40px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Margin Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.m-l-5</code> Only Margin Left will be 5px.</mat-list-item>\n                    <mat-list-item><code>.m-l-10</code> Only Margin Left will be 10px.</mat-list-item>\n                    <mat-list-item><code>.m-l-15</code> Only Margin Left will be 15px.</mat-list-item>\n                    <mat-list-item><code>.m-l-20</code> Only Margin Left will be 20px.</mat-list-item>\n                    <mat-list-item><code>.m-l-30</code> Only Margin Left will be 30px.</mat-list-item>\n                    <mat-list-item><code>.m-l-40</code> Only Margin Left will be 40px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-md=\"50\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Padding Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.p-0</code> Padding will be 0px from all side.</mat-list-item>\n                    <mat-list-item><code>.p-10</code> Padding will be 10px from all side.</mat-list-item>\n                    <mat-list-item><code>.p-20</code> Padding will be 20px from all side</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Padding Classes</mat-card-title>\n                <mat-list role=\"list\">\n                <mat-list-item><code>.p-l-0</code> Only Padding Left will be 0px.</mat-list-item>\n                    <mat-list-item><code>.p-l-10</code> Only Padding Left will be 10px.</mat-list-item>\n                    <mat-list-item><code>.p-l-20</code> Only Padding Left will be 20px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Padding Classes </mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.p-r-0</code> Only Padding right will be 0px.</mat-list-item>\n                    <mat-list-item><code>.p-r-10</code> Only Padding right will be 10px.</mat-list-item>\n                    <mat-list-item><code>.p-r-20</code> Only Padding right will be 20px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"25\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Padding Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.p-t-0</code> Only Padding top will be 0px.</mat-list-item>\n                    <mat-list-item><code>.p-t-10</code> Only Padding top will be 10px.</mat-list-item>\n                    <mat-list-item><code>.p-t-20</code> Only Padding top will be 20px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-md=\"50\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Font Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.font-bold</code> Font bold</mat-list-item>\n                    <mat-list-item><code>.font-normal</code> Font Normal.</mat-list-item>\n                    <mat-list-item><code>.font-light</code> Font Light</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>font Classes</mat-card-title>\n                <mat-list role=\"list\">\n                <mat-list-item><code>.font-medium</code> Font Medium.</mat-list-item>\n                    <mat-list-item><code>.font-10</code> Font size 10px.</mat-list-item>\n                    <mat-list-item><code>.font-12</code> Font size 12px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Font Classes </mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.font-14</code> font-size 14px.</mat-list-item>\n                    <mat-list-item><code>.font-16</code> font size 16px.</mat-list-item>\n                    <mat-list-item><code>.font-20</code> font size 20px.</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Extra Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.img-circle</code> Image transform in circle.</mat-list-item>\n                    <mat-list-item><code>.radius</code> Rounded corner to the element.</mat-list-item>\n                    <mat-list-item><code>.thumb-lg</code> Thumbnail 88px.</mat-list-item>\n                    <mat-list-item><code>.thumb-md</code> Thumbnail 48px.</mat-list-item>\n                    <mat-list-item><code>.thumb-sm</code> Thumbnail 32px.</mat-list-item>\n                    \n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n\n\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n    <div fxFlex.gt-md=\"50\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Round Classes</mat-card-title>\n                <mat-list role=\"list\">\n                    <mat-list-item><code>.round-lg</code> round-lg 60px </mat-list-item>\n                    <mat-list-item><code>.round-success</code> Round green.</mat-list-item>\n                    <mat-list-item><code>.round-info</code> Round Blue</mat-list-item>\n                    <mat-list-item><code>.round-warning</code> Round yellow</mat-list-item>\n                    <mat-list-item><code>.round-danger</code> Round Red</mat-list-item>\n                    <mat-list-item><code>.round-primary</code> Round perple</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Lebel Classes</mat-card-title>\n                <mat-list role=\"list\">\n                <mat-list-item><code>.label-rounded</code> rounded label </mat-list-item>\n                    <mat-list-item><code>.label-success</code> label green.</mat-list-item>\n                    <mat-list-item><code>.label-info</code> label Blue</mat-list-item>\n                    <mat-list-item><code>.label-warning</code> label yellow</mat-list-item>\n                    <mat-list-item><code>.label-danger</code> label Red</mat-list-item>\n                    <mat-list-item><code>.label-primary</code> label perple</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <div fxFlex.gt-md=\"50\" fxFlex=\"100\">\n        <mat-card>\n            <mat-card-content>\n                <mat-card-title>Lebel Light Classes </mat-card-title>\n                <mat-list role=\"list\">\n                   <mat-list-item><code>.label-light-success</code> label light green.</mat-list-item>\n                    <mat-list-item><code>.label-light-info</code> label light Blue</mat-list-item>\n                    <mat-list-item><code>.label-light-warning</code> label light yellow</mat-list-item>\n                    <mat-list-item><code>.label-light-danger</code> label light Red</mat-list-item>\n                    <mat-list-item><code>.label-light-primary</code> label light perple</mat-list-item>\n                </mat-list>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    \n</div>"

/***/ }),

/***/ "./src/app/pages/helper-classes/helper.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/helper-classes/helper.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-list .mat-list-item {\n  height: auto;\n  margin: 5px -16px; }\n  .mat-list .mat-list-item code {\n    flex-shrink: 0;\n    margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/pages/helper-classes/helper.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/helper-classes/helper.component.ts ***!
  \**********************************************************/
/*! exports provided: HelperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelperComponent", function() { return HelperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HelperComponent = /** @class */ (function () {
    function HelperComponent() {
    }
    HelperComponent.prototype.ngOnInit = function () {
    };
    HelperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-helper',
            template: __webpack_require__(/*! ./helper.component.html */ "./src/app/pages/helper-classes/helper.component.html"),
            styles: [__webpack_require__(/*! ./helper.component.scss */ "./src/app/pages/helper-classes/helper.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HelperComponent);
    return HelperComponent;
}());



/***/ }),

/***/ "./src/app/pages/invoice/invoice.component.html":
/*!******************************************************!*\
  !*** ./src/app/pages/invoice/invoice.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n  <mat-card-content>\n    <div class=\"mb-1\">\n      <h1 class=\"mt-0\"><strong>INVOICE</strong></h1>\n      \n    </div>\n\n    <div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\n      <div fxFlex.gt-xs=\"50\" fxFlex=\"100\">\n        <h4 class=\"text-danger font-bold\">Material Pro Admin</h4>\n        <div fxLayout=\"column\">\n          <span>Invoice No: #0001</span>\n          <span>E 104, Dharti-2,</span>\n          <span>Nr' Viswakarma Temple,<br/>Talaja Road, Bhavnagar - 364002</span>\n              \n \n\n        </div>\n      </div>\n      <div fxFlex.gt-xs=\"50\" fxFlex=\"100\" class=\"text-right\">\n        <h4 class=\"font-bold\">Gaala &amp;     Sons,</h4>\n        <div fxLayout=\"column\">\n        <span>company@address.com</span>\n        <span>E 105, ODharti-2,</span>\n          <span>Nr' Viswakarma Temple,<br/>Talaja Road, Ahmedabad - 364002</span>\n      </div>\n      </div>\n    </div>\n  </mat-card-content>\n\n  <mat-card-content class=\"table-responsive\">\n    <ngx-datatable\n      class=\"material\"\n      [rows]=\"invoiceItems\"\n      [columnMode]=\"'flex'\"\n      [headerHeight]=\"50\"\n      [footerHeight]=\"0\"\n      [rowHeight]=\"'auto'\">\n      <ngx-datatable-column name=\"Description\" [flexGrow]=\"4\">\n        <ng-template ngx-datatable-cell-template let-row=\"row\">\n          <strong>{{row.title}}</strong><br/><span class=\"mat-text-muted\">{{row.subtitle}}</span>\n        </ng-template>\n      </ngx-datatable-column>\n      <ngx-datatable-column name=\"Unit Price\" [flexGrow]=\"1\">\n        <ng-template ngx-datatable-cell-template let-row=\"row\">\n          <strong>{{row.price | currency:'USD':'symbol':'1.2-2'}}</strong>\n        </ng-template>\n      </ngx-datatable-column>\n      <ngx-datatable-column name=\"Quantity\" [flexGrow]=\"1\">\n        <ng-template ngx-datatable-cell-template let-row=\"row\">\n          <strong>{{row.quantity}}</strong>\n        </ng-template>\n      </ngx-datatable-column>\n      <ngx-datatable-column name=\"Total\" [flexGrow]=\"1\">\n        <ng-template ngx-datatable-cell-template let-row=\"row\">\n          <strong>{{ (row.price * row.quantity) | currency:'USD':'symbol':'1.2-2'}}</strong>\n        </ng-template>\n      </ngx-datatable-column>\n    </ngx-datatable>\n  </mat-card-content>\n\n  <mat-card-content class=\"bg-light\">\n    <div class=\"d-flex align-items-center\">\n      \n      <div class=\"text-right  ml-auto\">\n        <h6 class=\"text-uppercase text-md ma-0 text-bold\">Subtotal :</h6>  \n        <h6 class=\"text-uppercase text-md ma-0 text-bold\">Tax (2%) :</h6>\n        <h6 class=\"text-uppercase text-sm ma-0 text-bold\">Total :</h6>\n      </div>\n      <div class=\"m-l-10\">\n        <h4 class=\"font-medium\">{{getSubTotal() | currency:'USD':'symbol':'1.2-2'}}</h4> \n        <h4 class=\"font-medium\">{{getCalculatedTax() | currency:'USD':'symbol':'1.2-2'}}</h4>  \n        <h4 class=\"font-medium\">{{ getTotal() | currency:'USD':'symbol':'1.2-2'}}</h4>\n      </div>\n    </div>\n  </mat-card-content>\n  \n  \n</mat-card>\n"

/***/ }),

/***/ "./src/app/pages/invoice/invoice.component.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/invoice/invoice.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/invoice/invoice.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/invoice/invoice.component.ts ***!
  \****************************************************/
/*! exports provided: InvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceComponent", function() { return InvoiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InvoiceComponent = /** @class */ (function () {
    function InvoiceComponent() {
        this.invoiceItems = [{
                'title': 'Ample Admin',
                'subtitle': 'The ultimate admin template',
                'price': 499.00,
                'quantity': 1
            }, {
                'title': 'Material Pro Admin',
                'subtitle': 'Material Based admin template',
                'price': 399.00,
                'quantity': 1
            }, {
                'title': 'Wrapkit',
                'subtitle': 'Bootstrap 4 UI kit',
                'price': 599.00,
                'quantity': 1
            }, {
                'title': 'Admin Wrap',
                'subtitle': 'Free admin template with UI kit',
                'price': 0.00,
                'quantity': 1
            }];
        this.invoiceTotals = [{
                'subtotal': this.getSubTotal(),
                'tax': this.getCalculatedTax(),
                'discount': 0.00,
                'total': 0
            }];
    }
    InvoiceComponent.prototype.getSubTotal = function () {
        var total = 0.00;
        for (var i = 1; i < this.invoiceItems.length; i++) {
            total += (this.invoiceItems[i].price * this.invoiceItems[i].quantity);
        }
        return total;
    };
    InvoiceComponent.prototype.getCalculatedTax = function () {
        return ((2 * this.getSubTotal()) / 100);
    };
    InvoiceComponent.prototype.getTotal = function () {
        return (this.getSubTotal() + this.getCalculatedTax());
    };
    InvoiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-invoice',
            template: __webpack_require__(/*! ./invoice.component.html */ "./src/app/pages/invoice/invoice.component.html"),
            styles: [__webpack_require__(/*! ./invoice.component.scss */ "./src/app/pages/invoice/invoice.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InvoiceComponent);
    return InvoiceComponent;
}());



/***/ }),

/***/ "./src/app/pages/material-icons/mat-icon.component.html":
/*!**************************************************************!*\
  !*** ./src/app/pages/material-icons/mat-icon.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content>\r\n            <mat-card-title>Material Icon list</mat-card-title>    \r\n            <mat-card-subtitle>Simple and easy to use <code>&lt;mat-icon&gt; ... &lt;/mat-icon&gt;</code></mat-card-subtitle>\r\n            <div fxLayout=\"row\" fxLayoutWrap=\"wrap\" >\r\n                <div class=\"icon-box\" *ngFor=\"let maticon of maticons; let i = index\">\r\n                    <mat-icon>{{maticon.iconName}}</mat-icon>\r\n                    <div class=\"text-muted\">{{maticon.iconClass}}</div>\r\n                </div>\r\n            \r\n                </div>    \r\n            </mat-card-content>    \r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/material-icons/mat-icon.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/material-icons/mat-icon.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".icon-box {\n  padding: 5px;\n  margin-bottom: 15px; }\n"

/***/ }),

/***/ "./src/app/pages/material-icons/mat-icon.component.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/material-icons/mat-icon.component.ts ***!
  \************************************************************/
/*! exports provided: MatIconComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MatIconComponent", function() { return MatIconComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _mock_mat_icon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mock-mat-icon */ "./src/app/pages/material-icons/mock-mat-icon.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MatIconComponent = /** @class */ (function () {
    function MatIconComponent() {
        this.maticons = _mock_mat_icon__WEBPACK_IMPORTED_MODULE_1__["MATICONS"];
    }
    MatIconComponent.prototype.ngOnInit = function () {
    };
    MatIconComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-icon',
            template: __webpack_require__(/*! ./mat-icon.component.html */ "./src/app/pages/material-icons/mat-icon.component.html"),
            styles: [__webpack_require__(/*! ./mat-icon.component.scss */ "./src/app/pages/material-icons/mat-icon.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MatIconComponent);
    return MatIconComponent;
}());



/***/ }),

/***/ "./src/app/pages/material-icons/mock-mat-icon.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/material-icons/mock-mat-icon.ts ***!
  \*******************************************************/
/*! exports provided: MATICONS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MATICONS", function() { return MATICONS; });
/*tslint:disable:max-line-length*/
var MATICONS = [{
        iconName: '3d_rotation',
        iconClass: '3d_rotation'
    }, {
        iconName: 'ac_unit',
        iconClass: 'ac_unit'
    }, {
        iconName: 'access_alarm',
        iconClass: 'access_alarm'
    }, {
        iconName: 'access_alarms',
        iconClass: 'access_alarms'
    }, {
        iconName: 'access_time',
        iconClass: 'access_time'
    }, {
        iconName: 'accessibility',
        iconClass: 'accessibility'
    }, {
        iconName: 'accessible',
        iconClass: 'accessible'
    }, {
        iconName: 'account_balance',
        iconClass: 'account_balance'
    }, {
        iconName: 'account_balance_wallet',
        iconClass: 'account_balance_wallet'
    }, {
        iconName: 'account_box',
        iconClass: 'account_box'
    }, {
        iconName: 'account_circle',
        iconClass: 'account_circle'
    }, {
        iconName: 'adb',
        iconClass: 'adb'
    }, {
        iconName: 'add',
        iconClass: 'add'
    }, {
        iconName: 'add_a_photo',
        iconClass: 'add_a_photo'
    }, {
        iconName: 'add_alarm',
        iconClass: 'add_alarm'
    }, {
        iconName: 'add_alert',
        iconClass: 'add_alert'
    }, {
        iconName: 'add_box',
        iconClass: 'add_box'
    }, {
        iconName: 'add_circle',
        iconClass: 'add_circle'
    }, {
        iconName: 'add_circle_outline',
        iconClass: 'add_circle_outline'
    }, {
        iconName: 'add_location',
        iconClass: 'add_location'
    }, {
        iconName: 'add_shopping_cart',
        iconClass: 'add_shopping_cart'
    }, {
        iconName: 'add_to_photos',
        iconClass: 'add_to_photos'
    }, {
        iconName: 'add_to_queue',
        iconClass: 'add_to_queue'
    }, {
        iconName: 'adjust',
        iconClass: 'adjust'
    }, {
        iconName: 'airline_seat_flat',
        iconClass: 'airline_seat_flat'
    }, {
        iconName: 'airline_seat_flat_angled',
        iconClass: 'airline_seat_flat_angled'
    }, {
        iconName: 'airline_seat_individual_suite',
        iconClass: 'airline_seat_individual_suite'
    }, {
        iconName: 'airline_seat_legroom_extra',
        iconClass: 'airline_seat_legroom_extra'
    }, {
        iconName: 'airline_seat_legroom_normal',
        iconClass: 'airline_seat_legroom_normal'
    }, {
        iconName: 'airline_seat_legroom_reduced',
        iconClass: 'airline_seat_legroom_reduced'
    }, {
        iconName: 'airline_seat_recline_extra',
        iconClass: 'airline_seat_recline_extra'
    }, {
        iconName: 'airline_seat_recline_normal',
        iconClass: 'airline_seat_recline_normal'
    }, {
        iconName: 'airplanemode_active',
        iconClass: 'airplanemode_active'
    }, {
        iconName: 'airplanemode_inactive',
        iconClass: 'airplanemode_inactive'
    }, {
        iconName: 'airplay',
        iconClass: 'airplay'
    }, {
        iconName: 'airport_shuttle',
        iconClass: 'airport_shuttle'
    }, {
        iconName: 'alarm',
        iconClass: 'alarm'
    }, {
        iconName: 'alarm_add',
        iconClass: 'alarm_add'
    }, {
        iconName: 'alarm_off',
        iconClass: 'alarm_off'
    }, {
        iconName: 'alarm_on',
        iconClass: 'alarm_on'
    }, {
        iconName: 'album',
        iconClass: 'album'
    }, {
        iconName: 'all_inclusive',
        iconClass: 'all_inclusive'
    }, {
        iconName: 'all_out',
        iconClass: 'all_out'
    }, {
        iconName: 'android',
        iconClass: 'android'
    }, {
        iconName: 'announcement',
        iconClass: 'announcement'
    }, {
        iconName: 'apps',
        iconClass: 'apps'
    }, {
        iconName: 'archive',
        iconClass: 'archive'
    }, {
        iconName: 'arrow_back',
        iconClass: 'arrow_back'
    }, {
        iconName: 'arrow_downward',
        iconClass: 'arrow_downward'
    }, {
        iconName: 'arrow_drop_down',
        iconClass: 'arrow_drop_down'
    }, {
        iconName: 'arrow_drop_down_circle',
        iconClass: 'arrow_drop_down_circle'
    }, {
        iconName: 'arrow_drop_up',
        iconClass: 'arrow_drop_up'
    }, {
        iconName: 'arrow_forward',
        iconClass: 'arrow_forward'
    }, {
        iconName: 'arrow_upward',
        iconClass: 'arrow_upward'
    }, {
        iconName: 'art_track',
        iconClass: 'art_track'
    }, {
        iconName: 'aspect_ratio',
        iconClass: 'aspect_ratio'
    }, {
        iconName: 'assessment',
        iconClass: 'assessment'
    }, {
        iconName: 'assignment',
        iconClass: 'assignment'
    }, {
        iconName: 'assignment_ind',
        iconClass: 'assignment_ind'
    }, {
        iconName: 'assignment_late',
        iconClass: 'assignment_late'
    }, {
        iconName: 'assignment_return',
        iconClass: 'assignment_return'
    }, {
        iconName: 'assignment_returned',
        iconClass: 'assignment_returned'
    }, {
        iconName: 'assignment_turned_in',
        iconClass: 'assignment_turned_in'
    }, {
        iconName: 'assistant',
        iconClass: 'assistant'
    }, {
        iconName: 'assistant_photo',
        iconClass: 'assistant_photo'
    }, {
        iconName: 'attach_file',
        iconClass: 'attach_file'
    }, {
        iconName: 'attach_money',
        iconClass: 'attach_money'
    }, {
        iconName: 'attachment',
        iconClass: 'attachment'
    }, {
        iconName: 'audiotrack',
        iconClass: 'audiotrack'
    }, {
        iconName: 'autorenew',
        iconClass: 'autorenew'
    }, {
        iconName: 'av_timer',
        iconClass: 'av_timer'
    }, {
        iconName: 'backspace',
        iconClass: 'backspace'
    }, {
        iconName: 'backup',
        iconClass: 'backup'
    }, {
        iconName: 'battery_alert',
        iconClass: 'battery_alert'
    }, {
        iconName: 'battery_charging_full',
        iconClass: 'battery_charging_full'
    }, {
        iconName: 'battery_full',
        iconClass: 'battery_full'
    }, {
        iconName: 'battery_std',
        iconClass: 'battery_std'
    }, {
        iconName: 'battery_unknown',
        iconClass: 'battery_unknown'
    }, {
        iconName: 'beach_access',
        iconClass: 'beach_access'
    }, {
        iconName: 'beenhere',
        iconClass: 'beenhere'
    }, {
        iconName: 'block',
        iconClass: 'block'
    }, {
        iconName: 'bluetooth',
        iconClass: 'bluetooth'
    }, {
        iconName: 'bluetooth_audio',
        iconClass: 'bluetooth_audio'
    }, {
        iconName: 'bluetooth_connected',
        iconClass: 'bluetooth_connected'
    }, {
        iconName: 'bluetooth_disabled',
        iconClass: 'bluetooth_disabled'
    }, {
        iconName: 'bluetooth_searching',
        iconClass: 'bluetooth_searching'
    }, {
        iconName: 'blur_circular',
        iconClass: 'blur_circular'
    }, {
        iconName: 'blur_linear',
        iconClass: 'blur_linear'
    }, {
        iconName: 'blur_off',
        iconClass: 'blur_off'
    }, {
        iconName: 'blur_on',
        iconClass: 'blur_on'
    }, {
        iconName: 'book',
        iconClass: 'book'
    }, {
        iconName: 'bookmark',
        iconClass: 'bookmark'
    }, {
        iconName: 'bookmark_border',
        iconClass: 'bookmark_border'
    }, {
        iconName: 'border_all',
        iconClass: 'border_all'
    }, {
        iconName: 'border_bottom',
        iconClass: 'border_bottom'
    }, {
        iconName: 'border_clear',
        iconClass: 'border_clear'
    }, {
        iconName: 'border_color',
        iconClass: 'border_color'
    }, {
        iconName: 'border_horizontal',
        iconClass: 'border_horizontal'
    }, {
        iconName: 'border_inner',
        iconClass: 'border_inner'
    }, {
        iconName: 'border_left',
        iconClass: 'border_left'
    }, {
        iconName: 'border_outer',
        iconClass: 'border_outer'
    }, {
        iconName: 'border_right',
        iconClass: 'border_right'
    }, {
        iconName: 'border_style',
        iconClass: 'border_style'
    }, {
        iconName: 'border_top',
        iconClass: 'border_top'
    }, {
        iconName: 'border_vertical',
        iconClass: 'border_vertical'
    }, {
        iconName: 'branding_watermark',
        iconClass: 'branding_watermark'
    }, {
        iconName: 'brightness_1',
        iconClass: 'brightness_1'
    }, {
        iconName: 'brightness_2',
        iconClass: 'brightness_2'
    }, {
        iconName: 'brightness_3',
        iconClass: 'brightness_3'
    }, {
        iconName: 'brightness_4',
        iconClass: 'brightness_4'
    }, {
        iconName: 'brightness_5',
        iconClass: 'brightness_5'
    }, {
        iconName: 'brightness_6',
        iconClass: 'brightness_6'
    }, {
        iconName: 'brightness_7',
        iconClass: 'brightness_7'
    }, {
        iconName: 'brightness_auto',
        iconClass: 'brightness_auto'
    }, {
        iconName: 'brightness_high',
        iconClass: 'brightness_high'
    }, {
        iconName: 'brightness_low',
        iconClass: 'brightness_low'
    }, {
        iconName: 'brightness_medium',
        iconClass: 'brightness_medium'
    }, {
        iconName: 'broken_image',
        iconClass: 'broken_image'
    }, {
        iconName: 'brush',
        iconClass: 'brush'
    }, {
        iconName: 'bubble_chart',
        iconClass: 'bubble_chart'
    }, {
        iconName: 'bug_report',
        iconClass: 'bug_report'
    }, {
        iconName: 'build',
        iconClass: 'build'
    }, {
        iconName: 'burst_mode',
        iconClass: 'burst_mode'
    }, {
        iconName: 'business',
        iconClass: 'business'
    }, {
        iconName: 'business_center',
        iconClass: 'business_center'
    }, {
        iconName: 'cached',
        iconClass: 'cached'
    }, {
        iconName: 'cake',
        iconClass: 'cake'
    }, {
        iconName: 'call',
        iconClass: 'call'
    }, {
        iconName: 'call_end',
        iconClass: 'call_end'
    }, {
        iconName: 'call_made',
        iconClass: 'call_made'
    }, {
        iconName: 'call_merge',
        iconClass: 'call_merge'
    }, {
        iconName: 'call_missed',
        iconClass: 'call_missed'
    }, {
        iconName: 'call_missed_outgoing',
        iconClass: 'call_missed_outgoing'
    }, {
        iconName: 'call_received',
        iconClass: 'call_received'
    }, {
        iconName: 'call_split',
        iconClass: 'call_split'
    }, {
        iconName: 'call_to_action',
        iconClass: 'call_to_action'
    }, {
        iconName: 'camera',
        iconClass: 'camera'
    }, {
        iconName: 'camera_alt',
        iconClass: 'camera_alt'
    }, {
        iconName: 'camera_enhance',
        iconClass: 'camera_enhance'
    }, {
        iconName: 'camera_front',
        iconClass: 'camera_front'
    }, {
        iconName: 'camera_rear',
        iconClass: 'camera_rear'
    }, {
        iconName: 'camera_roll',
        iconClass: 'camera_roll'
    }, {
        iconName: 'cancel',
        iconClass: 'cancel'
    }, {
        iconName: 'card_giftcard',
        iconClass: 'card_giftcard'
    }, {
        iconName: 'card_membership',
        iconClass: 'card_membership'
    }, {
        iconName: 'card_travel',
        iconClass: 'card_travel'
    }, {
        iconName: 'casino',
        iconClass: 'casino'
    }, {
        iconName: 'cast',
        iconClass: 'cast'
    }, {
        iconName: 'cast_connected',
        iconClass: 'cast_connected'
    }, {
        iconName: 'center_focus_strong',
        iconClass: 'center_focus_strong'
    }, {
        iconName: 'center_focus_weak',
        iconClass: 'center_focus_weak'
    }, {
        iconName: 'change_history',
        iconClass: 'change_history'
    }, {
        iconName: 'chat',
        iconClass: 'chat'
    }, {
        iconName: 'chat_bubble',
        iconClass: 'chat_bubble'
    }, {
        iconName: 'chat_bubble_outline',
        iconClass: 'chat_bubble_outline'
    }, {
        iconName: 'check',
        iconClass: 'check'
    }, {
        iconName: 'check_box',
        iconClass: 'check_box'
    }, {
        iconName: 'check_box_outline_blank',
        iconClass: 'check_box_outline_blank'
    }, {
        iconName: 'check_circle',
        iconClass: 'check_circle'
    }, {
        iconName: 'chevron_left',
        iconClass: 'chevron_left'
    }, {
        iconName: 'chevron_right',
        iconClass: 'chevron_right'
    }, {
        iconName: 'child_care',
        iconClass: 'child_care'
    }, {
        iconName: 'child_friendly',
        iconClass: 'child_friendly'
    }, {
        iconName: 'chrome_reader_mode',
        iconClass: 'chrome_reader_mode'
    }, {
        iconName: 'class',
        iconClass: 'class'
    }, {
        iconName: 'clear',
        iconClass: 'clear'
    }, {
        iconName: 'clear_all',
        iconClass: 'clear_all'
    }, {
        iconName: 'close',
        iconClass: 'close'
    }, {
        iconName: 'closed_caption',
        iconClass: 'closed_caption'
    }, {
        iconName: 'cloud',
        iconClass: 'cloud'
    }, {
        iconName: 'cloud_circle',
        iconClass: 'cloud_circle'
    }, {
        iconName: 'cloud_done',
        iconClass: 'cloud_done'
    }, {
        iconName: 'cloud_download',
        iconClass: 'cloud_download'
    }, {
        iconName: 'cloud_off',
        iconClass: 'cloud_off'
    }, {
        iconName: 'cloud_queue',
        iconClass: 'cloud_queue'
    }, {
        iconName: 'cloud_upload',
        iconClass: 'cloud_upload'
    }, {
        iconName: 'code',
        iconClass: 'code'
    }, {
        iconName: 'collections',
        iconClass: 'collections'
    }, {
        iconName: 'collections_bookmark',
        iconClass: 'collections_bookmark'
    }, {
        iconName: 'color_lens',
        iconClass: 'color_lens'
    }, {
        iconName: 'colorize',
        iconClass: 'colorize'
    }, {
        iconName: 'comment',
        iconClass: 'comment'
    }, {
        iconName: 'compare',
        iconClass: 'compare'
    }, {
        iconName: 'compare_arrows',
        iconClass: 'compare_arrows'
    }, {
        iconName: 'computer',
        iconClass: 'computer'
    }, {
        iconName: 'confirmation_number',
        iconClass: 'confirmation_number'
    }, {
        iconName: 'contact_mail',
        iconClass: 'contact_mail'
    }, {
        iconName: 'contact_phone',
        iconClass: 'contact_phone'
    }, {
        iconName: 'contacts',
        iconClass: 'contacts'
    }, {
        iconName: 'content_copy',
        iconClass: 'content_copy'
    }, {
        iconName: 'content_cut',
        iconClass: 'content_cut'
    }, {
        iconName: 'content_paste',
        iconClass: 'content_paste'
    }, {
        iconName: 'control_point',
        iconClass: 'control_point'
    }, {
        iconName: 'control_point_duplicate',
        iconClass: 'control_point_duplicate'
    }, {
        iconName: 'copyright',
        iconClass: 'copyright'
    }, {
        iconName: 'create',
        iconClass: 'create'
    }, {
        iconName: 'create_new_folder',
        iconClass: 'create_new_folder'
    }, {
        iconName: 'credit_card',
        iconClass: 'credit_card'
    }, {
        iconName: 'crop',
        iconClass: 'crop'
    }, {
        iconName: 'crop_16_9',
        iconClass: 'crop_16_9'
    }, {
        iconName: 'crop_3_2',
        iconClass: 'crop_3_2'
    }, {
        iconName: 'crop_5_4',
        iconClass: 'crop_5_4'
    }, {
        iconName: 'crop_7_5',
        iconClass: 'crop_7_5'
    }, {
        iconName: 'crop_din',
        iconClass: 'crop_din'
    }, {
        iconName: 'crop_free',
        iconClass: 'crop_free'
    }, {
        iconName: 'crop_landscape',
        iconClass: 'crop_landscape'
    }, {
        iconName: 'crop_original',
        iconClass: 'crop_original'
    }, {
        iconName: 'crop_portrait',
        iconClass: 'crop_portrait'
    }, {
        iconName: 'crop_rotate',
        iconClass: 'crop_rotate'
    }, {
        iconName: 'crop_square',
        iconClass: 'crop_square'
    }, {
        iconName: 'dashboard',
        iconClass: 'dashboard'
    }, {
        iconName: 'data_usage',
        iconClass: 'data_usage'
    }, {
        iconName: 'date_range',
        iconClass: 'date_range'
    }, {
        iconName: 'dehaze',
        iconClass: 'dehaze'
    }, {
        iconName: 'delete',
        iconClass: 'delete'
    }, {
        iconName: 'delete_forever',
        iconClass: 'delete_forever'
    }, {
        iconName: 'delete_sweep',
        iconClass: 'delete_sweep'
    }, {
        iconName: 'description',
        iconClass: 'description'
    }, {
        iconName: 'desktop_mac',
        iconClass: 'desktop_mac'
    }, {
        iconName: 'desktop_windows',
        iconClass: 'desktop_windows'
    }, {
        iconName: 'details',
        iconClass: 'details'
    }, {
        iconName: 'developer_board',
        iconClass: 'developer_board'
    }, {
        iconName: 'developer_mode',
        iconClass: 'developer_mode'
    }, {
        iconName: 'device_hub',
        iconClass: 'device_hub'
    }, {
        iconName: 'devices',
        iconClass: 'devices'
    }, {
        iconName: 'devices_other',
        iconClass: 'devices_other'
    }, {
        iconName: 'dialer_sip',
        iconClass: 'dialer_sip'
    }, {
        iconName: 'dialpad',
        iconClass: 'dialpad'
    }, {
        iconName: 'directions',
        iconClass: 'directions'
    }, {
        iconName: 'directions_bike',
        iconClass: 'directions_bike'
    }, {
        iconName: 'directions_boat',
        iconClass: 'directions_boat'
    }, {
        iconName: 'directions_bus',
        iconClass: 'directions_bus'
    }, {
        iconName: 'directions_car',
        iconClass: 'directions_car'
    }, {
        iconName: 'directions_railway',
        iconClass: 'directions_railway'
    }, {
        iconName: 'directions_run',
        iconClass: 'directions_run'
    }, {
        iconName: 'directions_subway',
        iconClass: 'directions_subway'
    }, {
        iconName: 'directions_transit',
        iconClass: 'directions_transit'
    }, {
        iconName: 'directions_walk',
        iconClass: 'directions_walk'
    }, {
        iconName: 'disc_full',
        iconClass: 'disc_full'
    }, {
        iconName: 'dns',
        iconClass: 'dns'
    }, {
        iconName: 'do_not_disturb',
        iconClass: 'do_not_disturb'
    }, {
        iconName: 'do_not_disturb_alt',
        iconClass: 'do_not_disturb_alt'
    }, {
        iconName: 'do_not_disturb_off',
        iconClass: 'do_not_disturb_off'
    }, {
        iconName: 'do_not_disturb_on',
        iconClass: 'do_not_disturb_on'
    }, {
        iconName: 'dock',
        iconClass: 'dock'
    }, {
        iconName: 'domain',
        iconClass: 'domain'
    }, {
        iconName: 'done',
        iconClass: 'done'
    }, {
        iconName: 'done_all',
        iconClass: 'done_all'
    }, {
        iconName: 'donut_large',
        iconClass: 'donut_large'
    }, {
        iconName: 'donut_small',
        iconClass: 'donut_small'
    }, {
        iconName: 'drafts',
        iconClass: 'drafts'
    }, {
        iconName: 'drag_handle',
        iconClass: 'drag_handle'
    }, {
        iconName: 'drive_eta',
        iconClass: 'drive_eta'
    }, {
        iconName: 'dvr',
        iconClass: 'dvr'
    }, {
        iconName: 'edit',
        iconClass: 'edit'
    }, {
        iconName: 'edit_location',
        iconClass: 'edit_location'
    }, {
        iconName: 'eject',
        iconClass: 'eject'
    }, {
        iconName: 'email',
        iconClass: 'email'
    }, {
        iconName: 'enhanced_encryption',
        iconClass: 'enhanced_encryption'
    }, {
        iconName: 'equalizer',
        iconClass: 'equalizer'
    }, {
        iconName: 'error',
        iconClass: 'error'
    }, {
        iconName: 'error_outline',
        iconClass: 'error_outline'
    }, {
        iconName: 'euro_symbol',
        iconClass: 'euro_symbol'
    }, {
        iconName: 'ev_station',
        iconClass: 'ev_station'
    }, {
        iconName: 'event',
        iconClass: 'event'
    }, {
        iconName: 'event_available',
        iconClass: 'event_available'
    }, {
        iconName: 'event_busy',
        iconClass: 'event_busy'
    }, {
        iconName: 'event_note',
        iconClass: 'event_note'
    }, {
        iconName: 'event_seat',
        iconClass: 'event_seat'
    }, {
        iconName: 'exit_to_app',
        iconClass: 'exit_to_app'
    }, {
        iconName: 'expand_less',
        iconClass: 'expand_less'
    }, {
        iconName: 'expand_more',
        iconClass: 'expand_more'
    }, {
        iconName: 'explicit',
        iconClass: 'explicit'
    }, {
        iconName: 'explore',
        iconClass: 'explore'
    }, {
        iconName: 'exposure',
        iconClass: 'exposure'
    }, {
        iconName: 'exposure_neg_1',
        iconClass: 'exposure_neg_1'
    }, {
        iconName: 'exposure_neg_2',
        iconClass: 'exposure_neg_2'
    }, {
        iconName: 'exposure_plus_1',
        iconClass: 'exposure_plus_1'
    }, {
        iconName: 'exposure_plus_2',
        iconClass: 'exposure_plus_2'
    }, {
        iconName: 'exposure_zero',
        iconClass: 'exposure_zero'
    }, {
        iconName: 'extension',
        iconClass: 'extension'
    }, {
        iconName: 'face',
        iconClass: 'face'
    }, {
        iconName: 'fast_forward',
        iconClass: 'fast_forward'
    }, {
        iconName: 'fast_rewind',
        iconClass: 'fast_rewind'
    }, {
        iconName: 'favorite',
        iconClass: 'favorite'
    }, {
        iconName: 'favorite_border',
        iconClass: 'favorite_border'
    }, {
        iconName: 'featured_play_list',
        iconClass: 'featured_play_list'
    }, {
        iconName: 'featured_video',
        iconClass: 'featured_video'
    }, {
        iconName: 'feedback',
        iconClass: 'feedback'
    }, {
        iconName: 'fiber_dvr',
        iconClass: 'fiber_dvr'
    }, {
        iconName: 'fiber_manual_record',
        iconClass: 'fiber_manual_record'
    }, {
        iconName: 'fiber_new',
        iconClass: 'fiber_new'
    }, {
        iconName: 'fiber_pin',
        iconClass: 'fiber_pin'
    }, {
        iconName: 'fiber_smart_record',
        iconClass: 'fiber_smart_record'
    }, {
        iconName: 'file_download',
        iconClass: 'file_download'
    }, {
        iconName: 'file_upload',
        iconClass: 'file_upload'
    }, {
        iconName: 'filter',
        iconClass: 'filter'
    }, {
        iconName: 'filter_1',
        iconClass: 'filter_1'
    }, {
        iconName: 'filter_2',
        iconClass: 'filter_2'
    }, {
        iconName: 'filter_3',
        iconClass: 'filter_3'
    }, {
        iconName: 'filter_4',
        iconClass: 'filter_4'
    }, {
        iconName: 'filter_5',
        iconClass: 'filter_5'
    }, {
        iconName: 'filter_6',
        iconClass: 'filter_6'
    }, {
        iconName: 'filter_7',
        iconClass: 'filter_7'
    }, {
        iconName: 'filter_8',
        iconClass: 'filter_8'
    }, {
        iconName: 'filter_9',
        iconClass: 'filter_9'
    }, {
        iconName: 'filter_9_plus',
        iconClass: 'filter_9_plus'
    }, {
        iconName: 'filter_b_and_w',
        iconClass: 'filter_b_and_w'
    }, {
        iconName: 'filter_center_focus',
        iconClass: 'filter_center_focus'
    }, {
        iconName: 'filter_drama',
        iconClass: 'filter_drama'
    }, {
        iconName: 'filter_frames',
        iconClass: 'filter_frames'
    }, {
        iconName: 'filter_hdr',
        iconClass: 'filter_hdr'
    }, {
        iconName: 'filter_list',
        iconClass: 'filter_list'
    }, {
        iconName: 'filter_none',
        iconClass: 'filter_none'
    }, {
        iconName: 'filter_tilt_shift',
        iconClass: 'filter_tilt_shift'
    }, {
        iconName: 'filter_vintage',
        iconClass: 'filter_vintage'
    }, {
        iconName: 'find_in_page',
        iconClass: 'find_in_page'
    }, {
        iconName: 'find_replace',
        iconClass: 'find_replace'
    }, {
        iconName: 'fingerprint',
        iconClass: 'fingerprint'
    }, {
        iconName: 'first_page',
        iconClass: 'first_page'
    }, {
        iconName: 'fitness_center',
        iconClass: 'fitness_center'
    }, {
        iconName: 'flag',
        iconClass: 'flag'
    }, {
        iconName: 'flare',
        iconClass: 'flare'
    }, {
        iconName: 'flash_auto',
        iconClass: 'flash_auto'
    }, {
        iconName: 'flash_off',
        iconClass: 'flash_off'
    }, {
        iconName: 'flash_on',
        iconClass: 'flash_on'
    }, {
        iconName: 'flight',
        iconClass: 'flight'
    }, {
        iconName: 'flight_land',
        iconClass: 'flight_land'
    }, {
        iconName: 'flight_takeoff',
        iconClass: 'flight_takeoff'
    }, {
        iconName: 'flip',
        iconClass: 'flip'
    }, {
        iconName: 'flip_to_back',
        iconClass: 'flip_to_back'
    }, {
        iconName: 'flip_to_front',
        iconClass: 'flip_to_front'
    }, {
        iconName: 'folder',
        iconClass: 'folder'
    }, {
        iconName: 'folder_open',
        iconClass: 'folder_open'
    }, {
        iconName: 'folder_shared',
        iconClass: 'folder_shared'
    }, {
        iconName: 'folder_special',
        iconClass: 'folder_special'
    }, {
        iconName: 'font_download',
        iconClass: 'font_download'
    }, {
        iconName: 'format_align_center',
        iconClass: 'format_align_center'
    }, {
        iconName: 'format_align_justify',
        iconClass: 'format_align_justify'
    }, {
        iconName: 'format_align_left',
        iconClass: 'format_align_left'
    }, {
        iconName: 'format_align_right',
        iconClass: 'format_align_right'
    }, {
        iconName: 'format_bold',
        iconClass: 'format_bold'
    }, {
        iconName: 'format_clear',
        iconClass: 'format_clear'
    }, {
        iconName: 'format_color_fill',
        iconClass: 'format_color_fill'
    }, {
        iconName: 'format_color_reset',
        iconClass: 'format_color_reset'
    }, {
        iconName: 'format_color_text',
        iconClass: 'format_color_text'
    }, {
        iconName: 'format_indent_decrease',
        iconClass: 'format_indent_decrease'
    }, {
        iconName: 'format_indent_increase',
        iconClass: 'format_indent_increase'
    }, {
        iconName: 'format_italic',
        iconClass: 'format_italic'
    }, {
        iconName: 'format_line_spacing',
        iconClass: 'format_line_spacing'
    }, {
        iconName: 'format_list_bulleted',
        iconClass: 'format_list_bulleted'
    }, {
        iconName: 'format_list_numbered',
        iconClass: 'format_list_numbered'
    }, {
        iconName: 'format_paint',
        iconClass: 'format_paint'
    }, {
        iconName: 'format_quote',
        iconClass: 'format_quote'
    }, {
        iconName: 'format_shapes',
        iconClass: 'format_shapes'
    }, {
        iconName: 'format_size',
        iconClass: 'format_size'
    }, {
        iconName: 'format_strikethrough',
        iconClass: 'format_strikethrough'
    }, {
        iconName: 'format_textdirection_l_to_r',
        iconClass: 'format_textdirection_l_to_r'
    }, {
        iconName: 'format_textdirection_r_to_l',
        iconClass: 'format_textdirection_r_to_l'
    }, {
        iconName: 'format_underlined',
        iconClass: 'format_underlined'
    }, {
        iconName: 'forum',
        iconClass: 'forum'
    }, {
        iconName: 'forward',
        iconClass: 'forward'
    }, {
        iconName: 'forward_10',
        iconClass: 'forward_10'
    }, {
        iconName: 'forward_30',
        iconClass: 'forward_30'
    }, {
        iconName: 'forward_5',
        iconClass: 'forward_5'
    }, {
        iconName: 'free_breakfast',
        iconClass: 'free_breakfast'
    }, {
        iconName: 'fullscreen',
        iconClass: 'fullscreen'
    }, {
        iconName: 'fullscreen_exit',
        iconClass: 'fullscreen_exit'
    }, {
        iconName: 'functions',
        iconClass: 'functions'
    }, {
        iconName: 'g_translate',
        iconClass: 'g_translate'
    }, {
        iconName: 'gamepad',
        iconClass: 'gamepad'
    }, {
        iconName: 'games',
        iconClass: 'games'
    }, {
        iconName: 'gavel',
        iconClass: 'gavel'
    }, {
        iconName: 'gesture',
        iconClass: 'gesture'
    }, {
        iconName: 'get_app',
        iconClass: 'get_app'
    }, {
        iconName: 'gif',
        iconClass: 'gif'
    }, {
        iconName: 'golf_course',
        iconClass: 'golf_course'
    }, {
        iconName: 'gps_fixed',
        iconClass: 'gps_fixed'
    }, {
        iconName: 'gps_not_fixed',
        iconClass: 'gps_not_fixed'
    }, {
        iconName: 'gps_off',
        iconClass: 'gps_off'
    }, {
        iconName: 'grade',
        iconClass: 'grade'
    }, {
        iconName: 'gradient',
        iconClass: 'gradient'
    }, {
        iconName: 'grain',
        iconClass: 'grain'
    }, {
        iconName: 'graphic_eq',
        iconClass: 'graphic_eq'
    }, {
        iconName: 'grid_off',
        iconClass: 'grid_off'
    }, {
        iconName: 'grid_on',
        iconClass: 'grid_on'
    }, {
        iconName: 'group',
        iconClass: 'group'
    }, {
        iconName: 'group_add',
        iconClass: 'group_add'
    }, {
        iconName: 'group_work',
        iconClass: 'group_work'
    }, {
        iconName: 'hd',
        iconClass: 'hd'
    }, {
        iconName: 'hdr_off',
        iconClass: 'hdr_off'
    }, {
        iconName: 'hdr_on',
        iconClass: 'hdr_on'
    }, {
        iconName: 'hdr_strong',
        iconClass: 'hdr_strong'
    }, {
        iconName: 'hdr_weak',
        iconClass: 'hdr_weak'
    }, {
        iconName: 'headset',
        iconClass: 'headset'
    }, {
        iconName: 'headset_mic',
        iconClass: 'headset_mic'
    }, {
        iconName: 'healing',
        iconClass: 'healing'
    }, {
        iconName: 'hearing',
        iconClass: 'hearing'
    }, {
        iconName: 'help',
        iconClass: 'help'
    }, {
        iconName: 'help_outline',
        iconClass: 'help_outline'
    }, {
        iconName: 'high_quality',
        iconClass: 'high_quality'
    }, {
        iconName: 'highlight',
        iconClass: 'highlight'
    }, {
        iconName: 'highlight_off',
        iconClass: 'highlight_off'
    }, {
        iconName: 'history',
        iconClass: 'history'
    }, {
        iconName: 'home',
        iconClass: 'home'
    }, {
        iconName: 'hot_tub',
        iconClass: 'hot_tub'
    }, {
        iconName: 'hotel',
        iconClass: 'hotel'
    }, {
        iconName: 'hourglass_empty',
        iconClass: 'hourglass_empty'
    }, {
        iconName: 'hourglass_full',
        iconClass: 'hourglass_full'
    }, {
        iconName: 'http',
        iconClass: 'http'
    }, {
        iconName: 'https',
        iconClass: 'https'
    }, {
        iconName: 'image',
        iconClass: 'image'
    }, {
        iconName: 'image_aspect_ratio',
        iconClass: 'image_aspect_ratio'
    }, {
        iconName: 'import_contacts',
        iconClass: 'import_contacts'
    }, {
        iconName: 'import_export',
        iconClass: 'import_export'
    }, {
        iconName: 'important_devices',
        iconClass: 'important_devices'
    }, {
        iconName: 'inbox',
        iconClass: 'inbox'
    }, {
        iconName: 'indeterminate_check_box',
        iconClass: 'indeterminate_check_box'
    }, {
        iconName: 'info',
        iconClass: 'info'
    }, {
        iconName: 'info_outline',
        iconClass: 'info_outline'
    }, {
        iconName: 'input',
        iconClass: 'input'
    }, {
        iconName: 'insert_chart',
        iconClass: 'insert_chart'
    }, {
        iconName: 'insert_comment',
        iconClass: 'insert_comment'
    }, {
        iconName: 'insert_drive_file',
        iconClass: 'insert_drive_file'
    }, {
        iconName: 'insert_emoticon',
        iconClass: 'insert_emoticon'
    }, {
        iconName: 'insert_invitation',
        iconClass: 'insert_invitation'
    }, {
        iconName: 'insert_link',
        iconClass: 'insert_link'
    }, {
        iconName: 'insert_photo',
        iconClass: 'insert_photo'
    }, {
        iconName: 'invert_colors',
        iconClass: 'invert_colors'
    }, {
        iconName: 'invert_colors_off',
        iconClass: 'invert_colors_off'
    }, {
        iconName: 'iso',
        iconClass: 'iso'
    }, {
        iconName: 'keyboard',
        iconClass: 'keyboard'
    }, {
        iconName: 'keyboard_arrow_down',
        iconClass: 'keyboard_arrow_down'
    }, {
        iconName: 'keyboard_arrow_left',
        iconClass: 'keyboard_arrow_left'
    }, {
        iconName: 'keyboard_arrow_right',
        iconClass: 'keyboard_arrow_right'
    }, {
        iconName: 'keyboard_arrow_up',
        iconClass: 'keyboard_arrow_up'
    }, {
        iconName: 'keyboard_backspace',
        iconClass: 'keyboard_backspace'
    }, {
        iconName: 'keyboard_capslock',
        iconClass: 'keyboard_capslock'
    }, {
        iconName: 'keyboard_hide',
        iconClass: 'keyboard_hide'
    }, {
        iconName: 'keyboard_return',
        iconClass: 'keyboard_return'
    }, {
        iconName: 'keyboard_tab',
        iconClass: 'keyboard_tab'
    }, {
        iconName: 'keyboard_voice',
        iconClass: 'keyboard_voice'
    }, {
        iconName: 'kitchen',
        iconClass: 'kitchen'
    }, {
        iconName: 'label',
        iconClass: 'label'
    }, {
        iconName: 'label_outline',
        iconClass: 'label_outline'
    }, {
        iconName: 'landscape',
        iconClass: 'landscape'
    }, {
        iconName: 'language',
        iconClass: 'language'
    }, {
        iconName: 'laptop',
        iconClass: 'laptop'
    }, {
        iconName: 'laptop_chromebook',
        iconClass: 'laptop_chromebook'
    }, {
        iconName: 'laptop_mac',
        iconClass: 'laptop_mac'
    }, {
        iconName: 'laptop_windows',
        iconClass: 'laptop_windows'
    }, {
        iconName: 'last_page',
        iconClass: 'last_page'
    }, {
        iconName: 'launch',
        iconClass: 'launch'
    }, {
        iconName: 'layers',
        iconClass: 'layers'
    }, {
        iconName: 'layers_clear',
        iconClass: 'layers_clear'
    }, {
        iconName: 'leak_add',
        iconClass: 'leak_add'
    }, {
        iconName: 'leak_remove',
        iconClass: 'leak_remove'
    }, {
        iconName: 'lens',
        iconClass: 'lens'
    }, {
        iconName: 'library_add',
        iconClass: 'library_add'
    }, {
        iconName: 'library_books',
        iconClass: 'library_books'
    }, {
        iconName: 'library_music',
        iconClass: 'library_music'
    }, {
        iconName: 'lightbulb_outline',
        iconClass: 'lightbulb_outline'
    }, {
        iconName: 'line_style',
        iconClass: 'line_style'
    }, {
        iconName: 'line_weight',
        iconClass: 'line_weight'
    }, {
        iconName: 'linear_scale',
        iconClass: 'linear_scale'
    }, {
        iconName: 'link',
        iconClass: 'link'
    }, {
        iconName: 'linked_camera',
        iconClass: 'linked_camera'
    }, {
        iconName: 'list',
        iconClass: 'list'
    }, {
        iconName: 'live_help',
        iconClass: 'live_help'
    }, {
        iconName: 'live_tv',
        iconClass: 'live_tv'
    }, {
        iconName: 'local_activity',
        iconClass: 'local_activity'
    }, {
        iconName: 'local_airport',
        iconClass: 'local_airport'
    }, {
        iconName: 'local_atm',
        iconClass: 'local_atm'
    }, {
        iconName: 'local_bar',
        iconClass: 'local_bar'
    }, {
        iconName: 'local_cafe',
        iconClass: 'local_cafe'
    }, {
        iconName: 'local_car_wash',
        iconClass: 'local_car_wash'
    }, {
        iconName: 'local_convenience_store',
        iconClass: 'local_convenience_store'
    }, {
        iconName: 'local_dining',
        iconClass: 'local_dining'
    }, {
        iconName: 'local_drink',
        iconClass: 'local_drink'
    }, {
        iconName: 'local_florist',
        iconClass: 'local_florist'
    }, {
        iconName: 'local_gas_station',
        iconClass: 'local_gas_station'
    }, {
        iconName: 'local_grocery_store',
        iconClass: 'local_grocery_store'
    }, {
        iconName: 'local_hospital',
        iconClass: 'local_hospital'
    }, {
        iconName: 'local_hotel',
        iconClass: 'local_hotel'
    }, {
        iconName: 'local_laundry_service',
        iconClass: 'local_laundry_service'
    }, {
        iconName: 'local_library',
        iconClass: 'local_library'
    }, {
        iconName: 'local_mall',
        iconClass: 'local_mall'
    }, {
        iconName: 'local_movies',
        iconClass: 'local_movies'
    }, {
        iconName: 'local_offer',
        iconClass: 'local_offer'
    }, {
        iconName: 'local_parking',
        iconClass: 'local_parking'
    }, {
        iconName: 'local_pharmacy',
        iconClass: 'local_pharmacy'
    }, {
        iconName: 'local_phone',
        iconClass: 'local_phone'
    }, {
        iconName: 'local_pizza',
        iconClass: 'local_pizza'
    }, {
        iconName: 'local_play',
        iconClass: 'local_play'
    }, {
        iconName: 'local_post_office',
        iconClass: 'local_post_office'
    }, {
        iconName: 'local_printshop',
        iconClass: 'local_printshop'
    }, {
        iconName: 'local_see',
        iconClass: 'local_see'
    }, {
        iconName: 'local_shipping',
        iconClass: 'local_shipping'
    }, {
        iconName: 'local_taxi',
        iconClass: 'local_taxi'
    }, {
        iconName: 'location_city',
        iconClass: 'location_city'
    }, {
        iconName: 'location_disabled',
        iconClass: 'location_disabled'
    }, {
        iconName: 'location_off',
        iconClass: 'location_off'
    }, {
        iconName: 'location_on',
        iconClass: 'location_on'
    }, {
        iconName: 'location_searching',
        iconClass: 'location_searching'
    }, {
        iconName: 'lock',
        iconClass: 'lock'
    }, {
        iconName: 'lock_open',
        iconClass: 'lock_open'
    }, {
        iconName: 'lock_outline',
        iconClass: 'lock_outline'
    }, {
        iconName: 'looks',
        iconClass: 'looks'
    }, {
        iconName: 'looks_3',
        iconClass: 'looks_3'
    }, {
        iconName: 'looks_4',
        iconClass: 'looks_4'
    }, {
        iconName: 'looks_5',
        iconClass: 'looks_5'
    }, {
        iconName: 'looks_6',
        iconClass: 'looks_6'
    }, {
        iconName: 'looks_one',
        iconClass: 'looks_one'
    }, {
        iconName: 'looks_two',
        iconClass: 'looks_two'
    }, {
        iconName: 'loop',
        iconClass: 'loop'
    }, {
        iconName: 'loupe',
        iconClass: 'loupe'
    }, {
        iconName: 'low_priority',
        iconClass: 'low_priority'
    }, {
        iconName: 'loyalty',
        iconClass: 'loyalty'
    }, {
        iconName: 'mail',
        iconClass: 'mail'
    }, {
        iconName: 'mail_outline',
        iconClass: 'mail_outline'
    }, {
        iconName: 'map',
        iconClass: 'map'
    }, {
        iconName: 'markunread',
        iconClass: 'markunread'
    }, {
        iconName: 'markunread_mailbox',
        iconClass: 'markunread_mailbox'
    }, {
        iconName: 'memory',
        iconClass: 'memory'
    }, {
        iconName: 'menu',
        iconClass: 'menu'
    }, {
        iconName: 'merge_type',
        iconClass: 'merge_type'
    }, {
        iconName: 'message',
        iconClass: 'message'
    }, {
        iconName: 'mic',
        iconClass: 'mic'
    }, {
        iconName: 'mic_none',
        iconClass: 'mic_none'
    }, {
        iconName: 'mic_off',
        iconClass: 'mic_off'
    }, {
        iconName: 'mms',
        iconClass: 'mms'
    }, {
        iconName: 'mode_comment',
        iconClass: 'mode_comment'
    }, {
        iconName: 'mode_edit',
        iconClass: 'mode_edit'
    }, {
        iconName: 'monetization_on',
        iconClass: 'monetization_on'
    }, {
        iconName: 'money_off',
        iconClass: 'money_off'
    }, {
        iconName: 'monochrome_photos',
        iconClass: 'monochrome_photos'
    }, {
        iconName: 'mood',
        iconClass: 'mood'
    }, {
        iconName: 'mood_bad',
        iconClass: 'mood_bad'
    }, {
        iconName: 'more',
        iconClass: 'more'
    }, {
        iconName: 'more_horiz',
        iconClass: 'more_horiz'
    }, {
        iconName: 'more_vert',
        iconClass: 'more_vert'
    }, {
        iconName: 'motorcycle',
        iconClass: 'motorcycle'
    }, {
        iconName: 'mouse',
        iconClass: 'mouse'
    }, {
        iconName: 'move_to_inbox',
        iconClass: 'move_to_inbox'
    }, {
        iconName: 'movie',
        iconClass: 'movie'
    }, {
        iconName: 'movie_creation',
        iconClass: 'movie_creation'
    }, {
        iconName: 'movie_filter',
        iconClass: 'movie_filter'
    }, {
        iconName: 'multiline_chart',
        iconClass: 'multiline_chart'
    }, {
        iconName: 'music_note',
        iconClass: 'music_note'
    }, {
        iconName: 'music_video',
        iconClass: 'music_video'
    }, {
        iconName: 'my_location',
        iconClass: 'my_location'
    }, {
        iconName: 'nature',
        iconClass: 'nature'
    }, {
        iconName: 'nature_people',
        iconClass: 'nature_people'
    }, {
        iconName: 'navigate_before',
        iconClass: 'navigate_before'
    }, {
        iconName: 'navigate_next',
        iconClass: 'navigate_next'
    }, {
        iconName: 'navigation',
        iconClass: 'navigation'
    }, {
        iconName: 'near_me',
        iconClass: 'near_me'
    }, {
        iconName: 'network_cell',
        iconClass: 'network_cell'
    }, {
        iconName: 'network_check',
        iconClass: 'network_check'
    }, {
        iconName: 'network_locked',
        iconClass: 'network_locked'
    }, {
        iconName: 'network_wifi',
        iconClass: 'network_wifi'
    }, {
        iconName: 'new_releases',
        iconClass: 'new_releases'
    }, {
        iconName: 'next_week',
        iconClass: 'next_week'
    }, {
        iconName: 'nfc',
        iconClass: 'nfc'
    }, {
        iconName: 'no_encryption',
        iconClass: 'no_encryption'
    }, {
        iconName: 'no_sim',
        iconClass: 'no_sim'
    }, {
        iconName: 'not_interested',
        iconClass: 'not_interested'
    }, {
        iconName: 'note',
        iconClass: 'note'
    }, {
        iconName: 'note_add',
        iconClass: 'note_add'
    }, {
        iconName: 'notifications',
        iconClass: 'notifications'
    }, {
        iconName: 'notifications_active',
        iconClass: 'notifications_active'
    }, {
        iconName: 'notifications_none',
        iconClass: 'notifications_none'
    }, {
        iconName: 'notifications_off',
        iconClass: 'notifications_off'
    }, {
        iconName: 'notifications_paused',
        iconClass: 'notifications_paused'
    }, {
        iconName: 'offline_pin',
        iconClass: 'offline_pin'
    }, {
        iconName: 'ondemand_video',
        iconClass: 'ondemand_video'
    }, {
        iconName: 'opacity',
        iconClass: 'opacity'
    }, {
        iconName: 'open_in_browser',
        iconClass: 'open_in_browser'
    }, {
        iconName: 'open_in_new',
        iconClass: 'open_in_new'
    }, {
        iconName: 'open_with',
        iconClass: 'open_with'
    }, {
        iconName: 'pages',
        iconClass: 'pages'
    }, {
        iconName: 'pageview',
        iconClass: 'pageview'
    }, {
        iconName: 'palette',
        iconClass: 'palette'
    }, {
        iconName: 'pan_tool',
        iconClass: 'pan_tool'
    }, {
        iconName: 'panorama',
        iconClass: 'panorama'
    }, {
        iconName: 'panorama_fish_eye',
        iconClass: 'panorama_fish_eye'
    }, {
        iconName: 'panorama_horizontal',
        iconClass: 'panorama_horizontal'
    }, {
        iconName: 'panorama_vertical',
        iconClass: 'panorama_vertical'
    }, {
        iconName: 'panorama_wide_angle',
        iconClass: 'panorama_wide_angle'
    }, {
        iconName: 'party_mode',
        iconClass: 'party_mode'
    }, {
        iconName: 'pause',
        iconClass: 'pause'
    }, {
        iconName: 'pause_circle_filled',
        iconClass: 'pause_circle_filled'
    }, {
        iconName: 'pause_circle_outline',
        iconClass: 'pause_circle_outline'
    }, {
        iconName: 'payment',
        iconClass: 'payment'
    }, {
        iconName: 'people',
        iconClass: 'people'
    }, {
        iconName: 'people_outline',
        iconClass: 'people_outline'
    }, {
        iconName: 'perm_camera_mic',
        iconClass: 'perm_camera_mic'
    }, {
        iconName: 'perm_contact_calendar',
        iconClass: 'perm_contact_calendar'
    }, {
        iconName: 'perm_data_setting',
        iconClass: 'perm_data_setting'
    }, {
        iconName: 'perm_device_information',
        iconClass: 'perm_device_information'
    }, {
        iconName: 'perm_identity',
        iconClass: 'perm_identity'
    }, {
        iconName: 'perm_media',
        iconClass: 'perm_media'
    }, {
        iconName: 'perm_phone_msg',
        iconClass: 'perm_phone_msg'
    }, {
        iconName: 'perm_scan_wifi',
        iconClass: 'perm_scan_wifi'
    }, {
        iconName: 'person',
        iconClass: 'person'
    }, {
        iconName: 'person_add',
        iconClass: 'person_add'
    }, {
        iconName: 'person_outline',
        iconClass: 'person_outline'
    }, {
        iconName: 'person_pin',
        iconClass: 'person_pin'
    }, {
        iconName: 'person_pin_circle',
        iconClass: 'person_pin_circle'
    }, {
        iconName: 'personal_video',
        iconClass: 'personal_video'
    }, {
        iconName: 'pets',
        iconClass: 'pets'
    }, {
        iconName: 'phone',
        iconClass: 'phone'
    }, {
        iconName: 'phone_android',
        iconClass: 'phone_android'
    }, {
        iconName: 'phone_bluetooth_speaker',
        iconClass: 'phone_bluetooth_speaker'
    }, {
        iconName: 'phone_forwarded',
        iconClass: 'phone_forwarded'
    }, {
        iconName: 'phone_in_talk',
        iconClass: 'phone_in_talk'
    }, {
        iconName: 'phone_iphone',
        iconClass: 'phone_iphone'
    }, {
        iconName: 'phone_locked',
        iconClass: 'phone_locked'
    }, {
        iconName: 'phone_missed',
        iconClass: 'phone_missed'
    }, {
        iconName: 'phone_paused',
        iconClass: 'phone_paused'
    }, {
        iconName: 'phonelink',
        iconClass: 'phonelink'
    }, {
        iconName: 'phonelink_erase',
        iconClass: 'phonelink_erase'
    }, {
        iconName: 'phonelink_lock',
        iconClass: 'phonelink_lock'
    }, {
        iconName: 'phonelink_off',
        iconClass: 'phonelink_off'
    }, {
        iconName: 'phonelink_ring',
        iconClass: 'phonelink_ring'
    }, {
        iconName: 'phonelink_setup',
        iconClass: 'phonelink_setup'
    }, {
        iconName: 'photo',
        iconClass: 'photo'
    }, {
        iconName: 'photo_album',
        iconClass: 'photo_album'
    }, {
        iconName: 'photo_camera',
        iconClass: 'photo_camera'
    }, {
        iconName: 'photo_filter',
        iconClass: 'photo_filter'
    }, {
        iconName: 'photo_library',
        iconClass: 'photo_library'
    }, {
        iconName: 'photo_size_select_actual',
        iconClass: 'photo_size_select_actual'
    }, {
        iconName: 'photo_size_select_large',
        iconClass: 'photo_size_select_large'
    }, {
        iconName: 'photo_size_select_small',
        iconClass: 'photo_size_select_small'
    }, {
        iconName: 'picture_as_pdf',
        iconClass: 'picture_as_pdf'
    }, {
        iconName: 'picture_in_picture',
        iconClass: 'picture_in_picture'
    }, {
        iconName: 'picture_in_picture_alt',
        iconClass: 'picture_in_picture_alt'
    }, {
        iconName: 'pie_chart',
        iconClass: 'pie_chart'
    }, {
        iconName: 'pie_chart_outlined',
        iconClass: 'pie_chart_outlined'
    }, {
        iconName: 'pin_drop',
        iconClass: 'pin_drop'
    }, {
        iconName: 'place',
        iconClass: 'place'
    }, {
        iconName: 'play_arrow',
        iconClass: 'play_arrow'
    }, {
        iconName: 'play_circle_filled',
        iconClass: 'play_circle_filled'
    }, {
        iconName: 'play_circle_outline',
        iconClass: 'play_circle_outline'
    }, {
        iconName: 'play_for_work',
        iconClass: 'play_for_work'
    }, {
        iconName: 'playlist_add',
        iconClass: 'playlist_add'
    }, {
        iconName: 'playlist_add_check',
        iconClass: 'playlist_add_check'
    }, {
        iconName: 'playlist_play',
        iconClass: 'playlist_play'
    }, {
        iconName: 'plus_one',
        iconClass: 'plus_one'
    }, {
        iconName: 'poll',
        iconClass: 'poll'
    }, {
        iconName: 'polymer',
        iconClass: 'polymer'
    }, {
        iconName: 'pool',
        iconClass: 'pool'
    }, {
        iconName: 'portable_wifi_off',
        iconClass: 'portable_wifi_off'
    }, {
        iconName: 'portrait',
        iconClass: 'portrait'
    }, {
        iconName: 'power',
        iconClass: 'power'
    }, {
        iconName: 'power_input',
        iconClass: 'power_input'
    }, {
        iconName: 'power_settings_new',
        iconClass: 'power_settings_new'
    }, {
        iconName: 'pregnant_woman',
        iconClass: 'pregnant_woman'
    }, {
        iconName: 'present_to_all',
        iconClass: 'present_to_all'
    }, {
        iconName: 'print',
        iconClass: 'print'
    }, {
        iconName: 'priority_high',
        iconClass: 'priority_high'
    }, {
        iconName: 'public',
        iconClass: 'public'
    }, {
        iconName: 'publish',
        iconClass: 'publish'
    }, {
        iconName: 'query_builder',
        iconClass: 'query_builder'
    }, {
        iconName: 'question_answer',
        iconClass: 'question_answer'
    }, {
        iconName: 'queue',
        iconClass: 'queue'
    }, {
        iconName: 'queue_music',
        iconClass: 'queue_music'
    }, {
        iconName: 'queue_play_next',
        iconClass: 'queue_play_next'
    }, {
        iconName: 'radio',
        iconClass: 'radio'
    }, {
        iconName: 'radio_button_checked',
        iconClass: 'radio_button_checked'
    }, {
        iconName: 'radio_button_unchecked',
        iconClass: 'radio_button_unchecked'
    }, {
        iconName: 'rate_review',
        iconClass: 'rate_review'
    }, {
        iconName: 'receipt',
        iconClass: 'receipt'
    }, {
        iconName: 'recent_actors',
        iconClass: 'recent_actors'
    }, {
        iconName: 'record_voice_over',
        iconClass: 'record_voice_over'
    }, {
        iconName: 'redeem',
        iconClass: 'redeem'
    }, {
        iconName: 'redo',
        iconClass: 'redo'
    }, {
        iconName: 'refresh',
        iconClass: 'refresh'
    }, {
        iconName: 'remove',
        iconClass: 'remove'
    }, {
        iconName: 'remove_circle',
        iconClass: 'remove_circle'
    }, {
        iconName: 'remove_circle_outline',
        iconClass: 'remove_circle_outline'
    }, {
        iconName: 'remove_from_queue',
        iconClass: 'remove_from_queue'
    }, {
        iconName: 'remove_red_eye',
        iconClass: 'remove_red_eye'
    }, {
        iconName: 'remove_shopping_cart',
        iconClass: 'remove_shopping_cart'
    }, {
        iconName: 'reorder',
        iconClass: 'reorder'
    }, {
        iconName: 'repeat',
        iconClass: 'repeat'
    }, {
        iconName: 'repeat_one',
        iconClass: 'repeat_one'
    }, {
        iconName: 'replay',
        iconClass: 'replay'
    }, {
        iconName: 'replay_10',
        iconClass: 'replay_10'
    }, {
        iconName: 'replay_30',
        iconClass: 'replay_30'
    }, {
        iconName: 'replay_5',
        iconClass: 'replay_5'
    }, {
        iconName: 'reply',
        iconClass: 'reply'
    }, {
        iconName: 'reply_all',
        iconClass: 'reply_all'
    }, {
        iconName: 'report',
        iconClass: 'report'
    }, {
        iconName: 'report_problem',
        iconClass: 'report_problem'
    }, {
        iconName: 'restaurant',
        iconClass: 'restaurant'
    }, {
        iconName: 'restaurant_menu',
        iconClass: 'restaurant_menu'
    }, {
        iconName: 'restore',
        iconClass: 'restore'
    }, {
        iconName: 'restore_page',
        iconClass: 'restore_page'
    }, {
        iconName: 'ring_volume',
        iconClass: 'ring_volume'
    }, {
        iconName: 'room',
        iconClass: 'room'
    }, {
        iconName: 'room_service',
        iconClass: 'room_service'
    }, {
        iconName: 'rotate_90_degrees_ccw',
        iconClass: 'rotate_90_degrees_ccw'
    }, {
        iconName: 'rotate_left',
        iconClass: 'rotate_left'
    }, {
        iconName: 'rotate_right',
        iconClass: 'rotate_right'
    }, {
        iconName: 'rounded_corner',
        iconClass: 'rounded_corner'
    }, {
        iconName: 'router',
        iconClass: 'router'
    }, {
        iconName: 'rowing',
        iconClass: 'rowing'
    }, {
        iconName: 'rss_feed',
        iconClass: 'rss_feed'
    }, {
        iconName: 'rv_hookup',
        iconClass: 'rv_hookup'
    }, {
        iconName: 'satellite',
        iconClass: 'satellite'
    }, {
        iconName: 'save',
        iconClass: 'save'
    }, {
        iconName: 'scanner',
        iconClass: 'scanner'
    }, {
        iconName: 'schedule',
        iconClass: 'schedule'
    }, {
        iconName: 'school',
        iconClass: 'school'
    }, {
        iconName: 'screen_lock_landscape',
        iconClass: 'screen_lock_landscape'
    }, {
        iconName: 'screen_lock_portrait',
        iconClass: 'screen_lock_portrait'
    }, {
        iconName: 'screen_lock_rotation',
        iconClass: 'screen_lock_rotation'
    }, {
        iconName: 'screen_rotation',
        iconClass: 'screen_rotation'
    }, {
        iconName: 'screen_share',
        iconClass: 'screen_share'
    }, {
        iconName: 'sd_card',
        iconClass: 'sd_card'
    }, {
        iconName: 'sd_storage',
        iconClass: 'sd_storage'
    }, {
        iconName: 'search',
        iconClass: 'search'
    }, {
        iconName: 'security',
        iconClass: 'security'
    }, {
        iconName: 'select_all',
        iconClass: 'select_all'
    }, {
        iconName: 'send',
        iconClass: 'send'
    }, {
        iconName: 'sentiment_dissatisfied',
        iconClass: 'sentiment_dissatisfied'
    }, {
        iconName: 'sentiment_neutral',
        iconClass: 'sentiment_neutral'
    }, {
        iconName: 'sentiment_satisfied',
        iconClass: 'sentiment_satisfied'
    }, {
        iconName: 'sentiment_very_dissatisfied',
        iconClass: 'sentiment_very_dissatisfied'
    }, {
        iconName: 'sentiment_very_satisfied',
        iconClass: 'sentiment_very_satisfied'
    }, {
        iconName: 'settings',
        iconClass: 'settings'
    }, {
        iconName: 'settings_applications',
        iconClass: 'settings_applications'
    }, {
        iconName: 'settings_backup_restore',
        iconClass: 'settings_backup_restore'
    }, {
        iconName: 'settings_bluetooth',
        iconClass: 'settings_bluetooth'
    }, {
        iconName: 'settings_brightness',
        iconClass: 'settings_brightness'
    }, {
        iconName: 'settings_cell',
        iconClass: 'settings_cell'
    }, {
        iconName: 'settings_ethernet',
        iconClass: 'settings_ethernet'
    }, {
        iconName: 'settings_input_antenna',
        iconClass: 'settings_input_antenna'
    }, {
        iconName: 'settings_input_component',
        iconClass: 'settings_input_component'
    }, {
        iconName: 'settings_input_composite',
        iconClass: 'settings_input_composite'
    }, {
        iconName: 'settings_input_hdmi',
        iconClass: 'settings_input_hdmi'
    }, {
        iconName: 'settings_input_svideo',
        iconClass: 'settings_input_svideo'
    }, {
        iconName: 'settings_overscan',
        iconClass: 'settings_overscan'
    }, {
        iconName: 'settings_phone',
        iconClass: 'settings_phone'
    }, {
        iconName: 'settings_power',
        iconClass: 'settings_power'
    }, {
        iconName: 'settings_remote',
        iconClass: 'settings_remote'
    }, {
        iconName: 'settings_system_daydream',
        iconClass: 'settings_system_daydream'
    }, {
        iconName: 'settings_voice',
        iconClass: 'settings_voice'
    }, {
        iconName: 'share',
        iconClass: 'share'
    }, {
        iconName: 'shop',
        iconClass: 'shop'
    }, {
        iconName: 'shop_two',
        iconClass: 'shop_two'
    }, {
        iconName: 'shopping_basket',
        iconClass: 'shopping_basket'
    }, {
        iconName: 'shopping_cart',
        iconClass: 'shopping_cart'
    }, {
        iconName: 'short_text',
        iconClass: 'short_text'
    }, {
        iconName: 'show_chart',
        iconClass: 'show_chart'
    }, {
        iconName: 'shuffle',
        iconClass: 'shuffle'
    }, {
        iconName: 'signal_cellular_4_bar',
        iconClass: 'signal_cellular_4_bar'
    }, {
        iconName: 'signal_cellular_connected_no_internet_4_bar',
        iconClass: 'signal_cellular_connected_no_internet_4_bar'
    }, {
        iconName: 'signal_cellular_no_sim',
        iconClass: 'signal_cellular_no_sim'
    }, {
        iconName: 'signal_cellular_null',
        iconClass: 'signal_cellular_null'
    }, {
        iconName: 'signal_cellular_off',
        iconClass: 'signal_cellular_off'
    }, {
        iconName: 'signal_wifi_4_bar',
        iconClass: 'signal_wifi_4_bar'
    }, {
        iconName: 'signal_wifi_4_bar_lock',
        iconClass: 'signal_wifi_4_bar_lock'
    }, {
        iconName: 'signal_wifi_off',
        iconClass: 'signal_wifi_off'
    }, {
        iconName: 'sim_card',
        iconClass: 'sim_card'
    }, {
        iconName: 'sim_card_alert',
        iconClass: 'sim_card_alert'
    }, {
        iconName: 'skip_next',
        iconClass: 'skip_next'
    }, {
        iconName: 'skip_previous',
        iconClass: 'skip_previous'
    }, {
        iconName: 'slideshow',
        iconClass: 'slideshow'
    }, {
        iconName: 'slow_motion_video',
        iconClass: 'slow_motion_video'
    }, {
        iconName: 'smartphone',
        iconClass: 'smartphone'
    }, {
        iconName: 'smoke_free',
        iconClass: 'smoke_free'
    }, {
        iconName: 'smoking_rooms',
        iconClass: 'smoking_rooms'
    }, {
        iconName: 'sms',
        iconClass: 'sms'
    }, {
        iconName: 'sms_failed',
        iconClass: 'sms_failed'
    }, {
        iconName: 'snooze',
        iconClass: 'snooze'
    }, {
        iconName: 'sort',
        iconClass: 'sort'
    }, {
        iconName: 'sort_by_alpha',
        iconClass: 'sort_by_alpha'
    }, {
        iconName: 'spa',
        iconClass: 'spa'
    }, {
        iconName: 'space_bar',
        iconClass: 'space_bar'
    }, {
        iconName: 'speaker',
        iconClass: 'speaker'
    }, {
        iconName: 'speaker_group',
        iconClass: 'speaker_group'
    }, {
        iconName: 'speaker_notes',
        iconClass: 'speaker_notes'
    }, {
        iconName: 'speaker_notes_off',
        iconClass: 'speaker_notes_off'
    }, {
        iconName: 'speaker_phone',
        iconClass: 'speaker_phone'
    }, {
        iconName: 'spellcheck',
        iconClass: 'spellcheck'
    }, {
        iconName: 'star',
        iconClass: 'star'
    }, {
        iconName: 'star_border',
        iconClass: 'star_border'
    }, {
        iconName: 'star_half',
        iconClass: 'star_half'
    }, {
        iconName: 'stars',
        iconClass: 'stars'
    }, {
        iconName: 'stay_current_landscape',
        iconClass: 'stay_current_landscape'
    }, {
        iconName: 'stay_current_portrait',
        iconClass: 'stay_current_portrait'
    }, {
        iconName: 'stay_primary_landscape',
        iconClass: 'stay_primary_landscape'
    }, {
        iconName: 'stay_primary_portrait',
        iconClass: 'stay_primary_portrait'
    }, {
        iconName: 'stop',
        iconClass: 'stop'
    }, {
        iconName: 'stop_screen_share',
        iconClass: 'stop_screen_share'
    }, {
        iconName: 'storage',
        iconClass: 'storage'
    }, {
        iconName: 'store',
        iconClass: 'store'
    }, {
        iconName: 'store_mall_directory',
        iconClass: 'store_mall_directory'
    }, {
        iconName: 'straighten',
        iconClass: 'straighten'
    }, {
        iconName: 'streetview',
        iconClass: 'streetview'
    }, {
        iconName: 'strikethrough_s',
        iconClass: 'strikethrough_s'
    }, {
        iconName: 'style',
        iconClass: 'style'
    }, {
        iconName: 'subdirectory_arrow_left',
        iconClass: 'subdirectory_arrow_left'
    }, {
        iconName: 'subdirectory_arrow_right',
        iconClass: 'subdirectory_arrow_right'
    }, {
        iconName: 'subject',
        iconClass: 'subject'
    }, {
        iconName: 'subscriptions',
        iconClass: 'subscriptions'
    }, {
        iconName: 'subtitles',
        iconClass: 'subtitles'
    }, {
        iconName: 'subway',
        iconClass: 'subway'
    }, {
        iconName: 'supervisor_account',
        iconClass: 'supervisor_account'
    }, {
        iconName: 'surround_sound',
        iconClass: 'surround_sound'
    }, {
        iconName: 'swap_calls',
        iconClass: 'swap_calls'
    }, {
        iconName: 'swap_horiz',
        iconClass: 'swap_horiz'
    }, {
        iconName: 'swap_vert',
        iconClass: 'swap_vert'
    }, {
        iconName: 'swap_vertical_circle',
        iconClass: 'swap_vertical_circle'
    }, {
        iconName: 'switch_camera',
        iconClass: 'switch_camera'
    }, {
        iconName: 'switch_video',
        iconClass: 'switch_video'
    }, {
        iconName: 'sync',
        iconClass: 'sync'
    }, {
        iconName: 'sync_disabled',
        iconClass: 'sync_disabled'
    }, {
        iconName: 'sync_problem',
        iconClass: 'sync_problem'
    }, {
        iconName: 'system_update',
        iconClass: 'system_update'
    }, {
        iconName: 'system_update_alt',
        iconClass: 'system_update_alt'
    }, {
        iconName: 'tab',
        iconClass: 'tab'
    }, {
        iconName: 'tab_unselected',
        iconClass: 'tab_unselected'
    }, {
        iconName: 'tablet',
        iconClass: 'tablet'
    }, {
        iconName: 'tablet_android',
        iconClass: 'tablet_android'
    }, {
        iconName: 'tablet_mac',
        iconClass: 'tablet_mac'
    }, {
        iconName: 'tag_faces',
        iconClass: 'tag_faces'
    }, {
        iconName: 'tap_and_play',
        iconClass: 'tap_and_play'
    }, {
        iconName: 'terrain',
        iconClass: 'terrain'
    }, {
        iconName: 'text_fields',
        iconClass: 'text_fields'
    }, {
        iconName: 'text_format',
        iconClass: 'text_format'
    }, {
        iconName: 'textsms',
        iconClass: 'textsms'
    }, {
        iconName: 'texture',
        iconClass: 'texture'
    }, {
        iconName: 'theaters',
        iconClass: 'theaters'
    }, {
        iconName: 'thumb_down',
        iconClass: 'thumb_down'
    }, {
        iconName: 'thumb_up',
        iconClass: 'thumb_up'
    }, {
        iconName: 'thumbs_up_down',
        iconClass: 'thumbs_up_down'
    }, {
        iconName: 'time_to_leave',
        iconClass: 'time_to_leave'
    }, {
        iconName: 'timelapse',
        iconClass: 'timelapse'
    }, {
        iconName: 'timeline',
        iconClass: 'timeline'
    }, {
        iconName: 'timer',
        iconClass: 'timer'
    }, {
        iconName: 'timer_10',
        iconClass: 'timer_10'
    }, {
        iconName: 'timer_3',
        iconClass: 'timer_3'
    }, {
        iconName: 'timer_off',
        iconClass: 'timer_off'
    }, {
        iconName: 'title',
        iconClass: 'title'
    }, {
        iconName: 'toc',
        iconClass: 'toc'
    }, {
        iconName: 'today',
        iconClass: 'today'
    }, {
        iconName: 'toll',
        iconClass: 'toll'
    }, {
        iconName: 'tonality',
        iconClass: 'tonality'
    }, {
        iconName: 'touch_app',
        iconClass: 'touch_app'
    }, {
        iconName: 'toys',
        iconClass: 'toys'
    }, {
        iconName: 'track_changes',
        iconClass: 'track_changes'
    }, {
        iconName: 'traffic',
        iconClass: 'traffic'
    }, {
        iconName: 'train',
        iconClass: 'train'
    }, {
        iconName: 'tram',
        iconClass: 'tram'
    }, {
        iconName: 'transfer_within_a_station',
        iconClass: 'transfer_within_a_station'
    }, {
        iconName: 'transform',
        iconClass: 'transform'
    }, {
        iconName: 'translate',
        iconClass: 'translate'
    }, {
        iconName: 'trending_down',
        iconClass: 'trending_down'
    }, {
        iconName: 'trending_flat',
        iconClass: 'trending_flat'
    }, {
        iconName: 'trending_up',
        iconClass: 'trending_up'
    }, {
        iconName: 'tune',
        iconClass: 'tune'
    }, {
        iconName: 'turned_in',
        iconClass: 'turned_in'
    }, {
        iconName: 'turned_in_not',
        iconClass: 'turned_in_not'
    }, {
        iconName: 'tv',
        iconClass: 'tv'
    }, {
        iconName: 'unarchive',
        iconClass: 'unarchive'
    }, {
        iconName: 'undo',
        iconClass: 'undo'
    }, {
        iconName: 'unfold_less',
        iconClass: 'unfold_less'
    }, {
        iconName: 'unfold_more',
        iconClass: 'unfold_more'
    }, {
        iconName: 'update',
        iconClass: 'update'
    }, {
        iconName: 'usb',
        iconClass: 'usb'
    }, {
        iconName: 'verified_user',
        iconClass: 'verified_user'
    }, {
        iconName: 'vertical_align_bottom',
        iconClass: 'vertical_align_bottom'
    }, {
        iconName: 'vertical_align_center',
        iconClass: 'vertical_align_center'
    }, {
        iconName: 'vertical_align_top',
        iconClass: 'vertical_align_top'
    }, {
        iconName: 'vibration',
        iconClass: 'vibration'
    }, {
        iconName: 'video_call',
        iconClass: 'video_call'
    }, {
        iconName: 'video_label',
        iconClass: 'video_label'
    }, {
        iconName: 'video_library',
        iconClass: 'video_library'
    }, {
        iconName: 'videocam',
        iconClass: 'videocam'
    }, {
        iconName: 'videocam_off',
        iconClass: 'videocam_off'
    }, {
        iconName: 'videogame_asset',
        iconClass: 'videogame_asset'
    }, {
        iconName: 'view_agenda',
        iconClass: 'view_agenda'
    }, {
        iconName: 'view_array',
        iconClass: 'view_array'
    }, {
        iconName: 'view_carousel',
        iconClass: 'view_carousel'
    }, {
        iconName: 'view_column',
        iconClass: 'view_column'
    }, {
        iconName: 'view_comfy',
        iconClass: 'view_comfy'
    }, {
        iconName: 'view_compact',
        iconClass: 'view_compact'
    }, {
        iconName: 'view_day',
        iconClass: 'view_day'
    }, {
        iconName: 'view_headline',
        iconClass: 'view_headline'
    }, {
        iconName: 'view_list',
        iconClass: 'view_list'
    }, {
        iconName: 'view_module',
        iconClass: 'view_module'
    }, {
        iconName: 'view_quilt',
        iconClass: 'view_quilt'
    }, {
        iconName: 'view_stream',
        iconClass: 'view_stream'
    }, {
        iconName: 'view_week',
        iconClass: 'view_week'
    }, {
        iconName: 'vignette',
        iconClass: 'vignette'
    }, {
        iconName: 'visibility',
        iconClass: 'visibility'
    }, {
        iconName: 'visibility_off',
        iconClass: 'visibility_off'
    }, {
        iconName: 'voice_chat',
        iconClass: 'voice_chat'
    }, {
        iconName: 'voicemail',
        iconClass: 'voicemail'
    }, {
        iconName: 'volume_down',
        iconClass: 'volume_down'
    }, {
        iconName: 'volume_mute',
        iconClass: 'volume_mute'
    }, {
        iconName: 'volume_off',
        iconClass: 'volume_off'
    }, {
        iconName: 'volume_up',
        iconClass: 'volume_up'
    }, {
        iconName: 'vpn_key',
        iconClass: 'vpn_key'
    }, {
        iconName: 'vpn_lock',
        iconClass: 'vpn_lock'
    }, {
        iconName: 'wallpaper',
        iconClass: 'wallpaper'
    }, {
        iconName: 'warning',
        iconClass: 'warning'
    }, {
        iconName: 'watch',
        iconClass: 'watch'
    }, {
        iconName: 'watch_later',
        iconClass: 'watch_later'
    }, {
        iconName: 'wb_auto',
        iconClass: 'wb_auto'
    }, {
        iconName: 'wb_cloudy',
        iconClass: 'wb_cloudy'
    }, {
        iconName: 'wb_incandescent',
        iconClass: 'wb_incandescent'
    }, {
        iconName: 'wb_iridescent',
        iconClass: 'wb_iridescent'
    }, {
        iconName: 'wb_sunny',
        iconClass: 'wb_sunny'
    }, {
        iconName: 'wc',
        iconClass: 'wc'
    }, {
        iconName: 'web',
        iconClass: 'web'
    }, {
        iconName: 'web_asset',
        iconClass: 'web_asset'
    }, {
        iconName: 'weekend',
        iconClass: 'weekend'
    }, {
        iconName: 'whatshot',
        iconClass: 'whatshot'
    }, {
        iconName: 'widgets',
        iconClass: 'widgets'
    }, {
        iconName: 'wifi',
        iconClass: 'wifi'
    }, {
        iconName: 'wifi_lock',
        iconClass: 'wifi_lock'
    }, {
        iconName: 'wifi_tethering',
        iconClass: 'wifi_tethering'
    }, {
        iconName: 'work',
        iconClass: 'work'
    }, {
        iconName: 'wrap_text',
        iconClass: 'wrap_text'
    }, {
        iconName: 'youtube_searched_for',
        iconClass: 'youtube_searched_for'
    }, {
        iconName: 'zoom_in',
        iconClass: 'zoom_in'
    }, {
        iconName: 'zoom_out',
        iconClass: 'zoom_out'
    }, {
        iconName: 'zoom_out_map',
        iconClass: 'zoom_out_map'
    }];


/***/ }),

/***/ "./src/app/pages/pages.module.ts":
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/*! exports provided: PagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _demo_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../demo-material-module */ "./src/app/demo-material-module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _pages_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages.routing */ "./src/app/pages/pages.routing.ts");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_icons_mat_icon_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./material-icons/mat-icon.component */ "./src/app/pages/material-icons/mat-icon.component.ts");
/* harmony import */ var _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./timeline/timeline.component */ "./src/app/pages/timeline/timeline.component.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/pages/invoice/invoice.component.ts");
/* harmony import */ var _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pricing/pricing.component */ "./src/app/pages/pricing/pricing.component.ts");
/* harmony import */ var _helper_classes_helper_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./helper-classes/helper.component */ "./src/app/pages/helper-classes/helper.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var PagesModule = /** @class */ (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_pages_routing__WEBPACK_IMPORTED_MODULE_7__["PagesRoutes"]),
                _demo_material_module__WEBPACK_IMPORTED_MODULE_4__["DemoMaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_8__["NgxDatatableModule"]
            ],
            declarations: [
                _material_icons_mat_icon_component__WEBPACK_IMPORTED_MODULE_9__["MatIconComponent"],
                _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_10__["TimelineComponent"],
                _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_11__["InvoiceComponent"],
                _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_12__["PricingComponent"],
                _helper_classes_helper_component__WEBPACK_IMPORTED_MODULE_13__["HelperComponent"]
            ],
        })
    ], PagesModule);
    return PagesModule;
}());



/***/ }),

/***/ "./src/app/pages/pages.routing.ts":
/*!****************************************!*\
  !*** ./src/app/pages/pages.routing.ts ***!
  \****************************************/
/*! exports provided: PagesRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesRoutes", function() { return PagesRoutes; });
/* harmony import */ var _material_icons_mat_icon_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./material-icons/mat-icon.component */ "./src/app/pages/material-icons/mat-icon.component.ts");
/* harmony import */ var _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./timeline/timeline.component */ "./src/app/pages/timeline/timeline.component.ts");
/* harmony import */ var _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./invoice/invoice.component */ "./src/app/pages/invoice/invoice.component.ts");
/* harmony import */ var _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pricing/pricing.component */ "./src/app/pages/pricing/pricing.component.ts");
/* harmony import */ var _helper_classes_helper_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./helper-classes/helper.component */ "./src/app/pages/helper-classes/helper.component.ts");





var PagesRoutes = [
    {
        path: '',
        children: [{
                path: 'icons',
                component: _material_icons_mat_icon_component__WEBPACK_IMPORTED_MODULE_0__["MatIconComponent"]
            }, {
                path: 'timeline',
                component: _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_1__["TimelineComponent"]
            }, {
                path: 'invoice',
                component: _invoice_invoice_component__WEBPACK_IMPORTED_MODULE_2__["InvoiceComponent"]
            }, {
                path: 'pricing',
                component: _pricing_pricing_component__WEBPACK_IMPORTED_MODULE_3__["PricingComponent"]
            }, {
                path: 'helper',
                component: _helper_classes_helper_component__WEBPACK_IMPORTED_MODULE_4__["HelperComponent"]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/pricing/pricing.component.html":
/*!******************************************************!*\
  !*** ./src/app/pages/pricing/pricing.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div fxLayout=\"row\" fxLayoutWrap=\"wrap\" fxLayoutAlign=\"center start\" class=\"pricing-box\">\n  <div fxFlex.gt-sm=\"100\" fxFlex=\"100\">\n    \n            <div class=\"card-group text-center\">\n      \n                <mat-card class=\"pricing-plan\">\n                  <mat-card-content>    \n                  <mat-card-title>Silver</mat-card-title>\n                  <h1><sup>$</sup>24</h1>    \n                  <mat-card-subtitle class=\"mat-text-primary\">per month</mat-card-subtitle>\n                    <mat-list class=\"price-listing\">\n                      <mat-list-item>3 Members</mat-list-item>\n                      <mat-list-item>Single Device</mat-list-item>\n                      <mat-list-item>50GB Storage</mat-list-item>\n                      <mat-list-item>Monthly Backups</mat-list-item>\n                    </mat-list>\n                    <button mat-raised-button color=\"primary\">Choose Plan</button>  \n                  </mat-card-content>\n                  \n                </mat-card>\n                \n\n                <mat-card class=\"pricing-plan\">\n                  <mat-card-content>    \n                  <mat-card-title>Gold</mat-card-title>\n                  <h1><sup>$</sup>34</h1>    \n                  <mat-card-subtitle class=\"mat-text-primary\">per month</mat-card-subtitle>\n                    <mat-list class=\"price-listing\">\n                      <mat-list-item>5 Members</mat-list-item>\n                      <mat-list-item>Single Device</mat-list-item>\n                      <mat-list-item>80GB Storage</mat-list-item>\n                      <mat-list-item>Monthly Backups</mat-list-item>\n                    </mat-list>\n                    <button mat-raised-button color=\"primary\">Choose Plan</button>  \n                  </mat-card-content>\n                  \n                </mat-card>\n                \n                <mat-card class=\"pricing-plan popular-plan\">\n                  <mat-card-content>\n                      <span class=\"pop-tag\">Popular</span>      \n                      <mat-card-title>Platinum</mat-card-title>\n                      <h1><sup>$</sup>45</h1>    \n                      <mat-card-subtitle class=\"mat-text-primary\">per month</mat-card-subtitle>\n                        <mat-list class=\"price-listing\">\n                          <mat-list-item>10 Members</mat-list-item>\n                          <mat-list-item>Single Device</mat-list-item>\n                          <mat-list-item>120GB Storage</mat-list-item>\n                          <mat-list-item>Monthly Backups</mat-list-item>\n                        </mat-list>\n                        <button mat-raised-button color=\"warn\">Choose Plan</button>  \n                  </mat-card-content>\n                  \n                </mat-card>\n                \n                <mat-card class=\"pricing-plan\">\n                  <mat-card-content>    \n                  <mat-card-title>Dimond</mat-card-title>\n                  <h1><sup>$</sup>54</h1>    \n                  <mat-card-subtitle class=\"mat-text-primary\">per month</mat-card-subtitle>\n                    <mat-list class=\"price-listing\">\n                      <mat-list-item>15 Members</mat-list-item>\n                      <mat-list-item>Single Device</mat-list-item>\n                      <mat-list-item>1TB Storage</mat-list-item>\n                      <mat-list-item>Monthly Backups</mat-list-item>\n                    </mat-list>\n                    <button mat-raised-button color=\"primary\">Choose Plan</button>  \n                  </mat-card-content>\n                  \n                </mat-card>\n                \n            </div>\n      \n        \n    \n  </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/pricing/pricing.component.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/pricing/pricing.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\");\n/*\r\nTemplate Name: Material Pro Admin\r\nAuthor: Themedesigner\r\nEmail: niravjoshi87@gmail.com\r\nFile: scss\r\n*/\n/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n/*Material Theme Colors*/\n.mat-elevation-z0 {\n  box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z1 {\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z2 {\n  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z3 {\n  box-shadow: 0px 3px 3px -2px rgba(0, 0, 0, 0.2), 0px 3px 4px 0px rgba(0, 0, 0, 0.14), 0px 1px 8px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z4 {\n  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z5 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z6 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z7 {\n  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z8 {\n  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z9 {\n  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2), 0px 9px 12px 1px rgba(0, 0, 0, 0.14), 0px 3px 16px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z10 {\n  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2), 0px 10px 14px 1px rgba(0, 0, 0, 0.14), 0px 4px 18px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z11 {\n  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2), 0px 11px 15px 1px rgba(0, 0, 0, 0.14), 0px 4px 20px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z12 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z13 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 5px 24px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z14 {\n  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2), 0px 14px 21px 2px rgba(0, 0, 0, 0.14), 0px 5px 26px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z15 {\n  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2), 0px 15px 22px 2px rgba(0, 0, 0, 0.14), 0px 6px 28px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z16 {\n  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z17 {\n  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2), 0px 17px 26px 2px rgba(0, 0, 0, 0.14), 0px 6px 32px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z18 {\n  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2), 0px 18px 28px 2px rgba(0, 0, 0, 0.14), 0px 7px 34px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z19 {\n  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2), 0px 19px 29px 2px rgba(0, 0, 0, 0.14), 0px 7px 36px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z20 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 20px 31px 3px rgba(0, 0, 0, 0.14), 0px 8px 38px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z21 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 21px 33px 3px rgba(0, 0, 0, 0.14), 0px 8px 40px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z22 {\n  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2), 0px 22px 35px 3px rgba(0, 0, 0, 0.14), 0px 8px 42px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z23 {\n  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2), 0px 23px 36px 3px rgba(0, 0, 0, 0.14), 0px 9px 44px 8px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z24 {\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 9px 46px 8px rgba(0, 0, 0, 0.12); }\n.mat-badge-content {\n  font-weight: 600;\n  font-size: 12px;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-badge-small .mat-badge-content {\n  font-size: 6px; }\n.mat-badge-large .mat-badge-content {\n  font-size: 24px; }\n.mat-h1, .mat-headline, .mat-typography h1 {\n  font: 400 24px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h2, .mat-title, .mat-typography h2 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h3, .mat-subheading-2, .mat-typography h3 {\n  font: 400 16px/28px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h4, .mat-subheading-1, .mat-typography h4 {\n  font: 400 15px/24px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h5, .mat-typography h5 {\n  font: 400 11.62px/20px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 12px; }\n.mat-h6, .mat-typography h6 {\n  font: 400 9.38px/20px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 12px; }\n.mat-body-strong, .mat-body-2 {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-body, .mat-body-1, .mat-typography {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-body p, .mat-body-1 p, .mat-typography p {\n    margin: 0 0 12px; }\n.mat-small, .mat-caption {\n  font: 400 12px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-display-4, .mat-typography .mat-display-4 {\n  font: 300 112px/112px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 56px;\n  letter-spacing: -0.05em; }\n.mat-display-3, .mat-typography .mat-display-3 {\n  font: 400 56px/56px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.02em; }\n.mat-display-2, .mat-typography .mat-display-2 {\n  font: 400 45px/48px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.005em; }\n.mat-display-1, .mat-typography .mat-display-1 {\n  font: 400 34px/40px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px; }\n.mat-bottom-sheet-container {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-button, .mat-raised-button, .mat-icon-button, .mat-stroked-button,\n.mat-flat-button, .mat-fab, .mat-mini-fab {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-button-toggle {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-card {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-card-title {\n  font-size: 24px;\n  font-weight: 400; }\n.mat-card-subtitle,\n.mat-card-content,\n.mat-card-header .mat-card-title {\n  font-size: 14px; }\n.mat-checkbox {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px; }\n.mat-chip {\n  font-size: 13px;\n  line-height: 18px; }\n.mat-chip .mat-chip-trailing-icon.mat-icon,\n  .mat-chip .mat-chip-remove.mat-icon {\n    font-size: 18px; }\n.mat-table {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-header-cell {\n  font-size: 12px;\n  font-weight: 500; }\n.mat-cell, .mat-footer-cell {\n  font-size: 14px; }\n.mat-calendar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-calendar-body {\n  font-size: 13px; }\n.mat-calendar-body-label,\n.mat-calendar-period-button {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-calendar-table-header th {\n  font-size: 11px;\n  font-weight: 400; }\n.mat-dialog-title {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-expansion-panel-header {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 15px;\n  font-weight: 400; }\n.mat-expansion-panel-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-form-field {\n  font-size: inherit;\n  font-weight: 400;\n  line-height: 1.125;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-form-field-wrapper {\n  padding-bottom: 1.34375em; }\n.mat-form-field-prefix .mat-icon,\n.mat-form-field-suffix .mat-icon {\n  font-size: 150%;\n  line-height: 1.125; }\n.mat-form-field-prefix .mat-icon-button,\n.mat-form-field-suffix .mat-icon-button {\n  height: 1.5em;\n  width: 1.5em; }\n.mat-form-field-prefix .mat-icon-button .mat-icon,\n  .mat-form-field-suffix .mat-icon-button .mat-icon {\n    height: 1.125em;\n    line-height: 1.125; }\n.mat-form-field-infix {\n  padding: 0.5em 0;\n  border-top: 0.84375em solid transparent; }\n.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.34375em) scale(0.75);\n          transform: translateY(-1.34375em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.34374em) scale(0.75);\n          transform: translateY(-1.34374em) scale(0.75);\n  width: 133.33334333%; }\n.mat-form-field-label-wrapper {\n  top: -0.84375em;\n  padding-top: 0.84375em; }\n.mat-form-field-label {\n  top: 1.34375em; }\n.mat-form-field-underline {\n  bottom: 1.34375em; }\n.mat-form-field-subscript-wrapper {\n  font-size: 75%;\n  margin-top: 0.66666667em;\n  top: calc(100% - 1.79166667em); }\n.mat-form-field-appearance-legacy .mat-form-field-wrapper {\n  padding-bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-infix {\n  padding: 0.4375em 0; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px);\n  -ms-transform: translateY(-1.28125em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00101px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00101px);\n  -ms-transform: translateY(-1.28124em) scale(0.75);\n  width: 133.33334333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00102px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00102px);\n  -ms-transform: translateY(-1.28123em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-appearance-legacy .mat-form-field-label {\n  top: 1.28125em; }\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n  bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper {\n  margin-top: 0.54166667em;\n  top: calc(100% - 1.66666667em); }\n.mat-form-field-appearance-fill .mat-form-field-infix {\n  padding: 0.25em 0 0.75em 0; }\n.mat-form-field-appearance-fill .mat-form-field-label {\n  top: 1.09375em;\n  margin-top: -0.5em; }\n.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-0.59375em) scale(0.75);\n          transform: translateY(-0.59375em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-0.59374em) scale(0.75);\n          transform: translateY(-0.59374em) scale(0.75);\n  width: 133.33334333%; }\n.mat-form-field-appearance-outline .mat-form-field-infix {\n  padding: 1em 0 1em 0; }\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  bottom: 1.34375em; }\n.mat-form-field-appearance-outline .mat-form-field-label {\n  top: 1.84375em;\n  margin-top: -0.25em; }\n.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.59375em) scale(0.75);\n          transform: translateY(-1.59375em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.59374em) scale(0.75);\n          transform: translateY(-1.59374em) scale(0.75);\n  width: 133.33334333%; }\n.mat-grid-tile-header,\n.mat-grid-tile-footer {\n  font-size: 14px; }\n.mat-grid-tile-header .mat-line,\n  .mat-grid-tile-footer .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-grid-tile-header .mat-line:nth-child(n+2),\n    .mat-grid-tile-footer .mat-line:nth-child(n+2) {\n      font-size: 12px; }\ninput.mat-input-element {\n  margin-top: -0.0625em; }\n.mat-menu-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px; }\n.mat-radio-button {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-select {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-select-trigger {\n  height: 1.125em; }\n.mat-slide-toggle-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-slider-thumb-label-text {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-stepper-vertical, .mat-stepper-horizontal {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-step-label {\n  font-size: 14px;\n  font-weight: 400; }\n.mat-step-label-selected {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-tab-group {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-tab-label, .mat-tab-link {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-toolbar,\n.mat-toolbar h1,\n.mat-toolbar h2,\n.mat-toolbar h3,\n.mat-toolbar h4,\n.mat-toolbar h5,\n.mat-toolbar h6 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0; }\n.mat-tooltip {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 10px;\n  padding-top: 6px;\n  padding-bottom: 6px; }\n.mat-tooltip-handset {\n  font-size: 14px;\n  padding-top: 9px;\n  padding-bottom: 9px; }\n.mat-list-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-list-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  font-size: 16px; }\n.mat-list .mat-list-item .mat-line, .mat-nav-list .mat-list-item .mat-line, .mat-selection-list .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  font-size: 16px; }\n.mat-list .mat-list-option .mat-line, .mat-nav-list .mat-list-option .mat-line, .mat-selection-list .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-list[dense] .mat-list-item, .mat-nav-list[dense] .mat-list-item, .mat-selection-list[dense] .mat-list-item {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-item .mat-line, .mat-nav-list[dense] .mat-list-item .mat-line, .mat-selection-list[dense] .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-list-option, .mat-nav-list[dense] .mat-list-option, .mat-selection-list[dense] .mat-list-option {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-option .mat-line, .mat-nav-list[dense] .mat-list-option .mat-line, .mat-selection-list[dense] .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-subheader, .mat-nav-list[dense] .mat-subheader, .mat-selection-list[dense] .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px; }\n.mat-optgroup-label {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-simple-snackbar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px; }\n.mat-simple-snackbar-action {\n  line-height: 1;\n  font-family: inherit;\n  font-size: inherit;\n  font-weight: 500; }\n.mat-tree {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-tree-node {\n  font-weight: 400;\n  font-size: 14px; }\n.mat-ripple {\n  overflow: hidden; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-ripple {\n      display: none; } }\n.mat-ripple.mat-ripple-unbounded {\n  overflow: visible; }\n.mat-ripple-element {\n  position: absolute;\n  border-radius: 50%;\n  pointer-events: none;\n  transition: opacity, -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  -webkit-transform: scale(0);\n          transform: scale(0); }\n.cdk-visually-hidden {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n  outline: 0;\n  -webkit-appearance: none;\n  -moz-appearance: none; }\n.cdk-overlay-container, .cdk-global-overlay-wrapper {\n  pointer-events: none;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n.cdk-overlay-container {\n  position: fixed;\n  z-index: 1000; }\n.cdk-overlay-container:empty {\n    display: none; }\n.cdk-global-overlay-wrapper {\n  display: flex;\n  position: absolute;\n  z-index: 1000; }\n.cdk-overlay-pane {\n  position: absolute;\n  pointer-events: auto;\n  box-sizing: border-box;\n  z-index: 1000;\n  display: flex;\n  max-width: 100%;\n  max-height: 100%; }\n.cdk-overlay-backdrop {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1000;\n  pointer-events: auto;\n  -webkit-tap-highlight-color: transparent;\n  transition: opacity 400ms cubic-bezier(0.25, 0.8, 0.25, 1);\n  opacity: 0; }\n.cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n    opacity: 1; }\n@media screen and (-ms-high-contrast: active) {\n      .cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n        opacity: 0.6; } }\n.cdk-overlay-dark-backdrop {\n  background: rgba(0, 0, 0, 0.288); }\n.cdk-overlay-transparent-backdrop, .cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing {\n  opacity: 0; }\n.cdk-overlay-connected-position-bounding-box {\n  position: absolute;\n  z-index: 1000;\n  display: flex;\n  flex-direction: column;\n  min-width: 1px;\n  min-height: 1px; }\n.cdk-global-scrollblock {\n  position: fixed;\n  width: 100%;\n  overflow-y: scroll; }\n@-webkit-keyframes cdk-text-field-autofill-start {}\n@keyframes cdk-text-field-autofill-start {}\n@-webkit-keyframes cdk-text-field-autofill-end {}\n@keyframes cdk-text-field-autofill-end {}\n.cdk-text-field-autofill-monitored:-webkit-autofill {\n  -webkit-animation-name: cdk-text-field-autofill-start;\n          animation-name: cdk-text-field-autofill-start; }\n.cdk-text-field-autofill-monitored:not(:-webkit-autofill) {\n  -webkit-animation-name: cdk-text-field-autofill-end;\n          animation-name: cdk-text-field-autofill-end; }\ntextarea.cdk-textarea-autosize {\n  resize: none; }\ntextarea.cdk-textarea-autosize-measuring {\n  height: auto !important;\n  overflow: hidden !important;\n  padding: 2px 0 !important;\n  box-sizing: content-box !important; }\n.mat-elevation-z0 {\n  box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z1 {\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z2 {\n  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z3 {\n  box-shadow: 0px 3px 3px -2px rgba(0, 0, 0, 0.2), 0px 3px 4px 0px rgba(0, 0, 0, 0.14), 0px 1px 8px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z4 {\n  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z5 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z6 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z7 {\n  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z8 {\n  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z9 {\n  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2), 0px 9px 12px 1px rgba(0, 0, 0, 0.14), 0px 3px 16px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z10 {\n  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2), 0px 10px 14px 1px rgba(0, 0, 0, 0.14), 0px 4px 18px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z11 {\n  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2), 0px 11px 15px 1px rgba(0, 0, 0, 0.14), 0px 4px 20px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z12 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z13 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 5px 24px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z14 {\n  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2), 0px 14px 21px 2px rgba(0, 0, 0, 0.14), 0px 5px 26px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z15 {\n  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2), 0px 15px 22px 2px rgba(0, 0, 0, 0.14), 0px 6px 28px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z16 {\n  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z17 {\n  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2), 0px 17px 26px 2px rgba(0, 0, 0, 0.14), 0px 6px 32px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z18 {\n  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2), 0px 18px 28px 2px rgba(0, 0, 0, 0.14), 0px 7px 34px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z19 {\n  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2), 0px 19px 29px 2px rgba(0, 0, 0, 0.14), 0px 7px 36px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z20 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 20px 31px 3px rgba(0, 0, 0, 0.14), 0px 8px 38px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z21 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 21px 33px 3px rgba(0, 0, 0, 0.14), 0px 8px 40px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z22 {\n  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2), 0px 22px 35px 3px rgba(0, 0, 0, 0.14), 0px 8px 42px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z23 {\n  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2), 0px 23px 36px 3px rgba(0, 0, 0, 0.14), 0px 9px 44px 8px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z24 {\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 9px 46px 8px rgba(0, 0, 0, 0.12); }\n.mat-badge-content {\n  font-weight: 600;\n  font-size: 12px;\n  font-family: Poppins, sans-serif; }\n.mat-badge-small .mat-badge-content {\n  font-size: 6px; }\n.mat-badge-large .mat-badge-content {\n  font-size: 24px; }\n.mat-h1, .mat-headline, .mat-typography h1 {\n  font: 400 24px/32px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h2, .mat-title, .mat-typography h2 {\n  font: 500 20px/32px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h3, .mat-subheading-2, .mat-typography h3 {\n  font: 400 16px/28px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h4, .mat-subheading-1, .mat-typography h4 {\n  font: 400 15px/24px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h5, .mat-typography h5 {\n  font: 400 11.62px/20px Poppins, sans-serif;\n  margin: 0 0 12px; }\n.mat-h6, .mat-typography h6 {\n  font: 400 9.38px/20px Poppins, sans-serif;\n  margin: 0 0 12px; }\n.mat-body-strong, .mat-body-2 {\n  font: 500 14px/24px Poppins, sans-serif; }\n.mat-body, .mat-body-1, .mat-typography {\n  font: 400 14px/20px Poppins, sans-serif; }\n.mat-body p, .mat-body-1 p, .mat-typography p {\n    margin: 0 0 12px; }\n.mat-small, .mat-caption {\n  font: 400 12px/20px Poppins, sans-serif; }\n.mat-display-4, .mat-typography .mat-display-4 {\n  font: 300 112px/112px Poppins, sans-serif;\n  margin: 0 0 56px;\n  letter-spacing: -0.05em; }\n.mat-display-3, .mat-typography .mat-display-3 {\n  font: 400 56px/56px Poppins, sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.02em; }\n.mat-display-2, .mat-typography .mat-display-2 {\n  font: 400 45px/48px Poppins, sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.005em; }\n.mat-display-1, .mat-typography .mat-display-1 {\n  font: 400 34px/40px Poppins, sans-serif;\n  margin: 0 0 64px; }\n.mat-bottom-sheet-container {\n  font-family: Poppins, sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-button, .mat-raised-button, .mat-icon-button, .mat-stroked-button,\n.mat-flat-button, .mat-fab, .mat-mini-fab {\n  font-family: Poppins, sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-button-toggle {\n  font-family: Poppins, sans-serif; }\n.mat-card {\n  font-family: Poppins, sans-serif; }\n.mat-card-title {\n  font-size: 24px;\n  font-weight: 400; }\n.mat-card-subtitle,\n.mat-card-content,\n.mat-card-header .mat-card-title {\n  font-size: 14px; }\n.mat-checkbox {\n  font-family: Poppins, sans-serif; }\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px; }\n.mat-chip {\n  font-size: 13px;\n  line-height: 18px; }\n.mat-chip .mat-chip-trailing-icon.mat-icon,\n  .mat-chip .mat-chip-remove.mat-icon {\n    font-size: 18px; }\n.mat-table {\n  font-family: Poppins, sans-serif; }\n.mat-header-cell {\n  font-size: 12px;\n  font-weight: 500; }\n.mat-cell, .mat-footer-cell {\n  font-size: 14px; }\n.mat-calendar {\n  font-family: Poppins, sans-serif; }\n.mat-calendar-body {\n  font-size: 13px; }\n.mat-calendar-body-label,\n.mat-calendar-period-button {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-calendar-table-header th {\n  font-size: 11px;\n  font-weight: 400; }\n.mat-dialog-title {\n  font: 500 20px/32px Poppins, sans-serif; }\n.mat-expansion-panel-header {\n  font-family: Poppins, sans-serif;\n  font-size: 15px;\n  font-weight: 400; }\n.mat-expansion-panel-content {\n  font: 400 14px/20px Poppins, sans-serif; }\n.mat-form-field {\n  font-size: inherit;\n  font-weight: 400;\n  line-height: 1.125;\n  font-family: Poppins, sans-serif; }\n.mat-form-field-wrapper {\n  padding-bottom: 1.34375em; }\n.mat-form-field-prefix .mat-icon,\n.mat-form-field-suffix .mat-icon {\n  font-size: 150%;\n  line-height: 1.125; }\n.mat-form-field-prefix .mat-icon-button,\n.mat-form-field-suffix .mat-icon-button {\n  height: 1.5em;\n  width: 1.5em; }\n.mat-form-field-prefix .mat-icon-button .mat-icon,\n  .mat-form-field-suffix .mat-icon-button .mat-icon {\n    height: 1.125em;\n    line-height: 1.125; }\n.mat-form-field-infix {\n  padding: 0.5em 0;\n  border-top: 0.84375em solid transparent; }\n.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.34373em) scale(0.75);\n          transform: translateY(-1.34373em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.34372em) scale(0.75);\n          transform: translateY(-1.34372em) scale(0.75);\n  width: 133.33336333%; }\n.mat-form-field-label-wrapper {\n  top: -0.84375em;\n  padding-top: 0.84375em; }\n.mat-form-field-label {\n  top: 1.34375em; }\n.mat-form-field-underline {\n  bottom: 1.34375em; }\n.mat-form-field-subscript-wrapper {\n  font-size: 75%;\n  margin-top: 0.66666667em;\n  top: calc(100% - 1.79166667em); }\n.mat-form-field-appearance-legacy .mat-form-field-wrapper {\n  padding-bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-infix {\n  padding: 0.4375em 0; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00103px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00103px);\n  -ms-transform: translateY(-1.28122em) scale(0.75);\n  width: 133.33336333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00104px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00104px);\n  -ms-transform: translateY(-1.28121em) scale(0.75);\n  width: 133.33337333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00105px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00105px);\n  -ms-transform: translateY(-1.2812em) scale(0.75);\n  width: 133.33338333%; }\n.mat-form-field-appearance-legacy .mat-form-field-label {\n  top: 1.28125em; }\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n  bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper {\n  margin-top: 0.54166667em;\n  top: calc(100% - 1.66666667em); }\n.mat-form-field-appearance-fill .mat-form-field-infix {\n  padding: 0.25em 0 0.75em 0; }\n.mat-form-field-appearance-fill .mat-form-field-label {\n  top: 1.09375em;\n  margin-top: -0.5em; }\n.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-0.59373em) scale(0.75);\n          transform: translateY(-0.59373em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-0.59372em) scale(0.75);\n          transform: translateY(-0.59372em) scale(0.75);\n  width: 133.33336333%; }\n.mat-form-field-appearance-outline .mat-form-field-infix {\n  padding: 1em 0 1em 0; }\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  bottom: 1.34375em; }\n.mat-form-field-appearance-outline .mat-form-field-label {\n  top: 1.84375em;\n  margin-top: -0.25em; }\n.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.59373em) scale(0.75);\n          transform: translateY(-1.59373em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.59372em) scale(0.75);\n          transform: translateY(-1.59372em) scale(0.75);\n  width: 133.33336333%; }\n.mat-grid-tile-header,\n.mat-grid-tile-footer {\n  font-size: 14px; }\n.mat-grid-tile-header .mat-line,\n  .mat-grid-tile-footer .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-grid-tile-header .mat-line:nth-child(n+2),\n    .mat-grid-tile-footer .mat-line:nth-child(n+2) {\n      font-size: 12px; }\ninput.mat-input-element {\n  margin-top: -0.0625em; }\n.mat-menu-item {\n  font-family: Poppins, sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  font-family: Poppins, sans-serif;\n  font-size: 12px; }\n.mat-radio-button {\n  font-family: Poppins, sans-serif; }\n.mat-select {\n  font-family: Poppins, sans-serif; }\n.mat-select-trigger {\n  height: 1.125em; }\n.mat-slide-toggle-content {\n  font: 400 14px/20px Poppins, sans-serif; }\n.mat-slider-thumb-label-text {\n  font-family: Poppins, sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-stepper-vertical, .mat-stepper-horizontal {\n  font-family: Poppins, sans-serif; }\n.mat-step-label {\n  font-size: 14px;\n  font-weight: 400; }\n.mat-step-label-selected {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-tab-group {\n  font-family: Poppins, sans-serif; }\n.mat-tab-label, .mat-tab-link {\n  font-family: Poppins, sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-toolbar,\n.mat-toolbar h1,\n.mat-toolbar h2,\n.mat-toolbar h3,\n.mat-toolbar h4,\n.mat-toolbar h5,\n.mat-toolbar h6 {\n  font: 500 20px/32px Poppins, sans-serif;\n  margin: 0; }\n.mat-tooltip {\n  font-family: Poppins, sans-serif;\n  font-size: 10px;\n  padding-top: 6px;\n  padding-bottom: 6px; }\n.mat-tooltip-handset {\n  font-size: 14px;\n  padding-top: 9px;\n  padding-bottom: 9px; }\n.mat-list-item {\n  font-family: Poppins, sans-serif; }\n.mat-list-option {\n  font-family: Poppins, sans-serif; }\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  font-size: 16px; }\n.mat-list .mat-list-item .mat-line, .mat-nav-list .mat-list-item .mat-line, .mat-selection-list .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  font-size: 16px; }\n.mat-list .mat-list-option .mat-line, .mat-nav-list .mat-list-option .mat-line, .mat-selection-list .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  font-family: Poppins, sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-list[dense] .mat-list-item, .mat-nav-list[dense] .mat-list-item, .mat-selection-list[dense] .mat-list-item {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-item .mat-line, .mat-nav-list[dense] .mat-list-item .mat-line, .mat-selection-list[dense] .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-list-option, .mat-nav-list[dense] .mat-list-option, .mat-selection-list[dense] .mat-list-option {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-option .mat-line, .mat-nav-list[dense] .mat-list-option .mat-line, .mat-selection-list[dense] .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-subheader, .mat-nav-list[dense] .mat-subheader, .mat-selection-list[dense] .mat-subheader {\n  font-family: Poppins, sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-option {\n  font-family: Poppins, sans-serif;\n  font-size: 16px; }\n.mat-optgroup-label {\n  font: 500 14px/24px Poppins, sans-serif; }\n.mat-simple-snackbar {\n  font-family: Poppins, sans-serif;\n  font-size: 14px; }\n.mat-simple-snackbar-action {\n  line-height: 1;\n  font-family: inherit;\n  font-size: inherit;\n  font-weight: 500; }\n.mat-tree {\n  font-family: Poppins, sans-serif; }\n.mat-tree-node {\n  font-weight: 400;\n  font-size: 14px; }\n.mat-ripple {\n  overflow: hidden; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-ripple {\n      display: none; } }\n.mat-ripple.mat-ripple-unbounded {\n  overflow: visible; }\n.mat-ripple-element {\n  position: absolute;\n  border-radius: 50%;\n  pointer-events: none;\n  transition: opacity, -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  -webkit-transform: scale(0);\n          transform: scale(0); }\n.cdk-visually-hidden {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n  outline: 0;\n  -webkit-appearance: none;\n  -moz-appearance: none; }\n.cdk-overlay-container, .cdk-global-overlay-wrapper {\n  pointer-events: none;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n.cdk-overlay-container {\n  position: fixed;\n  z-index: 1000; }\n.cdk-overlay-container:empty {\n    display: none; }\n.cdk-global-overlay-wrapper {\n  display: flex;\n  position: absolute;\n  z-index: 1000; }\n.cdk-overlay-pane {\n  position: absolute;\n  pointer-events: auto;\n  box-sizing: border-box;\n  z-index: 1000;\n  display: flex;\n  max-width: 100%;\n  max-height: 100%; }\n.cdk-overlay-backdrop {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1000;\n  pointer-events: auto;\n  -webkit-tap-highlight-color: transparent;\n  transition: opacity 400ms cubic-bezier(0.25, 0.8, 0.25, 1);\n  opacity: 0; }\n.cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n    opacity: 1; }\n@media screen and (-ms-high-contrast: active) {\n      .cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n        opacity: 0.6; } }\n.cdk-overlay-dark-backdrop {\n  background: rgba(0, 0, 0, 0.288); }\n.cdk-overlay-transparent-backdrop, .cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing {\n  opacity: 0; }\n.cdk-overlay-connected-position-bounding-box {\n  position: absolute;\n  z-index: 1000;\n  display: flex;\n  flex-direction: column;\n  min-width: 1px;\n  min-height: 1px; }\n.cdk-global-scrollblock {\n  position: fixed;\n  width: 100%;\n  overflow-y: scroll; }\n@keyframes cdk-text-field-autofill-start {}\n@keyframes cdk-text-field-autofill-end {}\n.cdk-text-field-autofill-monitored:-webkit-autofill {\n  -webkit-animation-name: cdk-text-field-autofill-start;\n          animation-name: cdk-text-field-autofill-start; }\n.cdk-text-field-autofill-monitored:not(:-webkit-autofill) {\n  -webkit-animation-name: cdk-text-field-autofill-end;\n          animation-name: cdk-text-field-autofill-end; }\ntextarea.cdk-textarea-autosize {\n  resize: none; }\ntextarea.cdk-textarea-autosize-measuring {\n  height: auto !important;\n  overflow: hidden !important;\n  padding: 2px 0 !important;\n  box-sizing: content-box !important; }\n.mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.1); }\n.mat-option {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-option:hover:not(.mat-option-disabled), .mat-option:focus:not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n.mat-option.mat-selected:not(.mat-option-multiple):not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n.mat-option.mat-active {\n    background: rgba(0, 0, 0, 0.04);\n    color: rgba(0, 0, 0, 0.87); }\n.mat-option.mat-option-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n.mat-primary .mat-option.mat-selected:not(.mat-option-disabled) {\n  color: #1e88e5; }\n.mat-accent .mat-option.mat-selected:not(.mat-option-disabled) {\n  color: #3f51b5; }\n.mat-warn .mat-option.mat-selected:not(.mat-option-disabled) {\n  color: #e91e63; }\n.mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-optgroup-disabled .mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-pseudo-checkbox {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-pseudo-checkbox::after {\n    color: #fafafa; }\n.mat-pseudo-checkbox-checked,\n.mat-pseudo-checkbox-indeterminate,\n.mat-accent .mat-pseudo-checkbox-checked,\n.mat-accent .mat-pseudo-checkbox-indeterminate {\n  background: #3f51b5; }\n.mat-primary .mat-pseudo-checkbox-checked,\n.mat-primary .mat-pseudo-checkbox-indeterminate {\n  background: #1e88e5; }\n.mat-warn .mat-pseudo-checkbox-checked,\n.mat-warn .mat-pseudo-checkbox-indeterminate {\n  background: #e91e63; }\n.mat-pseudo-checkbox-checked.mat-pseudo-checkbox-disabled,\n.mat-pseudo-checkbox-indeterminate.mat-pseudo-checkbox-disabled {\n  background: #b0b0b0; }\n.mat-app-background {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-theme-loaded-marker {\n  display: none; }\n.mat-autocomplete-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover) {\n    background: white; }\n.mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover):not(.mat-option-disabled) {\n      color: rgba(0, 0, 0, 0.87); }\n.mat-badge-content {\n  color: white;\n  background: #1e88e5; }\n.mat-badge-accent .mat-badge-content {\n  background: #3f51b5;\n  color: white; }\n.mat-badge-warn .mat-badge-content {\n  color: white;\n  background: #e91e63; }\n.mat-badge {\n  position: relative; }\n.mat-badge-hidden .mat-badge-content {\n  display: none; }\n.mat-badge-content {\n  position: absolute;\n  text-align: center;\n  display: inline-block;\n  border-radius: 50%;\n  transition: -webkit-transform 200ms ease-in-out;\n  transition: transform 200ms ease-in-out;\n  transition: transform 200ms ease-in-out, -webkit-transform 200ms ease-in-out;\n  -webkit-transform: scale(0.6);\n          transform: scale(0.6);\n  overflow: hidden;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  pointer-events: none; }\n.mat-badge-content.mat-badge-active {\n  -webkit-transform: none;\n          transform: none; }\n.mat-badge-small .mat-badge-content {\n  width: 16px;\n  height: 16px;\n  line-height: 16px; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-badge-small .mat-badge-content {\n      outline: solid 1px;\n      border-radius: 0; } }\n.mat-badge-small.mat-badge-above .mat-badge-content {\n  top: -8px; }\n.mat-badge-small.mat-badge-below .mat-badge-content {\n  bottom: -8px; }\n.mat-badge-small.mat-badge-before {\n  margin-left: 16px; }\n.mat-badge-small.mat-badge-before .mat-badge-content {\n    left: -16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-before {\n  margin-left: 0;\n  margin-right: 16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -16px; }\n.mat-badge-small.mat-badge-after {\n  margin-right: 16px; }\n.mat-badge-small.mat-badge-after .mat-badge-content {\n    right: -16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-after {\n  margin-right: 0;\n  margin-left: 16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -16px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-before {\n  margin-left: 8px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: -8px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-before {\n  margin-left: 0;\n  margin-right: 8px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -8px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-after {\n  margin-right: 8px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: -8px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-after {\n  margin-right: 0;\n  margin-left: 16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -8px; }\n.mat-badge-medium .mat-badge-content {\n  width: 22px;\n  height: 22px;\n  line-height: 22px; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-badge-medium .mat-badge-content {\n      outline: solid 1px;\n      border-radius: 0; } }\n.mat-badge-medium.mat-badge-above .mat-badge-content {\n  top: -11px; }\n.mat-badge-medium.mat-badge-below .mat-badge-content {\n  bottom: -11px; }\n.mat-badge-medium.mat-badge-before {\n  margin-left: 22px; }\n.mat-badge-medium.mat-badge-before .mat-badge-content {\n    left: -22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-before {\n  margin-left: 0;\n  margin-right: 22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -22px; }\n.mat-badge-medium.mat-badge-after {\n  margin-right: 22px; }\n.mat-badge-medium.mat-badge-after .mat-badge-content {\n    right: -22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-after {\n  margin-right: 0;\n  margin-left: 22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -22px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-before {\n  margin-left: 11px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: -11px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-before {\n  margin-left: 0;\n  margin-right: 11px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -11px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-after {\n  margin-right: 11px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: -11px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-after {\n  margin-right: 0;\n  margin-left: 22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -11px; }\n.mat-badge-large .mat-badge-content {\n  width: 28px;\n  height: 28px;\n  line-height: 28px; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-badge-large .mat-badge-content {\n      outline: solid 1px;\n      border-radius: 0; } }\n.mat-badge-large.mat-badge-above .mat-badge-content {\n  top: -14px; }\n.mat-badge-large.mat-badge-below .mat-badge-content {\n  bottom: -14px; }\n.mat-badge-large.mat-badge-before {\n  margin-left: 28px; }\n.mat-badge-large.mat-badge-before .mat-badge-content {\n    left: -28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-before {\n  margin-left: 0;\n  margin-right: 28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -28px; }\n.mat-badge-large.mat-badge-after {\n  margin-right: 28px; }\n.mat-badge-large.mat-badge-after .mat-badge-content {\n    right: -28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-after {\n  margin-right: 0;\n  margin-left: 28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -28px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-before {\n  margin-left: 14px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: -14px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-before {\n  margin-left: 0;\n  margin-right: 14px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -14px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-after {\n  margin-right: 14px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: -14px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-after {\n  margin-right: 0;\n  margin-left: 28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -14px; }\n.mat-bottom-sheet-container {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-button, .mat-icon-button, .mat-stroked-button {\n  color: inherit;\n  background: transparent; }\n.mat-button.mat-primary, .mat-icon-button.mat-primary, .mat-stroked-button.mat-primary {\n    color: #1e88e5; }\n.mat-button.mat-accent, .mat-icon-button.mat-accent, .mat-stroked-button.mat-accent {\n    color: #3f51b5; }\n.mat-button.mat-warn, .mat-icon-button.mat-warn, .mat-stroked-button.mat-warn {\n    color: #e91e63; }\n.mat-button.mat-primary[disabled], .mat-button.mat-accent[disabled], .mat-button.mat-warn[disabled], .mat-button[disabled][disabled], .mat-icon-button.mat-primary[disabled], .mat-icon-button.mat-accent[disabled], .mat-icon-button.mat-warn[disabled], .mat-icon-button[disabled][disabled], .mat-stroked-button.mat-primary[disabled], .mat-stroked-button.mat-accent[disabled], .mat-stroked-button.mat-warn[disabled], .mat-stroked-button[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.26); }\n.mat-button.mat-primary .mat-button-focus-overlay, .mat-icon-button.mat-primary .mat-button-focus-overlay, .mat-stroked-button.mat-primary .mat-button-focus-overlay {\n    background-color: rgba(30, 136, 229, 0.12); }\n.mat-button.mat-accent .mat-button-focus-overlay, .mat-icon-button.mat-accent .mat-button-focus-overlay, .mat-stroked-button.mat-accent .mat-button-focus-overlay {\n    background-color: rgba(63, 81, 181, 0.12); }\n.mat-button.mat-warn .mat-button-focus-overlay, .mat-icon-button.mat-warn .mat-button-focus-overlay, .mat-stroked-button.mat-warn .mat-button-focus-overlay {\n    background-color: rgba(233, 30, 99, 0.12); }\n.mat-button[disabled] .mat-button-focus-overlay, .mat-icon-button[disabled] .mat-button-focus-overlay, .mat-stroked-button[disabled] .mat-button-focus-overlay {\n    background-color: transparent; }\n.mat-button.mat-primary .mat-ripple-element, .mat-icon-button.mat-primary .mat-ripple-element, .mat-stroked-button.mat-primary .mat-ripple-element {\n    background-color: rgba(30, 136, 229, 0.1); }\n.mat-button.mat-accent .mat-ripple-element, .mat-icon-button.mat-accent .mat-ripple-element, .mat-stroked-button.mat-accent .mat-ripple-element {\n    background-color: rgba(63, 81, 181, 0.1); }\n.mat-button.mat-warn .mat-ripple-element, .mat-icon-button.mat-warn .mat-ripple-element, .mat-stroked-button.mat-warn .mat-ripple-element {\n    background-color: rgba(233, 30, 99, 0.1); }\n.mat-flat-button, .mat-raised-button, .mat-fab, .mat-mini-fab {\n  color: rgba(0, 0, 0, 0.87);\n  background-color: white; }\n.mat-flat-button.mat-primary, .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    color: white; }\n.mat-flat-button.mat-accent, .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    color: white; }\n.mat-flat-button.mat-warn, .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    color: white; }\n.mat-flat-button.mat-primary[disabled], .mat-flat-button.mat-accent[disabled], .mat-flat-button.mat-warn[disabled], .mat-flat-button[disabled][disabled], .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.26); }\n.mat-flat-button.mat-primary, .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    background-color: #1e88e5; }\n.mat-flat-button.mat-accent, .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    background-color: #3f51b5; }\n.mat-flat-button.mat-warn, .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    background-color: #e91e63; }\n.mat-flat-button.mat-primary[disabled], .mat-flat-button.mat-accent[disabled], .mat-flat-button.mat-warn[disabled], .mat-flat-button[disabled][disabled], .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    background-color: rgba(0, 0, 0, 0.12); }\n.mat-flat-button.mat-primary .mat-ripple-element, .mat-raised-button.mat-primary .mat-ripple-element, .mat-fab.mat-primary .mat-ripple-element, .mat-mini-fab.mat-primary .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.1); }\n.mat-flat-button.mat-accent .mat-ripple-element, .mat-raised-button.mat-accent .mat-ripple-element, .mat-fab.mat-accent .mat-ripple-element, .mat-mini-fab.mat-accent .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.1); }\n.mat-flat-button.mat-warn .mat-ripple-element, .mat-raised-button.mat-warn .mat-ripple-element, .mat-fab.mat-warn .mat-ripple-element, .mat-mini-fab.mat-warn .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.1); }\n.mat-icon-button.mat-primary .mat-ripple-element {\n  background-color: rgba(30, 136, 229, 0.2); }\n.mat-icon-button.mat-accent .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.2); }\n.mat-icon-button.mat-warn .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.2); }\n.mat-button-toggle {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-button-toggle .mat-button-toggle-focus-overlay {\n    background-color: rgba(0, 0, 0, 0.12); }\n.mat-button-toggle-checked {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.54); }\n.mat-button-toggle-disabled {\n  background-color: #eeeeee;\n  color: rgba(0, 0, 0, 0.26); }\n.mat-button-toggle-disabled.mat-button-toggle-checked {\n    background-color: #bdbdbd; }\n.mat-card {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-card-subtitle {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-checkbox-frame {\n  border-color: rgba(0, 0, 0, 0.54); }\n.mat-checkbox-checkmark {\n  fill: #fafafa; }\n.mat-checkbox-checkmark-path {\n  stroke: #fafafa !important; }\n.mat-checkbox-mixedmark {\n  background-color: #fafafa; }\n.mat-checkbox-indeterminate.mat-primary .mat-checkbox-background, .mat-checkbox-checked.mat-primary .mat-checkbox-background {\n  background-color: #1e88e5; }\n.mat-checkbox-indeterminate.mat-accent .mat-checkbox-background, .mat-checkbox-checked.mat-accent .mat-checkbox-background {\n  background-color: #3f51b5; }\n.mat-checkbox-indeterminate.mat-warn .mat-checkbox-background, .mat-checkbox-checked.mat-warn .mat-checkbox-background {\n  background-color: #e91e63; }\n.mat-checkbox-disabled.mat-checkbox-checked .mat-checkbox-background, .mat-checkbox-disabled.mat-checkbox-indeterminate .mat-checkbox-background {\n  background-color: #b0b0b0; }\n.mat-checkbox-disabled:not(.mat-checkbox-checked) .mat-checkbox-frame {\n  border-color: #b0b0b0; }\n.mat-checkbox-disabled .mat-checkbox-label {\n  color: #b0b0b0; }\n.mat-checkbox:not(.mat-checkbox-disabled).mat-primary .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(30, 136, 229, 0.26); }\n.mat-checkbox:not(.mat-checkbox-disabled).mat-accent .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.26); }\n.mat-checkbox:not(.mat-checkbox-disabled).mat-warn .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.26); }\n.mat-chip.mat-standard-chip {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-chip.mat-standard-chip .mat-chip-remove {\n    color: rgba(0, 0, 0, 0.87);\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-primary {\n  background-color: #1e88e5;\n  color: white; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-primary .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-primary .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-warn {\n  background-color: #e91e63;\n  color: white; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-warn .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-warn .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-accent {\n  background-color: #3f51b5;\n  color: white; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-accent .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-accent .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-table {\n  background: white; }\nmat-row, mat-header-row, mat-footer-row,\nth.mat-header-cell, td.mat-cell, td.mat-footer-cell {\n  border-bottom-color: rgba(0, 0, 0, 0.12); }\n.mat-header-cell {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-cell, .mat-footer-cell {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-calendar-arrow {\n  border-top-color: rgba(0, 0, 0, 0.54); }\n.mat-datepicker-toggle,\n.mat-datepicker-popup .mat-calendar-next-button,\n.mat-datepicker-popup .mat-calendar-previous-button {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-calendar-table-header {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-calendar-table-header-divider::after {\n  background: rgba(0, 0, 0, 0.12); }\n.mat-calendar-body-label {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-calendar-body-cell-content {\n  color: rgba(0, 0, 0, 0.87);\n  border-color: transparent; }\n.mat-calendar-body-disabled > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-calendar-body-cell:not(.mat-calendar-body-disabled):hover > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-keyboard-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-program-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n  background-color: rgba(0, 0, 0, 0.04); }\n.mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-calendar-body-disabled > .mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.18); }\n.mat-calendar-body-selected {\n  background-color: #1e88e5;\n  color: white; }\n.mat-calendar-body-disabled > .mat-calendar-body-selected {\n  background-color: rgba(30, 136, 229, 0.4); }\n.mat-calendar-body-today.mat-calendar-body-selected {\n  box-shadow: inset 0 0 0 1px white; }\n.mat-datepicker-content {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-datepicker-content.mat-accent .mat-calendar-body-selected {\n    background-color: #3f51b5;\n    color: white; }\n.mat-datepicker-content.mat-accent .mat-calendar-body-disabled > .mat-calendar-body-selected {\n    background-color: rgba(63, 81, 181, 0.4); }\n.mat-datepicker-content.mat-accent .mat-calendar-body-today.mat-calendar-body-selected {\n    box-shadow: inset 0 0 0 1px white; }\n.mat-datepicker-content.mat-warn .mat-calendar-body-selected {\n    background-color: #e91e63;\n    color: white; }\n.mat-datepicker-content.mat-warn .mat-calendar-body-disabled > .mat-calendar-body-selected {\n    background-color: rgba(233, 30, 99, 0.4); }\n.mat-datepicker-content.mat-warn .mat-calendar-body-today.mat-calendar-body-selected {\n    box-shadow: inset 0 0 0 1px white; }\n.mat-datepicker-toggle-active {\n  color: #1e88e5; }\n.mat-datepicker-toggle-active.mat-accent {\n    color: #3f51b5; }\n.mat-datepicker-toggle-active.mat-warn {\n    color: #e91e63; }\n.mat-dialog-container {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-divider {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n.mat-divider-vertical {\n  border-right-color: rgba(0, 0, 0, 0.12); }\n.mat-expansion-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-action-row {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n.mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-keyboard-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-program-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']):hover {\n  background: rgba(0, 0, 0, 0.04); }\n.mat-expansion-panel-header-title {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-expansion-panel-header-description,\n.mat-expansion-indicator::after {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-expansion-panel-header[aria-disabled='true'] {\n  color: rgba(0, 0, 0, 0.26); }\n.mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-title,\n  .mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-description {\n    color: inherit; }\n.mat-form-field-label {\n  color: rgba(0, 0, 0, 0.6); }\n.mat-hint {\n  color: rgba(0, 0, 0, 0.6); }\n.mat-form-field.mat-focused .mat-form-field-label {\n  color: #1e88e5; }\n.mat-form-field.mat-focused .mat-form-field-label.mat-accent {\n    color: #3f51b5; }\n.mat-form-field.mat-focused .mat-form-field-label.mat-warn {\n    color: #e91e63; }\n.mat-focused .mat-form-field-required-marker {\n  color: #3f51b5; }\n.mat-form-field-ripple {\n  background-color: rgba(0, 0, 0, 0.87); }\n.mat-form-field.mat-focused .mat-form-field-ripple {\n  background-color: #1e88e5; }\n.mat-form-field.mat-focused .mat-form-field-ripple.mat-accent {\n    background-color: #3f51b5; }\n.mat-form-field.mat-focused .mat-form-field-ripple.mat-warn {\n    background-color: #e91e63; }\n.mat-form-field.mat-form-field-invalid .mat-form-field-label {\n  color: #e91e63; }\n.mat-form-field.mat-form-field-invalid .mat-form-field-label.mat-accent,\n  .mat-form-field.mat-form-field-invalid .mat-form-field-label .mat-form-field-required-marker {\n    color: #e91e63; }\n.mat-form-field.mat-form-field-invalid .mat-form-field-ripple {\n  background-color: #e91e63; }\n.mat-error {\n  color: #e91e63; }\n.mat-form-field-appearance-legacy .mat-form-field-label {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-form-field-appearance-legacy .mat-hint {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n  background-color: rgba(0, 0, 0, 0.42); }\n.mat-form-field-appearance-legacy.mat-form-field-disabled .mat-form-field-underline {\n  background-image: linear-gradient(to right, rgba(0, 0, 0, 0.42) 0%, rgba(0, 0, 0, 0.42) 33%, transparent 0%);\n  background-size: 4px 100%;\n  background-repeat: repeat-x; }\n.mat-form-field-appearance-standard .mat-form-field-underline {\n  background-color: rgba(0, 0, 0, 0.42); }\n.mat-form-field-appearance-standard.mat-form-field-disabled .mat-form-field-underline {\n  background-image: linear-gradient(to right, rgba(0, 0, 0, 0.42) 0%, rgba(0, 0, 0, 0.42) 33%, transparent 0%);\n  background-size: 4px 100%;\n  background-repeat: repeat-x; }\n.mat-form-field-appearance-fill .mat-form-field-flex {\n  background-color: rgba(0, 0, 0, 0.04); }\n.mat-form-field-appearance-fill.mat-form-field-disabled .mat-form-field-flex {\n  background-color: rgba(0, 0, 0, 0.02); }\n.mat-form-field-appearance-fill .mat-form-field-underline::before {\n  background-color: rgba(0, 0, 0, 0.42); }\n.mat-form-field-appearance-fill.mat-form-field-disabled .mat-form-field-label {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-form-field-appearance-fill.mat-form-field-disabled .mat-form-field-underline::before {\n  background-color: transparent; }\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  color: rgba(0, 0, 0, 0.12); }\n.mat-form-field-appearance-outline .mat-form-field-outline-thick {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {\n  color: #1e88e5; }\n.mat-form-field-appearance-outline.mat-focused.mat-accent .mat-form-field-outline-thick {\n  color: #3f51b5; }\n.mat-form-field-appearance-outline.mat-focused.mat-warn .mat-form-field-outline-thick {\n  color: #e91e63; }\n.mat-form-field-appearance-outline.mat-form-field-invalid.mat-form-field-invalid .mat-form-field-outline-thick {\n  color: #e91e63; }\n.mat-form-field-appearance-outline.mat-form-field-disabled .mat-form-field-label {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-form-field-appearance-outline.mat-form-field-disabled .mat-form-field-outline {\n  color: rgba(0, 0, 0, 0.06); }\n.mat-icon.mat-primary {\n  color: #1e88e5; }\n.mat-icon.mat-accent {\n  color: #3f51b5; }\n.mat-icon.mat-warn {\n  color: #e91e63; }\n.mat-input-element:disabled {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-input-element {\n  caret-color: #1e88e5; }\n.mat-input-element::-webkit-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element:-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::-moz-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::-webkit-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element:-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-accent .mat-input-element {\n  caret-color: #3f51b5; }\n.mat-warn .mat-input-element,\n.mat-form-field-invalid .mat-input-element {\n  caret-color: #e91e63; }\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-list-item-disabled {\n  background-color: #eeeeee; }\n.mat-list-option:hover, .mat-list-option.mat-list-item-focus,\n.mat-nav-list .mat-list-item:hover,\n.mat-nav-list .mat-list-item.mat-list-item-focus {\n  background: rgba(0, 0, 0, 0.04); }\n.mat-menu-panel {\n  background: white; }\n.mat-menu-item {\n  background: transparent;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-menu-item[disabled], .mat-menu-item[disabled]::after {\n    color: rgba(0, 0, 0, 0.38); }\n.mat-menu-item .mat-icon:not([color]),\n.mat-menu-item-submenu-trigger::after {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-menu-item:hover:not([disabled]),\n.mat-menu-item.cdk-program-focused:not([disabled]),\n.mat-menu-item.cdk-keyboard-focused:not([disabled]),\n.mat-menu-item-highlighted:not([disabled]) {\n  background: rgba(0, 0, 0, 0.04); }\n.mat-paginator {\n  background: white; }\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-paginator-decrement,\n.mat-paginator-increment {\n  border-top: 2px solid rgba(0, 0, 0, 0.54);\n  border-right: 2px solid rgba(0, 0, 0, 0.54); }\n.mat-paginator-first,\n.mat-paginator-last {\n  border-top: 2px solid rgba(0, 0, 0, 0.54); }\n.mat-icon-button[disabled] .mat-paginator-decrement,\n.mat-icon-button[disabled] .mat-paginator-increment,\n.mat-icon-button[disabled] .mat-paginator-first,\n.mat-icon-button[disabled] .mat-paginator-last {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-progress-bar-background {\n  fill: #bbdefb; }\n.mat-progress-bar-buffer {\n  background-color: #bbdefb; }\n.mat-progress-bar-fill::after {\n  background-color: #1e88e5; }\n.mat-progress-bar.mat-accent .mat-progress-bar-background {\n  fill: #c5cae9; }\n.mat-progress-bar.mat-accent .mat-progress-bar-buffer {\n  background-color: #c5cae9; }\n.mat-progress-bar.mat-accent .mat-progress-bar-fill::after {\n  background-color: #3f51b5; }\n.mat-progress-bar.mat-warn .mat-progress-bar-background {\n  fill: #f8bbd0; }\n.mat-progress-bar.mat-warn .mat-progress-bar-buffer {\n  background-color: #f8bbd0; }\n.mat-progress-bar.mat-warn .mat-progress-bar-fill::after {\n  background-color: #e91e63; }\n.mat-progress-spinner circle, .mat-spinner circle {\n  stroke: #1e88e5; }\n.mat-progress-spinner.mat-accent circle, .mat-spinner.mat-accent circle {\n  stroke: #3f51b5; }\n.mat-progress-spinner.mat-warn circle, .mat-spinner.mat-warn circle {\n  stroke: #e91e63; }\n.mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.54); }\n.mat-radio-disabled .mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-radio-disabled .mat-radio-ripple .mat-ripple-element, .mat-radio-disabled .mat-radio-inner-circle {\n  background-color: rgba(0, 0, 0, 0.38); }\n.mat-radio-disabled .mat-radio-label-content {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #1e88e5; }\n.mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #1e88e5; }\n.mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(30, 136, 229, 0.26); }\n.mat-radio-button.mat-accent.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #3f51b5; }\n.mat-radio-button.mat-accent .mat-radio-inner-circle {\n  background-color: #3f51b5; }\n.mat-radio-button.mat-accent .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.26); }\n.mat-radio-button.mat-warn.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #e91e63; }\n.mat-radio-button.mat-warn .mat-radio-inner-circle {\n  background-color: #e91e63; }\n.mat-radio-button.mat-warn .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.26); }\n.mat-select-content, .mat-select-panel-done-animating {\n  background: white; }\n.mat-select-value {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-select-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n.mat-select-disabled .mat-select-value {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-select-arrow {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-select-panel .mat-option.mat-selected:not(.mat-option-multiple) {\n  background: rgba(0, 0, 0, 0.12); }\n.mat-form-field.mat-focused.mat-primary .mat-select-arrow {\n  color: #1e88e5; }\n.mat-form-field.mat-focused.mat-accent .mat-select-arrow {\n  color: #3f51b5; }\n.mat-form-field.mat-focused.mat-warn .mat-select-arrow {\n  color: #e91e63; }\n.mat-form-field .mat-select.mat-select-invalid .mat-select-arrow {\n  color: #e91e63; }\n.mat-form-field .mat-select.mat-select-disabled .mat-select-arrow {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-drawer-container {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-drawer {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-drawer.mat-drawer-push {\n    background-color: white; }\n.mat-drawer-backdrop.mat-drawer-shown {\n  background-color: rgba(0, 0, 0, 0.6); }\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #3f51b5; }\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(63, 81, 181, 0.5); }\n.mat-slide-toggle:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n.mat-slide-toggle .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.12); }\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #2196f3; }\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(33, 150, 243, 0.5); }\n.mat-slide-toggle.mat-primary:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n.mat-slide-toggle.mat-primary .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.12); }\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #e91e63; }\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(233, 30, 99, 0.5); }\n.mat-slide-toggle.mat-warn:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n.mat-slide-toggle.mat-warn .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.12); }\n.mat-disabled .mat-slide-toggle-thumb {\n  background-color: #bdbdbd; }\n.mat-disabled .mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.1); }\n.mat-slide-toggle-thumb {\n  background-color: #fafafa; }\n.mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.38); }\n.mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-primary .mat-slider-track-fill,\n.mat-primary .mat-slider-thumb,\n.mat-primary .mat-slider-thumb-label {\n  background-color: #1e88e5; }\n.mat-primary .mat-slider-thumb-label-text {\n  color: white; }\n.mat-accent .mat-slider-track-fill,\n.mat-accent .mat-slider-thumb,\n.mat-accent .mat-slider-thumb-label {\n  background-color: #3f51b5; }\n.mat-accent .mat-slider-thumb-label-text {\n  color: white; }\n.mat-warn .mat-slider-track-fill,\n.mat-warn .mat-slider-thumb,\n.mat-warn .mat-slider-thumb-label {\n  background-color: #e91e63; }\n.mat-warn .mat-slider-thumb-label-text {\n  color: white; }\n.mat-slider-focus-ring {\n  background-color: rgba(63, 81, 181, 0.2); }\n.mat-slider:hover .mat-slider-track-background,\n.cdk-focused .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.38); }\n.mat-slider-disabled .mat-slider-track-background,\n.mat-slider-disabled .mat-slider-track-fill,\n.mat-slider-disabled .mat-slider-thumb {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-disabled:hover .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-min-value .mat-slider-focus-ring {\n  background-color: rgba(0, 0, 0, 0.12); }\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.87); }\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing) .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26);\n  background-color: transparent; }\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover.mat-slider-disabled .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused.mat-slider-disabled .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-has-ticks .mat-slider-wrapper::after {\n  border-color: rgba(0, 0, 0, 0.7); }\n.mat-slider-horizontal .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent);\n  background-image: -moz-repeating-linear-gradient(0.0001deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n.mat-slider-vertical .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to bottom, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n.mat-step-header.cdk-keyboard-focused, .mat-step-header.cdk-program-focused, .mat-step-header:hover {\n  background-color: rgba(0, 0, 0, 0.04); }\n.mat-step-header .mat-step-label,\n.mat-step-header .mat-step-optional {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-step-header .mat-step-icon {\n  background-color: #1e88e5;\n  color: white; }\n.mat-step-header .mat-step-icon-not-touched {\n  background-color: rgba(0, 0, 0, 0.38);\n  color: white; }\n.mat-step-header .mat-step-label.mat-step-label-active {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-stepper-horizontal, .mat-stepper-vertical {\n  background-color: white; }\n.mat-stepper-vertical-line::before {\n  border-left-color: rgba(0, 0, 0, 0.12); }\n.mat-stepper-horizontal-line {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n.mat-tab-nav-bar,\n.mat-tab-header {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n.mat-tab-group-inverted-header .mat-tab-nav-bar,\n.mat-tab-group-inverted-header .mat-tab-header {\n  border-top: 1px solid rgba(0, 0, 0, 0.12);\n  border-bottom: none; }\n.mat-tab-label, .mat-tab-link {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-tab-label.mat-tab-disabled, .mat-tab-link.mat-tab-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n.mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.87); }\n.mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-tab-group[class*='mat-background-'] .mat-tab-header,\n.mat-tab-nav-bar[class*='mat-background-'] {\n  border-bottom: none;\n  border-top: none; }\n.mat-tab-group.mat-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-primary .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-primary .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n.mat-tab-group.mat-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary .mat-ink-bar {\n  background-color: #1e88e5; }\n.mat-tab-group.mat-primary.mat-background-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary.mat-background-primary .mat-ink-bar {\n  background-color: white; }\n.mat-tab-group.mat-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-accent .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-accent .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(197, 202, 233, 0.3); }\n.mat-tab-group.mat-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent .mat-ink-bar {\n  background-color: #3f51b5; }\n.mat-tab-group.mat-accent.mat-background-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent.mat-background-accent .mat-ink-bar {\n  background-color: white; }\n.mat-tab-group.mat-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-warn .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-warn .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(248, 187, 208, 0.3); }\n.mat-tab-group.mat-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn .mat-ink-bar {\n  background-color: #e91e63; }\n.mat-tab-group.mat-warn.mat-background-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn.mat-background-warn .mat-ink-bar {\n  background-color: white; }\n.mat-tab-group.mat-background-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-background-primary .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-background-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-background-primary .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n.mat-tab-group.mat-background-primary .mat-tab-header, .mat-tab-group.mat-background-primary .mat-tab-links, .mat-tab-nav-bar.mat-background-primary .mat-tab-header, .mat-tab-nav-bar.mat-background-primary .mat-tab-links {\n  background-color: #1e88e5; }\n.mat-tab-group.mat-background-primary .mat-tab-label, .mat-tab-group.mat-background-primary .mat-tab-link, .mat-tab-nav-bar.mat-background-primary .mat-tab-label, .mat-tab-nav-bar.mat-background-primary .mat-tab-link {\n  color: white; }\n.mat-tab-group.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-primary .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-chevron {\n  border-color: white; }\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-primary .mat-ripple-element, .mat-tab-nav-bar.mat-background-primary .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n.mat-tab-group.mat-background-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-background-accent .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-background-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-background-accent .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(197, 202, 233, 0.3); }\n.mat-tab-group.mat-background-accent .mat-tab-header, .mat-tab-group.mat-background-accent .mat-tab-links, .mat-tab-nav-bar.mat-background-accent .mat-tab-header, .mat-tab-nav-bar.mat-background-accent .mat-tab-links {\n  background-color: #3f51b5; }\n.mat-tab-group.mat-background-accent .mat-tab-label, .mat-tab-group.mat-background-accent .mat-tab-link, .mat-tab-nav-bar.mat-background-accent .mat-tab-label, .mat-tab-nav-bar.mat-background-accent .mat-tab-link {\n  color: white; }\n.mat-tab-group.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-accent .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-chevron {\n  border-color: white; }\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-accent .mat-ripple-element, .mat-tab-nav-bar.mat-background-accent .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n.mat-tab-group.mat-background-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-background-warn .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-background-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-background-warn .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(248, 187, 208, 0.3); }\n.mat-tab-group.mat-background-warn .mat-tab-header, .mat-tab-group.mat-background-warn .mat-tab-links, .mat-tab-nav-bar.mat-background-warn .mat-tab-header, .mat-tab-nav-bar.mat-background-warn .mat-tab-links {\n  background-color: #e91e63; }\n.mat-tab-group.mat-background-warn .mat-tab-label, .mat-tab-group.mat-background-warn .mat-tab-link, .mat-tab-nav-bar.mat-background-warn .mat-tab-label, .mat-tab-nav-bar.mat-background-warn .mat-tab-link {\n  color: white; }\n.mat-tab-group.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-warn .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-chevron {\n  border-color: white; }\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-warn .mat-ripple-element, .mat-tab-nav-bar.mat-background-warn .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n.mat-toolbar {\n  background: whitesmoke;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-toolbar.mat-primary {\n    background: #1e88e5;\n    color: white; }\n.mat-toolbar.mat-accent {\n    background: #3f51b5;\n    color: white; }\n.mat-toolbar.mat-warn {\n    background: #e91e63;\n    color: white; }\n.mat-toolbar .mat-form-field-underline,\n  .mat-toolbar .mat-form-field-ripple,\n  .mat-toolbar .mat-focused .mat-form-field-ripple {\n    background-color: currentColor; }\n.mat-toolbar .mat-form-field-label,\n  .mat-toolbar .mat-focused .mat-form-field-label,\n  .mat-toolbar .mat-select-value,\n  .mat-toolbar .mat-select-arrow,\n  .mat-toolbar .mat-form-field.mat-focused .mat-select-arrow {\n    color: inherit; }\n.mat-toolbar .mat-input-element {\n    caret-color: currentColor; }\n.mat-tooltip {\n  background: rgba(97, 97, 97, 0.9); }\n.mat-tree {\n  background: white; }\n.mat-tree-node {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-snack-bar-container {\n  background: #323232;\n  color: white; }\n.mat-simple-snackbar-action {\n  color: #3f51b5; }\n/*Theme Colors*/\n/*bootstrap Color*/\n/*Light colors*/\n/*Normal Color*/\n/*Extra Variable*/\n.pricing-box {\n  padding-top: 60px;\n  padding-bottom: 40px; }\n.pricing-plan mat-card-title {\n  margin-top: 30px; }\n.pricing-plan h1 {\n  margin: 7px 0;\n  font-size: 55px; }\n.pricing-plan sup {\n  font-size: 18px; }\n.pricing-plan .price-listing .mat-list-item .mat-list-item-content {\n  padding: 15px 0; }\n.pricing-plan .pop-tag {\n  padding: 0 5px;\n  border-radius: 4px;\n  background: #1e88e5;\n  position: absolute;\n  top: -13px;\n  color: #ffffff;\n  font-size: 12px;\n  width: 100px;\n  margin: 0 auto;\n  left: 0;\n  right: 0; }\n"

/***/ }),

/***/ "./src/app/pages/pricing/pricing.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/pricing/pricing.component.ts ***!
  \****************************************************/
/*! exports provided: PricingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricingComponent", function() { return PricingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PricingComponent = /** @class */ (function () {
    function PricingComponent() {
    }
    PricingComponent.prototype.ngOnInit = function () {
    };
    PricingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-pricing',
            template: __webpack_require__(/*! ./pricing.component.html */ "./src/app/pages/pricing/pricing.component.html"),
            styles: [__webpack_require__(/*! ./pricing.component.scss */ "./src/app/pages/pricing/pricing.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PricingComponent);
    return PricingComponent;
}());



/***/ }),

/***/ "./src/app/pages/timeline/timeline.component.html":
/*!********************************************************!*\
  !*** ./src/app/pages/timeline/timeline.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card>\n    <mat-card-content>\n        <ul class=\"timeline\">\n            <li>\n                <div class=\"timeline-badge success\"><img class=\"img-responsive\" alt=\"user\" src=\"assets/images/users/1.jpg\" alt=\"img\"> </div>\n                <div class=\"timeline-panel\">\n                    <div class=\"timeline-heading\">\n                        <h4 class=\"timeline-title\">Genelia</h4>\n                        <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i> 11 hours ago via Twitter</small> </p>\n                    </div>\n                    <div class=\"timeline-body\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero laboriosam dolor perspiciatis omnis exercitationem. Beatae, officia pariatur? Est cum veniam excepturi. Maiores praesentium, porro voluptas suscipit facere rem dicta, debitis.</p>\n                    </div>\n                </div>\n            </li>\n            <li class=\"timeline-inverted\">\n                <div class=\"timeline-badge warning\"><img class=\"img-responsive\" alt=\"user\" src=\"assets/images/users/2.jpg\" alt=\"img\"> </div>\n                <div class=\"timeline-panel\">\n                    <div class=\"timeline-heading\">\n                        <h4 class=\"timeline-title\">Ritesh Deshmukh</h4>\n                    </div>\n                    <div class=\"timeline-body\">\n                        <p><img class=\"img-responsive\" alt=\"user\" src=\"assets/images/users/3.jpg\" alt=\"img\"></p>\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium maiores odit qui est tempora eos, nostrum provident explicabo dignissimos debitis vel! Adipisci eius voluptates, ad aut recusandae minus eaque facere.</p>\n                    </div>\n                </div>\n            </li>\n            <li>\n                <div class=\"timeline-badge danger\"><i class=\"fa fa-bomb\"></i> </div>\n                <div class=\"timeline-panel\">\n                    <div class=\"timeline-heading\">\n                        <h4 class=\"timeline-title\">Lorem ipsum dolor</h4>\n                    </div>\n                    <div class=\"timeline-body\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus numquam facilis enim eaque, tenetur nam id qui vel velit similique nihil iure molestias aliquam, voluptatem totam quaerat, magni commodi quisquam.</p>\n                    </div>\n                </div>\n            </li>\n            <li class=\"timeline-inverted\">\n                <div class=\"timeline-panel\">\n                    <div class=\"timeline-heading\">\n                        <h4 class=\"timeline-title\">Lorem ipsum dolor</h4>\n                    </div>\n                    <div class=\"timeline-body\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates est quaerat asperiores sapiente, eligendi, nihil. Itaque quos, alias sapiente rerum quas odit! Aperiam officiis quidem delectus libero, omnis ut debitis!</p>\n                    </div>\n                </div>\n            </li>\n            <li>\n                <div class=\"timeline-badge info\"><i class=\"fa fa-save\"></i> </div>\n                <div class=\"timeline-panel\">\n                    <div class=\"timeline-heading\">\n                        <h4 class=\"timeline-title\">Lorem ipsum dolor</h4>\n                    </div>\n                    <div class=\"timeline-body\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis minus modi quam ipsum alias at est molestiae excepturi delectus nesciunt, quibusdam debitis amet, beatae consequuntur impedit nulla qui! Laborum, atque.</p>\n                        <hr>\n                        <button mat-icon-button [matMenuTriggerFor]=\"menu2\"><mat-icon>more_horiz</mat-icon></button>\n                        <mat-menu #menu2=\"matMenu\">\n                          <button mat-menu-item>Item 1</button>\n                          <button mat-menu-item>Item 2</button>\n                        </mat-menu>\n                    </div>\n                </div>\n            </li>\n            <li class=\"timeline-inverted\">\n                <div class=\"timeline-badge success\"><i class=\"fa fa-graduation-cap\"></i> </div>\n                <div class=\"timeline-panel\">\n                    <div class=\"timeline-heading\">\n                        <h4 class=\"timeline-title\">Lorem ipsum dolor</h4>\n                    </div>\n                    <div class=\"timeline-body\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt obcaecati, quaerat tempore officia voluptas debitis consectetur culpa amet, accusamus dolorum fugiat, animi dicta aperiam, enim incidunt quisquam maxime neque eaque.</p>\n                    </div>\n                </div>\n            </li>\n        </ul>\n    </mat-card-content>\n</mat-card>\n      \n  \n"

/***/ }),

/***/ "./src/app/pages/timeline/timeline.component.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/timeline/timeline.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\");\n/*\r\nTemplate Name: Material Pro Admin\r\nAuthor: Themedesigner\r\nEmail: niravjoshi87@gmail.com\r\nFile: scss\r\n*/\n/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n/*Material Theme Colors*/\n.mat-elevation-z0 {\n  box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z1 {\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z2 {\n  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z3 {\n  box-shadow: 0px 3px 3px -2px rgba(0, 0, 0, 0.2), 0px 3px 4px 0px rgba(0, 0, 0, 0.14), 0px 1px 8px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z4 {\n  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z5 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z6 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z7 {\n  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z8 {\n  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z9 {\n  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2), 0px 9px 12px 1px rgba(0, 0, 0, 0.14), 0px 3px 16px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z10 {\n  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2), 0px 10px 14px 1px rgba(0, 0, 0, 0.14), 0px 4px 18px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z11 {\n  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2), 0px 11px 15px 1px rgba(0, 0, 0, 0.14), 0px 4px 20px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z12 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z13 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 5px 24px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z14 {\n  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2), 0px 14px 21px 2px rgba(0, 0, 0, 0.14), 0px 5px 26px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z15 {\n  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2), 0px 15px 22px 2px rgba(0, 0, 0, 0.14), 0px 6px 28px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z16 {\n  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z17 {\n  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2), 0px 17px 26px 2px rgba(0, 0, 0, 0.14), 0px 6px 32px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z18 {\n  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2), 0px 18px 28px 2px rgba(0, 0, 0, 0.14), 0px 7px 34px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z19 {\n  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2), 0px 19px 29px 2px rgba(0, 0, 0, 0.14), 0px 7px 36px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z20 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 20px 31px 3px rgba(0, 0, 0, 0.14), 0px 8px 38px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z21 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 21px 33px 3px rgba(0, 0, 0, 0.14), 0px 8px 40px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z22 {\n  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2), 0px 22px 35px 3px rgba(0, 0, 0, 0.14), 0px 8px 42px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z23 {\n  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2), 0px 23px 36px 3px rgba(0, 0, 0, 0.14), 0px 9px 44px 8px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z24 {\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 9px 46px 8px rgba(0, 0, 0, 0.12); }\n.mat-badge-content {\n  font-weight: 600;\n  font-size: 12px;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-badge-small .mat-badge-content {\n  font-size: 6px; }\n.mat-badge-large .mat-badge-content {\n  font-size: 24px; }\n.mat-h1, .mat-headline, .mat-typography h1 {\n  font: 400 24px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h2, .mat-title, .mat-typography h2 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h3, .mat-subheading-2, .mat-typography h3 {\n  font: 400 16px/28px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h4, .mat-subheading-1, .mat-typography h4 {\n  font: 400 15px/24px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 16px; }\n.mat-h5, .mat-typography h5 {\n  font: 400 11.62px/20px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 12px; }\n.mat-h6, .mat-typography h6 {\n  font: 400 9.38px/20px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 12px; }\n.mat-body-strong, .mat-body-2 {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-body, .mat-body-1, .mat-typography {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-body p, .mat-body-1 p, .mat-typography p {\n    margin: 0 0 12px; }\n.mat-small, .mat-caption {\n  font: 400 12px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-display-4, .mat-typography .mat-display-4 {\n  font: 300 112px/112px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 56px;\n  letter-spacing: -0.05em; }\n.mat-display-3, .mat-typography .mat-display-3 {\n  font: 400 56px/56px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.02em; }\n.mat-display-2, .mat-typography .mat-display-2 {\n  font: 400 45px/48px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.005em; }\n.mat-display-1, .mat-typography .mat-display-1 {\n  font: 400 34px/40px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0 0 64px; }\n.mat-bottom-sheet-container {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-button, .mat-raised-button, .mat-icon-button, .mat-stroked-button,\n.mat-flat-button, .mat-fab, .mat-mini-fab {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-button-toggle {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-card {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-card-title {\n  font-size: 24px;\n  font-weight: 400; }\n.mat-card-subtitle,\n.mat-card-content,\n.mat-card-header .mat-card-title {\n  font-size: 14px; }\n.mat-checkbox {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px; }\n.mat-chip {\n  font-size: 13px;\n  line-height: 18px; }\n.mat-chip .mat-chip-trailing-icon.mat-icon,\n  .mat-chip .mat-chip-remove.mat-icon {\n    font-size: 18px; }\n.mat-table {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-header-cell {\n  font-size: 12px;\n  font-weight: 500; }\n.mat-cell, .mat-footer-cell {\n  font-size: 14px; }\n.mat-calendar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-calendar-body {\n  font-size: 13px; }\n.mat-calendar-body-label,\n.mat-calendar-period-button {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-calendar-table-header th {\n  font-size: 11px;\n  font-weight: 400; }\n.mat-dialog-title {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-expansion-panel-header {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 15px;\n  font-weight: 400; }\n.mat-expansion-panel-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-form-field {\n  font-size: inherit;\n  font-weight: 400;\n  line-height: 1.125;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-form-field-wrapper {\n  padding-bottom: 1.34375em; }\n.mat-form-field-prefix .mat-icon,\n.mat-form-field-suffix .mat-icon {\n  font-size: 150%;\n  line-height: 1.125; }\n.mat-form-field-prefix .mat-icon-button,\n.mat-form-field-suffix .mat-icon-button {\n  height: 1.5em;\n  width: 1.5em; }\n.mat-form-field-prefix .mat-icon-button .mat-icon,\n  .mat-form-field-suffix .mat-icon-button .mat-icon {\n    height: 1.125em;\n    line-height: 1.125; }\n.mat-form-field-infix {\n  padding: 0.5em 0;\n  border-top: 0.84375em solid transparent; }\n.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.34375em) scale(0.75);\n          transform: translateY(-1.34375em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.34374em) scale(0.75);\n          transform: translateY(-1.34374em) scale(0.75);\n  width: 133.33334333%; }\n.mat-form-field-label-wrapper {\n  top: -0.84375em;\n  padding-top: 0.84375em; }\n.mat-form-field-label {\n  top: 1.34375em; }\n.mat-form-field-underline {\n  bottom: 1.34375em; }\n.mat-form-field-subscript-wrapper {\n  font-size: 75%;\n  margin-top: 0.66666667em;\n  top: calc(100% - 1.79166667em); }\n.mat-form-field-appearance-legacy .mat-form-field-wrapper {\n  padding-bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-infix {\n  padding: 0.4375em 0; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px);\n  -ms-transform: translateY(-1.28125em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00101px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00101px);\n  -ms-transform: translateY(-1.28124em) scale(0.75);\n  width: 133.33334333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00102px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00102px);\n  -ms-transform: translateY(-1.28123em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-appearance-legacy .mat-form-field-label {\n  top: 1.28125em; }\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n  bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper {\n  margin-top: 0.54166667em;\n  top: calc(100% - 1.66666667em); }\n.mat-form-field-appearance-fill .mat-form-field-infix {\n  padding: 0.25em 0 0.75em 0; }\n.mat-form-field-appearance-fill .mat-form-field-label {\n  top: 1.09375em;\n  margin-top: -0.5em; }\n.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-0.59375em) scale(0.75);\n          transform: translateY(-0.59375em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-0.59374em) scale(0.75);\n          transform: translateY(-0.59374em) scale(0.75);\n  width: 133.33334333%; }\n.mat-form-field-appearance-outline .mat-form-field-infix {\n  padding: 1em 0 1em 0; }\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  bottom: 1.34375em; }\n.mat-form-field-appearance-outline .mat-form-field-label {\n  top: 1.84375em;\n  margin-top: -0.25em; }\n.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.59375em) scale(0.75);\n          transform: translateY(-1.59375em) scale(0.75);\n  width: 133.33333333%; }\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.59374em) scale(0.75);\n          transform: translateY(-1.59374em) scale(0.75);\n  width: 133.33334333%; }\n.mat-grid-tile-header,\n.mat-grid-tile-footer {\n  font-size: 14px; }\n.mat-grid-tile-header .mat-line,\n  .mat-grid-tile-footer .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-grid-tile-header .mat-line:nth-child(n+2),\n    .mat-grid-tile-footer .mat-line:nth-child(n+2) {\n      font-size: 12px; }\ninput.mat-input-element {\n  margin-top: -0.0625em; }\n.mat-menu-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px; }\n.mat-radio-button {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-select {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-select-trigger {\n  height: 1.125em; }\n.mat-slide-toggle-content {\n  font: 400 14px/20px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-slider-thumb-label-text {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-stepper-vertical, .mat-stepper-horizontal {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-step-label {\n  font-size: 14px;\n  font-weight: 400; }\n.mat-step-label-selected {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-tab-group {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-tab-label, .mat-tab-link {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-toolbar,\n.mat-toolbar h1,\n.mat-toolbar h2,\n.mat-toolbar h3,\n.mat-toolbar h4,\n.mat-toolbar h5,\n.mat-toolbar h6 {\n  font: 500 20px/32px Roboto, \"Helvetica Neue\", sans-serif;\n  margin: 0; }\n.mat-tooltip {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 10px;\n  padding-top: 6px;\n  padding-bottom: 6px; }\n.mat-tooltip-handset {\n  font-size: 14px;\n  padding-top: 9px;\n  padding-bottom: 9px; }\n.mat-list-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-list-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  font-size: 16px; }\n.mat-list .mat-list-item .mat-line, .mat-nav-list .mat-list-item .mat-line, .mat-selection-list .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  font-size: 16px; }\n.mat-list .mat-list-option .mat-line, .mat-nav-list .mat-list-option .mat-line, .mat-selection-list .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-list[dense] .mat-list-item, .mat-nav-list[dense] .mat-list-item, .mat-selection-list[dense] .mat-list-item {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-item .mat-line, .mat-nav-list[dense] .mat-list-item .mat-line, .mat-selection-list[dense] .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-list-option, .mat-nav-list[dense] .mat-list-option, .mat-selection-list[dense] .mat-list-option {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-option .mat-line, .mat-nav-list[dense] .mat-list-option .mat-line, .mat-selection-list[dense] .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-subheader, .mat-nav-list[dense] .mat-subheader, .mat-selection-list[dense] .mat-subheader {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-option {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 16px; }\n.mat-optgroup-label {\n  font: 500 14px/24px Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-simple-snackbar {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-size: 14px; }\n.mat-simple-snackbar-action {\n  line-height: 1;\n  font-family: inherit;\n  font-size: inherit;\n  font-weight: 500; }\n.mat-tree {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n.mat-tree-node {\n  font-weight: 400;\n  font-size: 14px; }\n.mat-ripple {\n  overflow: hidden; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-ripple {\n      display: none; } }\n.mat-ripple.mat-ripple-unbounded {\n  overflow: visible; }\n.mat-ripple-element {\n  position: absolute;\n  border-radius: 50%;\n  pointer-events: none;\n  transition: opacity, -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  -webkit-transform: scale(0);\n          transform: scale(0); }\n.cdk-visually-hidden {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n  outline: 0;\n  -webkit-appearance: none;\n  -moz-appearance: none; }\n.cdk-overlay-container, .cdk-global-overlay-wrapper {\n  pointer-events: none;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n.cdk-overlay-container {\n  position: fixed;\n  z-index: 1000; }\n.cdk-overlay-container:empty {\n    display: none; }\n.cdk-global-overlay-wrapper {\n  display: flex;\n  position: absolute;\n  z-index: 1000; }\n.cdk-overlay-pane {\n  position: absolute;\n  pointer-events: auto;\n  box-sizing: border-box;\n  z-index: 1000;\n  display: flex;\n  max-width: 100%;\n  max-height: 100%; }\n.cdk-overlay-backdrop {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1000;\n  pointer-events: auto;\n  -webkit-tap-highlight-color: transparent;\n  transition: opacity 400ms cubic-bezier(0.25, 0.8, 0.25, 1);\n  opacity: 0; }\n.cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n    opacity: 1; }\n@media screen and (-ms-high-contrast: active) {\n      .cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n        opacity: 0.6; } }\n.cdk-overlay-dark-backdrop {\n  background: rgba(0, 0, 0, 0.288); }\n.cdk-overlay-transparent-backdrop, .cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing {\n  opacity: 0; }\n.cdk-overlay-connected-position-bounding-box {\n  position: absolute;\n  z-index: 1000;\n  display: flex;\n  flex-direction: column;\n  min-width: 1px;\n  min-height: 1px; }\n.cdk-global-scrollblock {\n  position: fixed;\n  width: 100%;\n  overflow-y: scroll; }\n@-webkit-keyframes cdk-text-field-autofill-start {}\n@keyframes cdk-text-field-autofill-start {}\n@-webkit-keyframes cdk-text-field-autofill-end {}\n@keyframes cdk-text-field-autofill-end {}\n.cdk-text-field-autofill-monitored:-webkit-autofill {\n  -webkit-animation-name: cdk-text-field-autofill-start;\n          animation-name: cdk-text-field-autofill-start; }\n.cdk-text-field-autofill-monitored:not(:-webkit-autofill) {\n  -webkit-animation-name: cdk-text-field-autofill-end;\n          animation-name: cdk-text-field-autofill-end; }\ntextarea.cdk-textarea-autosize {\n  resize: none; }\ntextarea.cdk-textarea-autosize-measuring {\n  height: auto !important;\n  overflow: hidden !important;\n  padding: 2px 0 !important;\n  box-sizing: content-box !important; }\n.mat-elevation-z0 {\n  box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z1 {\n  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z2 {\n  box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z3 {\n  box-shadow: 0px 3px 3px -2px rgba(0, 0, 0, 0.2), 0px 3px 4px 0px rgba(0, 0, 0, 0.14), 0px 1px 8px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z4 {\n  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z5 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z6 {\n  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z7 {\n  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2), 0px 7px 10px 1px rgba(0, 0, 0, 0.14), 0px 2px 16px 1px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z8 {\n  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z9 {\n  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2), 0px 9px 12px 1px rgba(0, 0, 0, 0.14), 0px 3px 16px 2px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z10 {\n  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2), 0px 10px 14px 1px rgba(0, 0, 0, 0.14), 0px 4px 18px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z11 {\n  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2), 0px 11px 15px 1px rgba(0, 0, 0, 0.14), 0px 4px 20px 3px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z12 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z13 {\n  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 5px 24px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z14 {\n  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2), 0px 14px 21px 2px rgba(0, 0, 0, 0.14), 0px 5px 26px 4px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z15 {\n  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2), 0px 15px 22px 2px rgba(0, 0, 0, 0.14), 0px 6px 28px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z16 {\n  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z17 {\n  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2), 0px 17px 26px 2px rgba(0, 0, 0, 0.14), 0px 6px 32px 5px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z18 {\n  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2), 0px 18px 28px 2px rgba(0, 0, 0, 0.14), 0px 7px 34px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z19 {\n  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2), 0px 19px 29px 2px rgba(0, 0, 0, 0.14), 0px 7px 36px 6px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z20 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 20px 31px 3px rgba(0, 0, 0, 0.14), 0px 8px 38px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z21 {\n  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2), 0px 21px 33px 3px rgba(0, 0, 0, 0.14), 0px 8px 40px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z22 {\n  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2), 0px 22px 35px 3px rgba(0, 0, 0, 0.14), 0px 8px 42px 7px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z23 {\n  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2), 0px 23px 36px 3px rgba(0, 0, 0, 0.14), 0px 9px 44px 8px rgba(0, 0, 0, 0.12); }\n.mat-elevation-z24 {\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14), 0px 9px 46px 8px rgba(0, 0, 0, 0.12); }\n.mat-badge-content {\n  font-weight: 600;\n  font-size: 12px;\n  font-family: Poppins, sans-serif; }\n.mat-badge-small .mat-badge-content {\n  font-size: 6px; }\n.mat-badge-large .mat-badge-content {\n  font-size: 24px; }\n.mat-h1, .mat-headline, .mat-typography h1 {\n  font: 400 24px/32px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h2, .mat-title, .mat-typography h2 {\n  font: 500 20px/32px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h3, .mat-subheading-2, .mat-typography h3 {\n  font: 400 16px/28px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h4, .mat-subheading-1, .mat-typography h4 {\n  font: 400 15px/24px Poppins, sans-serif;\n  margin: 0 0 16px; }\n.mat-h5, .mat-typography h5 {\n  font: 400 11.62px/20px Poppins, sans-serif;\n  margin: 0 0 12px; }\n.mat-h6, .mat-typography h6 {\n  font: 400 9.38px/20px Poppins, sans-serif;\n  margin: 0 0 12px; }\n.mat-body-strong, .mat-body-2 {\n  font: 500 14px/24px Poppins, sans-serif; }\n.mat-body, .mat-body-1, .mat-typography {\n  font: 400 14px/20px Poppins, sans-serif; }\n.mat-body p, .mat-body-1 p, .mat-typography p {\n    margin: 0 0 12px; }\n.mat-small, .mat-caption {\n  font: 400 12px/20px Poppins, sans-serif; }\n.mat-display-4, .mat-typography .mat-display-4 {\n  font: 300 112px/112px Poppins, sans-serif;\n  margin: 0 0 56px;\n  letter-spacing: -0.05em; }\n.mat-display-3, .mat-typography .mat-display-3 {\n  font: 400 56px/56px Poppins, sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.02em; }\n.mat-display-2, .mat-typography .mat-display-2 {\n  font: 400 45px/48px Poppins, sans-serif;\n  margin: 0 0 64px;\n  letter-spacing: -0.005em; }\n.mat-display-1, .mat-typography .mat-display-1 {\n  font: 400 34px/40px Poppins, sans-serif;\n  margin: 0 0 64px; }\n.mat-bottom-sheet-container {\n  font-family: Poppins, sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-button, .mat-raised-button, .mat-icon-button, .mat-stroked-button,\n.mat-flat-button, .mat-fab, .mat-mini-fab {\n  font-family: Poppins, sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-button-toggle {\n  font-family: Poppins, sans-serif; }\n.mat-card {\n  font-family: Poppins, sans-serif; }\n.mat-card-title {\n  font-size: 24px;\n  font-weight: 400; }\n.mat-card-subtitle,\n.mat-card-content,\n.mat-card-header .mat-card-title {\n  font-size: 14px; }\n.mat-checkbox {\n  font-family: Poppins, sans-serif; }\n.mat-checkbox-layout .mat-checkbox-label {\n  line-height: 24px; }\n.mat-chip {\n  font-size: 13px;\n  line-height: 18px; }\n.mat-chip .mat-chip-trailing-icon.mat-icon,\n  .mat-chip .mat-chip-remove.mat-icon {\n    font-size: 18px; }\n.mat-table {\n  font-family: Poppins, sans-serif; }\n.mat-header-cell {\n  font-size: 12px;\n  font-weight: 500; }\n.mat-cell, .mat-footer-cell {\n  font-size: 14px; }\n.mat-calendar {\n  font-family: Poppins, sans-serif; }\n.mat-calendar-body {\n  font-size: 13px; }\n.mat-calendar-body-label,\n.mat-calendar-period-button {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-calendar-table-header th {\n  font-size: 11px;\n  font-weight: 400; }\n.mat-dialog-title {\n  font: 500 20px/32px Poppins, sans-serif; }\n.mat-expansion-panel-header {\n  font-family: Poppins, sans-serif;\n  font-size: 15px;\n  font-weight: 400; }\n.mat-expansion-panel-content {\n  font: 400 14px/20px Poppins, sans-serif; }\n.mat-form-field {\n  font-size: inherit;\n  font-weight: 400;\n  line-height: 1.125;\n  font-family: Poppins, sans-serif; }\n.mat-form-field-wrapper {\n  padding-bottom: 1.34375em; }\n.mat-form-field-prefix .mat-icon,\n.mat-form-field-suffix .mat-icon {\n  font-size: 150%;\n  line-height: 1.125; }\n.mat-form-field-prefix .mat-icon-button,\n.mat-form-field-suffix .mat-icon-button {\n  height: 1.5em;\n  width: 1.5em; }\n.mat-form-field-prefix .mat-icon-button .mat-icon,\n  .mat-form-field-suffix .mat-icon-button .mat-icon {\n    height: 1.125em;\n    line-height: 1.125; }\n.mat-form-field-infix {\n  padding: 0.5em 0;\n  border-top: 0.84375em solid transparent; }\n.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.34373em) scale(0.75);\n          transform: translateY(-1.34373em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.34372em) scale(0.75);\n          transform: translateY(-1.34372em) scale(0.75);\n  width: 133.33336333%; }\n.mat-form-field-label-wrapper {\n  top: -0.84375em;\n  padding-top: 0.84375em; }\n.mat-form-field-label {\n  top: 1.34375em; }\n.mat-form-field-underline {\n  bottom: 1.34375em; }\n.mat-form-field-subscript-wrapper {\n  font-size: 75%;\n  margin-top: 0.66666667em;\n  top: calc(100% - 1.79166667em); }\n.mat-form-field-appearance-legacy .mat-form-field-wrapper {\n  padding-bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-infix {\n  padding: 0.4375em 0; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00103px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00103px);\n  -ms-transform: translateY(-1.28122em) scale(0.75);\n  width: 133.33336333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00104px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00104px);\n  -ms-transform: translateY(-1.28121em) scale(0.75);\n  width: 133.33337333%; }\n.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00105px);\n          transform: translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.00105px);\n  -ms-transform: translateY(-1.2812em) scale(0.75);\n  width: 133.33338333%; }\n.mat-form-field-appearance-legacy .mat-form-field-label {\n  top: 1.28125em; }\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n  bottom: 1.25em; }\n.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper {\n  margin-top: 0.54166667em;\n  top: calc(100% - 1.66666667em); }\n.mat-form-field-appearance-fill .mat-form-field-infix {\n  padding: 0.25em 0 0.75em 0; }\n.mat-form-field-appearance-fill .mat-form-field-label {\n  top: 1.09375em;\n  margin-top: -0.5em; }\n.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-0.59373em) scale(0.75);\n          transform: translateY(-0.59373em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-0.59372em) scale(0.75);\n          transform: translateY(-0.59372em) scale(0.75);\n  width: 133.33336333%; }\n.mat-form-field-appearance-outline .mat-form-field-infix {\n  padding: 1em 0 1em 0; }\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  bottom: 1.34375em; }\n.mat-form-field-appearance-outline .mat-form-field-label {\n  top: 1.84375em;\n  margin-top: -0.25em; }\n.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label,\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus + .mat-form-field-label-wrapper .mat-form-field-label {\n  -webkit-transform: translateY(-1.59373em) scale(0.75);\n          transform: translateY(-1.59373em) scale(0.75);\n  width: 133.33335333%; }\n.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown) + .mat-form-field-label-wrapper\n.mat-form-field-label {\n  -webkit-transform: translateY(-1.59372em) scale(0.75);\n          transform: translateY(-1.59372em) scale(0.75);\n  width: 133.33336333%; }\n.mat-grid-tile-header,\n.mat-grid-tile-footer {\n  font-size: 14px; }\n.mat-grid-tile-header .mat-line,\n  .mat-grid-tile-footer .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-grid-tile-header .mat-line:nth-child(n+2),\n    .mat-grid-tile-footer .mat-line:nth-child(n+2) {\n      font-size: 12px; }\ninput.mat-input-element {\n  margin-top: -0.0625em; }\n.mat-menu-item {\n  font-family: Poppins, sans-serif;\n  font-size: 16px;\n  font-weight: 400; }\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  font-family: Poppins, sans-serif;\n  font-size: 12px; }\n.mat-radio-button {\n  font-family: Poppins, sans-serif; }\n.mat-select {\n  font-family: Poppins, sans-serif; }\n.mat-select-trigger {\n  height: 1.125em; }\n.mat-slide-toggle-content {\n  font: 400 14px/20px Poppins, sans-serif; }\n.mat-slider-thumb-label-text {\n  font-family: Poppins, sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-stepper-vertical, .mat-stepper-horizontal {\n  font-family: Poppins, sans-serif; }\n.mat-step-label {\n  font-size: 14px;\n  font-weight: 400; }\n.mat-step-label-selected {\n  font-size: 14px;\n  font-weight: 500; }\n.mat-tab-group {\n  font-family: Poppins, sans-serif; }\n.mat-tab-label, .mat-tab-link {\n  font-family: Poppins, sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-toolbar,\n.mat-toolbar h1,\n.mat-toolbar h2,\n.mat-toolbar h3,\n.mat-toolbar h4,\n.mat-toolbar h5,\n.mat-toolbar h6 {\n  font: 500 20px/32px Poppins, sans-serif;\n  margin: 0; }\n.mat-tooltip {\n  font-family: Poppins, sans-serif;\n  font-size: 10px;\n  padding-top: 6px;\n  padding-bottom: 6px; }\n.mat-tooltip-handset {\n  font-size: 14px;\n  padding-top: 9px;\n  padding-bottom: 9px; }\n.mat-list-item {\n  font-family: Poppins, sans-serif; }\n.mat-list-option {\n  font-family: Poppins, sans-serif; }\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  font-size: 16px; }\n.mat-list .mat-list-item .mat-line, .mat-nav-list .mat-list-item .mat-line, .mat-selection-list .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  font-size: 16px; }\n.mat-list .mat-list-option .mat-line, .mat-nav-list .mat-list-option .mat-line, .mat-selection-list .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 14px; }\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  font-family: Poppins, sans-serif;\n  font-size: 14px;\n  font-weight: 500; }\n.mat-list[dense] .mat-list-item, .mat-nav-list[dense] .mat-list-item, .mat-selection-list[dense] .mat-list-item {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-item .mat-line, .mat-nav-list[dense] .mat-list-item .mat-line, .mat-selection-list[dense] .mat-list-item .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-item .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-item .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-list-option, .mat-nav-list[dense] .mat-list-option, .mat-selection-list[dense] .mat-list-option {\n  font-size: 12px; }\n.mat-list[dense] .mat-list-option .mat-line, .mat-nav-list[dense] .mat-list-option .mat-line, .mat-selection-list[dense] .mat-list-option .mat-line {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    display: block;\n    box-sizing: border-box; }\n.mat-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-nav-list[dense] .mat-list-option .mat-line:nth-child(n+2), .mat-selection-list[dense] .mat-list-option .mat-line:nth-child(n+2) {\n      font-size: 12px; }\n.mat-list[dense] .mat-subheader, .mat-nav-list[dense] .mat-subheader, .mat-selection-list[dense] .mat-subheader {\n  font-family: Poppins, sans-serif;\n  font-size: 12px;\n  font-weight: 500; }\n.mat-option {\n  font-family: Poppins, sans-serif;\n  font-size: 16px; }\n.mat-optgroup-label {\n  font: 500 14px/24px Poppins, sans-serif; }\n.mat-simple-snackbar {\n  font-family: Poppins, sans-serif;\n  font-size: 14px; }\n.mat-simple-snackbar-action {\n  line-height: 1;\n  font-family: inherit;\n  font-size: inherit;\n  font-weight: 500; }\n.mat-tree {\n  font-family: Poppins, sans-serif; }\n.mat-tree-node {\n  font-weight: 400;\n  font-size: 14px; }\n.mat-ripple {\n  overflow: hidden; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-ripple {\n      display: none; } }\n.mat-ripple.mat-ripple-unbounded {\n  overflow: visible; }\n.mat-ripple-element {\n  position: absolute;\n  border-radius: 50%;\n  pointer-events: none;\n  transition: opacity, -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  transition: opacity, transform 0ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 0ms cubic-bezier(0, 0, 0.2, 1);\n  -webkit-transform: scale(0);\n          transform: scale(0); }\n.cdk-visually-hidden {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n  outline: 0;\n  -webkit-appearance: none;\n  -moz-appearance: none; }\n.cdk-overlay-container, .cdk-global-overlay-wrapper {\n  pointer-events: none;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n.cdk-overlay-container {\n  position: fixed;\n  z-index: 1000; }\n.cdk-overlay-container:empty {\n    display: none; }\n.cdk-global-overlay-wrapper {\n  display: flex;\n  position: absolute;\n  z-index: 1000; }\n.cdk-overlay-pane {\n  position: absolute;\n  pointer-events: auto;\n  box-sizing: border-box;\n  z-index: 1000;\n  display: flex;\n  max-width: 100%;\n  max-height: 100%; }\n.cdk-overlay-backdrop {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 1000;\n  pointer-events: auto;\n  -webkit-tap-highlight-color: transparent;\n  transition: opacity 400ms cubic-bezier(0.25, 0.8, 0.25, 1);\n  opacity: 0; }\n.cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n    opacity: 1; }\n@media screen and (-ms-high-contrast: active) {\n      .cdk-overlay-backdrop.cdk-overlay-backdrop-showing {\n        opacity: 0.6; } }\n.cdk-overlay-dark-backdrop {\n  background: rgba(0, 0, 0, 0.288); }\n.cdk-overlay-transparent-backdrop, .cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing {\n  opacity: 0; }\n.cdk-overlay-connected-position-bounding-box {\n  position: absolute;\n  z-index: 1000;\n  display: flex;\n  flex-direction: column;\n  min-width: 1px;\n  min-height: 1px; }\n.cdk-global-scrollblock {\n  position: fixed;\n  width: 100%;\n  overflow-y: scroll; }\n@keyframes cdk-text-field-autofill-start {}\n@keyframes cdk-text-field-autofill-end {}\n.cdk-text-field-autofill-monitored:-webkit-autofill {\n  -webkit-animation-name: cdk-text-field-autofill-start;\n          animation-name: cdk-text-field-autofill-start; }\n.cdk-text-field-autofill-monitored:not(:-webkit-autofill) {\n  -webkit-animation-name: cdk-text-field-autofill-end;\n          animation-name: cdk-text-field-autofill-end; }\ntextarea.cdk-textarea-autosize {\n  resize: none; }\ntextarea.cdk-textarea-autosize-measuring {\n  height: auto !important;\n  overflow: hidden !important;\n  padding: 2px 0 !important;\n  box-sizing: content-box !important; }\n.mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.1); }\n.mat-option {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-option:hover:not(.mat-option-disabled), .mat-option:focus:not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n.mat-option.mat-selected:not(.mat-option-multiple):not(.mat-option-disabled) {\n    background: rgba(0, 0, 0, 0.04); }\n.mat-option.mat-active {\n    background: rgba(0, 0, 0, 0.04);\n    color: rgba(0, 0, 0, 0.87); }\n.mat-option.mat-option-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n.mat-primary .mat-option.mat-selected:not(.mat-option-disabled) {\n  color: #1e88e5; }\n.mat-accent .mat-option.mat-selected:not(.mat-option-disabled) {\n  color: #3f51b5; }\n.mat-warn .mat-option.mat-selected:not(.mat-option-disabled) {\n  color: #e91e63; }\n.mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-optgroup-disabled .mat-optgroup-label {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-pseudo-checkbox {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-pseudo-checkbox::after {\n    color: #fafafa; }\n.mat-pseudo-checkbox-checked,\n.mat-pseudo-checkbox-indeterminate,\n.mat-accent .mat-pseudo-checkbox-checked,\n.mat-accent .mat-pseudo-checkbox-indeterminate {\n  background: #3f51b5; }\n.mat-primary .mat-pseudo-checkbox-checked,\n.mat-primary .mat-pseudo-checkbox-indeterminate {\n  background: #1e88e5; }\n.mat-warn .mat-pseudo-checkbox-checked,\n.mat-warn .mat-pseudo-checkbox-indeterminate {\n  background: #e91e63; }\n.mat-pseudo-checkbox-checked.mat-pseudo-checkbox-disabled,\n.mat-pseudo-checkbox-indeterminate.mat-pseudo-checkbox-disabled {\n  background: #b0b0b0; }\n.mat-app-background {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-theme-loaded-marker {\n  display: none; }\n.mat-autocomplete-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover) {\n    background: white; }\n.mat-autocomplete-panel .mat-option.mat-selected:not(.mat-active):not(:hover):not(.mat-option-disabled) {\n      color: rgba(0, 0, 0, 0.87); }\n.mat-badge-content {\n  color: white;\n  background: #1e88e5; }\n.mat-badge-accent .mat-badge-content {\n  background: #3f51b5;\n  color: white; }\n.mat-badge-warn .mat-badge-content {\n  color: white;\n  background: #e91e63; }\n.mat-badge {\n  position: relative; }\n.mat-badge-hidden .mat-badge-content {\n  display: none; }\n.mat-badge-content {\n  position: absolute;\n  text-align: center;\n  display: inline-block;\n  border-radius: 50%;\n  transition: -webkit-transform 200ms ease-in-out;\n  transition: transform 200ms ease-in-out;\n  transition: transform 200ms ease-in-out, -webkit-transform 200ms ease-in-out;\n  -webkit-transform: scale(0.6);\n          transform: scale(0.6);\n  overflow: hidden;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  pointer-events: none; }\n.mat-badge-content.mat-badge-active {\n  -webkit-transform: none;\n          transform: none; }\n.mat-badge-small .mat-badge-content {\n  width: 16px;\n  height: 16px;\n  line-height: 16px; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-badge-small .mat-badge-content {\n      outline: solid 1px;\n      border-radius: 0; } }\n.mat-badge-small.mat-badge-above .mat-badge-content {\n  top: -8px; }\n.mat-badge-small.mat-badge-below .mat-badge-content {\n  bottom: -8px; }\n.mat-badge-small.mat-badge-before {\n  margin-left: 16px; }\n.mat-badge-small.mat-badge-before .mat-badge-content {\n    left: -16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-before {\n  margin-left: 0;\n  margin-right: 16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -16px; }\n.mat-badge-small.mat-badge-after {\n  margin-right: 16px; }\n.mat-badge-small.mat-badge-after .mat-badge-content {\n    right: -16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-after {\n  margin-right: 0;\n  margin-left: 16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -16px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-before {\n  margin-left: 8px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: -8px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-before {\n  margin-left: 0;\n  margin-right: 8px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -8px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-after {\n  margin-right: 8px; }\n.mat-badge-small.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: -8px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-after {\n  margin-right: 0;\n  margin-left: 16px; }\n[dir='rtl'] .mat-badge-small.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -8px; }\n.mat-badge-medium .mat-badge-content {\n  width: 22px;\n  height: 22px;\n  line-height: 22px; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-badge-medium .mat-badge-content {\n      outline: solid 1px;\n      border-radius: 0; } }\n.mat-badge-medium.mat-badge-above .mat-badge-content {\n  top: -11px; }\n.mat-badge-medium.mat-badge-below .mat-badge-content {\n  bottom: -11px; }\n.mat-badge-medium.mat-badge-before {\n  margin-left: 22px; }\n.mat-badge-medium.mat-badge-before .mat-badge-content {\n    left: -22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-before {\n  margin-left: 0;\n  margin-right: 22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -22px; }\n.mat-badge-medium.mat-badge-after {\n  margin-right: 22px; }\n.mat-badge-medium.mat-badge-after .mat-badge-content {\n    right: -22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-after {\n  margin-right: 0;\n  margin-left: 22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -22px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-before {\n  margin-left: 11px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: -11px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-before {\n  margin-left: 0;\n  margin-right: 11px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -11px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-after {\n  margin-right: 11px; }\n.mat-badge-medium.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: -11px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-after {\n  margin-right: 0;\n  margin-left: 22px; }\n[dir='rtl'] .mat-badge-medium.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -11px; }\n.mat-badge-large .mat-badge-content {\n  width: 28px;\n  height: 28px;\n  line-height: 28px; }\n@media screen and (-ms-high-contrast: active) {\n    .mat-badge-large .mat-badge-content {\n      outline: solid 1px;\n      border-radius: 0; } }\n.mat-badge-large.mat-badge-above .mat-badge-content {\n  top: -14px; }\n.mat-badge-large.mat-badge-below .mat-badge-content {\n  bottom: -14px; }\n.mat-badge-large.mat-badge-before {\n  margin-left: 28px; }\n.mat-badge-large.mat-badge-before .mat-badge-content {\n    left: -28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-before {\n  margin-left: 0;\n  margin-right: 28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -28px; }\n.mat-badge-large.mat-badge-after {\n  margin-right: 28px; }\n.mat-badge-large.mat-badge-after .mat-badge-content {\n    right: -28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-after {\n  margin-right: 0;\n  margin-left: 28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -28px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-before {\n  margin-left: 14px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: -14px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-before {\n  margin-left: 0;\n  margin-right: 14px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-before .mat-badge-content {\n    left: auto;\n    right: -14px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-after {\n  margin-right: 14px; }\n.mat-badge-large.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: -14px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-after {\n  margin-right: 0;\n  margin-left: 28px; }\n[dir='rtl'] .mat-badge-large.mat-badge-overlap.mat-badge-after .mat-badge-content {\n    right: auto;\n    left: -14px; }\n.mat-bottom-sheet-container {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-button, .mat-icon-button, .mat-stroked-button {\n  color: inherit;\n  background: transparent; }\n.mat-button.mat-primary, .mat-icon-button.mat-primary, .mat-stroked-button.mat-primary {\n    color: #1e88e5; }\n.mat-button.mat-accent, .mat-icon-button.mat-accent, .mat-stroked-button.mat-accent {\n    color: #3f51b5; }\n.mat-button.mat-warn, .mat-icon-button.mat-warn, .mat-stroked-button.mat-warn {\n    color: #e91e63; }\n.mat-button.mat-primary[disabled], .mat-button.mat-accent[disabled], .mat-button.mat-warn[disabled], .mat-button[disabled][disabled], .mat-icon-button.mat-primary[disabled], .mat-icon-button.mat-accent[disabled], .mat-icon-button.mat-warn[disabled], .mat-icon-button[disabled][disabled], .mat-stroked-button.mat-primary[disabled], .mat-stroked-button.mat-accent[disabled], .mat-stroked-button.mat-warn[disabled], .mat-stroked-button[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.26); }\n.mat-button.mat-primary .mat-button-focus-overlay, .mat-icon-button.mat-primary .mat-button-focus-overlay, .mat-stroked-button.mat-primary .mat-button-focus-overlay {\n    background-color: rgba(30, 136, 229, 0.12); }\n.mat-button.mat-accent .mat-button-focus-overlay, .mat-icon-button.mat-accent .mat-button-focus-overlay, .mat-stroked-button.mat-accent .mat-button-focus-overlay {\n    background-color: rgba(63, 81, 181, 0.12); }\n.mat-button.mat-warn .mat-button-focus-overlay, .mat-icon-button.mat-warn .mat-button-focus-overlay, .mat-stroked-button.mat-warn .mat-button-focus-overlay {\n    background-color: rgba(233, 30, 99, 0.12); }\n.mat-button[disabled] .mat-button-focus-overlay, .mat-icon-button[disabled] .mat-button-focus-overlay, .mat-stroked-button[disabled] .mat-button-focus-overlay {\n    background-color: transparent; }\n.mat-button.mat-primary .mat-ripple-element, .mat-icon-button.mat-primary .mat-ripple-element, .mat-stroked-button.mat-primary .mat-ripple-element {\n    background-color: rgba(30, 136, 229, 0.1); }\n.mat-button.mat-accent .mat-ripple-element, .mat-icon-button.mat-accent .mat-ripple-element, .mat-stroked-button.mat-accent .mat-ripple-element {\n    background-color: rgba(63, 81, 181, 0.1); }\n.mat-button.mat-warn .mat-ripple-element, .mat-icon-button.mat-warn .mat-ripple-element, .mat-stroked-button.mat-warn .mat-ripple-element {\n    background-color: rgba(233, 30, 99, 0.1); }\n.mat-flat-button, .mat-raised-button, .mat-fab, .mat-mini-fab {\n  color: rgba(0, 0, 0, 0.87);\n  background-color: white; }\n.mat-flat-button.mat-primary, .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    color: white; }\n.mat-flat-button.mat-accent, .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    color: white; }\n.mat-flat-button.mat-warn, .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    color: white; }\n.mat-flat-button.mat-primary[disabled], .mat-flat-button.mat-accent[disabled], .mat-flat-button.mat-warn[disabled], .mat-flat-button[disabled][disabled], .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    color: rgba(0, 0, 0, 0.26); }\n.mat-flat-button.mat-primary, .mat-raised-button.mat-primary, .mat-fab.mat-primary, .mat-mini-fab.mat-primary {\n    background-color: #1e88e5; }\n.mat-flat-button.mat-accent, .mat-raised-button.mat-accent, .mat-fab.mat-accent, .mat-mini-fab.mat-accent {\n    background-color: #3f51b5; }\n.mat-flat-button.mat-warn, .mat-raised-button.mat-warn, .mat-fab.mat-warn, .mat-mini-fab.mat-warn {\n    background-color: #e91e63; }\n.mat-flat-button.mat-primary[disabled], .mat-flat-button.mat-accent[disabled], .mat-flat-button.mat-warn[disabled], .mat-flat-button[disabled][disabled], .mat-raised-button.mat-primary[disabled], .mat-raised-button.mat-accent[disabled], .mat-raised-button.mat-warn[disabled], .mat-raised-button[disabled][disabled], .mat-fab.mat-primary[disabled], .mat-fab.mat-accent[disabled], .mat-fab.mat-warn[disabled], .mat-fab[disabled][disabled], .mat-mini-fab.mat-primary[disabled], .mat-mini-fab.mat-accent[disabled], .mat-mini-fab.mat-warn[disabled], .mat-mini-fab[disabled][disabled] {\n    background-color: rgba(0, 0, 0, 0.12); }\n.mat-flat-button.mat-primary .mat-ripple-element, .mat-raised-button.mat-primary .mat-ripple-element, .mat-fab.mat-primary .mat-ripple-element, .mat-mini-fab.mat-primary .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.1); }\n.mat-flat-button.mat-accent .mat-ripple-element, .mat-raised-button.mat-accent .mat-ripple-element, .mat-fab.mat-accent .mat-ripple-element, .mat-mini-fab.mat-accent .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.1); }\n.mat-flat-button.mat-warn .mat-ripple-element, .mat-raised-button.mat-warn .mat-ripple-element, .mat-fab.mat-warn .mat-ripple-element, .mat-mini-fab.mat-warn .mat-ripple-element {\n    background-color: rgba(255, 255, 255, 0.1); }\n.mat-icon-button.mat-primary .mat-ripple-element {\n  background-color: rgba(30, 136, 229, 0.2); }\n.mat-icon-button.mat-accent .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.2); }\n.mat-icon-button.mat-warn .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.2); }\n.mat-button-toggle {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-button-toggle .mat-button-toggle-focus-overlay {\n    background-color: rgba(0, 0, 0, 0.12); }\n.mat-button-toggle-checked {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.54); }\n.mat-button-toggle-disabled {\n  background-color: #eeeeee;\n  color: rgba(0, 0, 0, 0.26); }\n.mat-button-toggle-disabled.mat-button-toggle-checked {\n    background-color: #bdbdbd; }\n.mat-card {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-card-subtitle {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-checkbox-frame {\n  border-color: rgba(0, 0, 0, 0.54); }\n.mat-checkbox-checkmark {\n  fill: #fafafa; }\n.mat-checkbox-checkmark-path {\n  stroke: #fafafa !important; }\n.mat-checkbox-mixedmark {\n  background-color: #fafafa; }\n.mat-checkbox-indeterminate.mat-primary .mat-checkbox-background, .mat-checkbox-checked.mat-primary .mat-checkbox-background {\n  background-color: #1e88e5; }\n.mat-checkbox-indeterminate.mat-accent .mat-checkbox-background, .mat-checkbox-checked.mat-accent .mat-checkbox-background {\n  background-color: #3f51b5; }\n.mat-checkbox-indeterminate.mat-warn .mat-checkbox-background, .mat-checkbox-checked.mat-warn .mat-checkbox-background {\n  background-color: #e91e63; }\n.mat-checkbox-disabled.mat-checkbox-checked .mat-checkbox-background, .mat-checkbox-disabled.mat-checkbox-indeterminate .mat-checkbox-background {\n  background-color: #b0b0b0; }\n.mat-checkbox-disabled:not(.mat-checkbox-checked) .mat-checkbox-frame {\n  border-color: #b0b0b0; }\n.mat-checkbox-disabled .mat-checkbox-label {\n  color: #b0b0b0; }\n.mat-checkbox:not(.mat-checkbox-disabled).mat-primary .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(30, 136, 229, 0.26); }\n.mat-checkbox:not(.mat-checkbox-disabled).mat-accent .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.26); }\n.mat-checkbox:not(.mat-checkbox-disabled).mat-warn .mat-checkbox-ripple .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.26); }\n.mat-chip.mat-standard-chip {\n  background-color: #e0e0e0;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-chip.mat-standard-chip .mat-chip-remove {\n    color: rgba(0, 0, 0, 0.87);\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-primary {\n  background-color: #1e88e5;\n  color: white; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-primary .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-primary .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-warn {\n  background-color: #e91e63;\n  color: white; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-warn .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-warn .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-accent {\n  background-color: #3f51b5;\n  color: white; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-accent .mat-chip-remove {\n    color: white;\n    opacity: 0.4; }\n.mat-chip.mat-standard-chip.mat-chip-selected.mat-accent .mat-chip-remove:hover {\n    opacity: 0.54; }\n.mat-table {\n  background: white; }\nmat-row, mat-header-row, mat-footer-row,\nth.mat-header-cell, td.mat-cell, td.mat-footer-cell {\n  border-bottom-color: rgba(0, 0, 0, 0.12); }\n.mat-header-cell {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-cell, .mat-footer-cell {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-calendar-arrow {\n  border-top-color: rgba(0, 0, 0, 0.54); }\n.mat-datepicker-toggle,\n.mat-datepicker-popup .mat-calendar-next-button,\n.mat-datepicker-popup .mat-calendar-previous-button {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-calendar-table-header {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-calendar-table-header-divider::after {\n  background: rgba(0, 0, 0, 0.12); }\n.mat-calendar-body-label {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-calendar-body-cell-content {\n  color: rgba(0, 0, 0, 0.87);\n  border-color: transparent; }\n.mat-calendar-body-disabled > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-calendar-body-cell:not(.mat-calendar-body-disabled):hover > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-keyboard-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected),\n.cdk-program-focused .mat-calendar-body-active > .mat-calendar-body-cell-content:not(.mat-calendar-body-selected) {\n  background-color: rgba(0, 0, 0, 0.04); }\n.mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-calendar-body-disabled > .mat-calendar-body-today:not(.mat-calendar-body-selected) {\n  border-color: rgba(0, 0, 0, 0.18); }\n.mat-calendar-body-selected {\n  background-color: #1e88e5;\n  color: white; }\n.mat-calendar-body-disabled > .mat-calendar-body-selected {\n  background-color: rgba(30, 136, 229, 0.4); }\n.mat-calendar-body-today.mat-calendar-body-selected {\n  box-shadow: inset 0 0 0 1px white; }\n.mat-datepicker-content {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-datepicker-content.mat-accent .mat-calendar-body-selected {\n    background-color: #3f51b5;\n    color: white; }\n.mat-datepicker-content.mat-accent .mat-calendar-body-disabled > .mat-calendar-body-selected {\n    background-color: rgba(63, 81, 181, 0.4); }\n.mat-datepicker-content.mat-accent .mat-calendar-body-today.mat-calendar-body-selected {\n    box-shadow: inset 0 0 0 1px white; }\n.mat-datepicker-content.mat-warn .mat-calendar-body-selected {\n    background-color: #e91e63;\n    color: white; }\n.mat-datepicker-content.mat-warn .mat-calendar-body-disabled > .mat-calendar-body-selected {\n    background-color: rgba(233, 30, 99, 0.4); }\n.mat-datepicker-content.mat-warn .mat-calendar-body-today.mat-calendar-body-selected {\n    box-shadow: inset 0 0 0 1px white; }\n.mat-datepicker-toggle-active {\n  color: #1e88e5; }\n.mat-datepicker-toggle-active.mat-accent {\n    color: #3f51b5; }\n.mat-datepicker-toggle-active.mat-warn {\n    color: #e91e63; }\n.mat-dialog-container {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-divider {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n.mat-divider-vertical {\n  border-right-color: rgba(0, 0, 0, 0.12); }\n.mat-expansion-panel {\n  background: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-action-row {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n.mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-keyboard-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']).cdk-program-focused, .mat-expansion-panel:not(.mat-expanded) .mat-expansion-panel-header:not([aria-disabled='true']):hover {\n  background: rgba(0, 0, 0, 0.04); }\n.mat-expansion-panel-header-title {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-expansion-panel-header-description,\n.mat-expansion-indicator::after {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-expansion-panel-header[aria-disabled='true'] {\n  color: rgba(0, 0, 0, 0.26); }\n.mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-title,\n  .mat-expansion-panel-header[aria-disabled='true'] .mat-expansion-panel-header-description {\n    color: inherit; }\n.mat-form-field-label {\n  color: rgba(0, 0, 0, 0.6); }\n.mat-hint {\n  color: rgba(0, 0, 0, 0.6); }\n.mat-form-field.mat-focused .mat-form-field-label {\n  color: #1e88e5; }\n.mat-form-field.mat-focused .mat-form-field-label.mat-accent {\n    color: #3f51b5; }\n.mat-form-field.mat-focused .mat-form-field-label.mat-warn {\n    color: #e91e63; }\n.mat-focused .mat-form-field-required-marker {\n  color: #3f51b5; }\n.mat-form-field-ripple {\n  background-color: rgba(0, 0, 0, 0.87); }\n.mat-form-field.mat-focused .mat-form-field-ripple {\n  background-color: #1e88e5; }\n.mat-form-field.mat-focused .mat-form-field-ripple.mat-accent {\n    background-color: #3f51b5; }\n.mat-form-field.mat-focused .mat-form-field-ripple.mat-warn {\n    background-color: #e91e63; }\n.mat-form-field.mat-form-field-invalid .mat-form-field-label {\n  color: #e91e63; }\n.mat-form-field.mat-form-field-invalid .mat-form-field-label.mat-accent,\n  .mat-form-field.mat-form-field-invalid .mat-form-field-label .mat-form-field-required-marker {\n    color: #e91e63; }\n.mat-form-field.mat-form-field-invalid .mat-form-field-ripple {\n  background-color: #e91e63; }\n.mat-error {\n  color: #e91e63; }\n.mat-form-field-appearance-legacy .mat-form-field-label {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-form-field-appearance-legacy .mat-hint {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-form-field-appearance-legacy .mat-form-field-underline {\n  background-color: rgba(0, 0, 0, 0.42); }\n.mat-form-field-appearance-legacy.mat-form-field-disabled .mat-form-field-underline {\n  background-image: linear-gradient(to right, rgba(0, 0, 0, 0.42) 0%, rgba(0, 0, 0, 0.42) 33%, transparent 0%);\n  background-size: 4px 100%;\n  background-repeat: repeat-x; }\n.mat-form-field-appearance-standard .mat-form-field-underline {\n  background-color: rgba(0, 0, 0, 0.42); }\n.mat-form-field-appearance-standard.mat-form-field-disabled .mat-form-field-underline {\n  background-image: linear-gradient(to right, rgba(0, 0, 0, 0.42) 0%, rgba(0, 0, 0, 0.42) 33%, transparent 0%);\n  background-size: 4px 100%;\n  background-repeat: repeat-x; }\n.mat-form-field-appearance-fill .mat-form-field-flex {\n  background-color: rgba(0, 0, 0, 0.04); }\n.mat-form-field-appearance-fill.mat-form-field-disabled .mat-form-field-flex {\n  background-color: rgba(0, 0, 0, 0.02); }\n.mat-form-field-appearance-fill .mat-form-field-underline::before {\n  background-color: rgba(0, 0, 0, 0.42); }\n.mat-form-field-appearance-fill.mat-form-field-disabled .mat-form-field-label {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-form-field-appearance-fill.mat-form-field-disabled .mat-form-field-underline::before {\n  background-color: transparent; }\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  color: rgba(0, 0, 0, 0.12); }\n.mat-form-field-appearance-outline .mat-form-field-outline-thick {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-form-field-appearance-outline.mat-focused .mat-form-field-outline-thick {\n  color: #1e88e5; }\n.mat-form-field-appearance-outline.mat-focused.mat-accent .mat-form-field-outline-thick {\n  color: #3f51b5; }\n.mat-form-field-appearance-outline.mat-focused.mat-warn .mat-form-field-outline-thick {\n  color: #e91e63; }\n.mat-form-field-appearance-outline.mat-form-field-invalid.mat-form-field-invalid .mat-form-field-outline-thick {\n  color: #e91e63; }\n.mat-form-field-appearance-outline.mat-form-field-disabled .mat-form-field-label {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-form-field-appearance-outline.mat-form-field-disabled .mat-form-field-outline {\n  color: rgba(0, 0, 0, 0.06); }\n.mat-icon.mat-primary {\n  color: #1e88e5; }\n.mat-icon.mat-accent {\n  color: #3f51b5; }\n.mat-icon.mat-warn {\n  color: #e91e63; }\n.mat-input-element:disabled {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-input-element {\n  caret-color: #1e88e5; }\n.mat-input-element::-webkit-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element:-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::-moz-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element::-webkit-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-input-element:-ms-input-placeholder {\n    color: rgba(0, 0, 0, 0.42); }\n.mat-accent .mat-input-element {\n  caret-color: #3f51b5; }\n.mat-warn .mat-input-element,\n.mat-form-field-invalid .mat-input-element {\n  caret-color: #e91e63; }\n.mat-list .mat-list-item, .mat-nav-list .mat-list-item, .mat-selection-list .mat-list-item {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-list .mat-list-option, .mat-nav-list .mat-list-option, .mat-selection-list .mat-list-option {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-list .mat-subheader, .mat-nav-list .mat-subheader, .mat-selection-list .mat-subheader {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-list-item-disabled {\n  background-color: #eeeeee; }\n.mat-list-option:hover, .mat-list-option.mat-list-item-focus,\n.mat-nav-list .mat-list-item:hover,\n.mat-nav-list .mat-list-item.mat-list-item-focus {\n  background: rgba(0, 0, 0, 0.04); }\n.mat-menu-panel {\n  background: white; }\n.mat-menu-item {\n  background: transparent;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-menu-item[disabled], .mat-menu-item[disabled]::after {\n    color: rgba(0, 0, 0, 0.38); }\n.mat-menu-item .mat-icon:not([color]),\n.mat-menu-item-submenu-trigger::after {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-menu-item:hover:not([disabled]),\n.mat-menu-item.cdk-program-focused:not([disabled]),\n.mat-menu-item.cdk-keyboard-focused:not([disabled]),\n.mat-menu-item-highlighted:not([disabled]) {\n  background: rgba(0, 0, 0, 0.04); }\n.mat-paginator {\n  background: white; }\n.mat-paginator,\n.mat-paginator-page-size .mat-select-trigger {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-paginator-decrement,\n.mat-paginator-increment {\n  border-top: 2px solid rgba(0, 0, 0, 0.54);\n  border-right: 2px solid rgba(0, 0, 0, 0.54); }\n.mat-paginator-first,\n.mat-paginator-last {\n  border-top: 2px solid rgba(0, 0, 0, 0.54); }\n.mat-icon-button[disabled] .mat-paginator-decrement,\n.mat-icon-button[disabled] .mat-paginator-increment,\n.mat-icon-button[disabled] .mat-paginator-first,\n.mat-icon-button[disabled] .mat-paginator-last {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-progress-bar-background {\n  fill: #bbdefb; }\n.mat-progress-bar-buffer {\n  background-color: #bbdefb; }\n.mat-progress-bar-fill::after {\n  background-color: #1e88e5; }\n.mat-progress-bar.mat-accent .mat-progress-bar-background {\n  fill: #c5cae9; }\n.mat-progress-bar.mat-accent .mat-progress-bar-buffer {\n  background-color: #c5cae9; }\n.mat-progress-bar.mat-accent .mat-progress-bar-fill::after {\n  background-color: #3f51b5; }\n.mat-progress-bar.mat-warn .mat-progress-bar-background {\n  fill: #f8bbd0; }\n.mat-progress-bar.mat-warn .mat-progress-bar-buffer {\n  background-color: #f8bbd0; }\n.mat-progress-bar.mat-warn .mat-progress-bar-fill::after {\n  background-color: #e91e63; }\n.mat-progress-spinner circle, .mat-spinner circle {\n  stroke: #1e88e5; }\n.mat-progress-spinner.mat-accent circle, .mat-spinner.mat-accent circle {\n  stroke: #3f51b5; }\n.mat-progress-spinner.mat-warn circle, .mat-spinner.mat-warn circle {\n  stroke: #e91e63; }\n.mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.54); }\n.mat-radio-disabled .mat-radio-outer-circle {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-radio-disabled .mat-radio-ripple .mat-ripple-element, .mat-radio-disabled .mat-radio-inner-circle {\n  background-color: rgba(0, 0, 0, 0.38); }\n.mat-radio-disabled .mat-radio-label-content {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-radio-button.mat-primary.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #1e88e5; }\n.mat-radio-button.mat-primary .mat-radio-inner-circle {\n  background-color: #1e88e5; }\n.mat-radio-button.mat-primary .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(30, 136, 229, 0.26); }\n.mat-radio-button.mat-accent.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #3f51b5; }\n.mat-radio-button.mat-accent .mat-radio-inner-circle {\n  background-color: #3f51b5; }\n.mat-radio-button.mat-accent .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.26); }\n.mat-radio-button.mat-warn.mat-radio-checked .mat-radio-outer-circle {\n  border-color: #e91e63; }\n.mat-radio-button.mat-warn .mat-radio-inner-circle {\n  background-color: #e91e63; }\n.mat-radio-button.mat-warn .mat-radio-ripple .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.26); }\n.mat-select-content, .mat-select-panel-done-animating {\n  background: white; }\n.mat-select-value {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-select-placeholder {\n  color: rgba(0, 0, 0, 0.42); }\n.mat-select-disabled .mat-select-value {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-select-arrow {\n  color: rgba(0, 0, 0, 0.54); }\n.mat-select-panel .mat-option.mat-selected:not(.mat-option-multiple) {\n  background: rgba(0, 0, 0, 0.12); }\n.mat-form-field.mat-focused.mat-primary .mat-select-arrow {\n  color: #1e88e5; }\n.mat-form-field.mat-focused.mat-accent .mat-select-arrow {\n  color: #3f51b5; }\n.mat-form-field.mat-focused.mat-warn .mat-select-arrow {\n  color: #e91e63; }\n.mat-form-field .mat-select.mat-select-invalid .mat-select-arrow {\n  color: #e91e63; }\n.mat-form-field .mat-select.mat-select-disabled .mat-select-arrow {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-drawer-container {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-drawer {\n  background-color: white;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-drawer.mat-drawer-push {\n    background-color: white; }\n.mat-drawer-backdrop.mat-drawer-shown {\n  background-color: rgba(0, 0, 0, 0.6); }\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #3f51b5; }\n.mat-slide-toggle.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(63, 81, 181, 0.5); }\n.mat-slide-toggle:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n.mat-slide-toggle .mat-ripple-element {\n  background-color: rgba(63, 81, 181, 0.12); }\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #2196f3; }\n.mat-slide-toggle.mat-primary.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(33, 150, 243, 0.5); }\n.mat-slide-toggle.mat-primary:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n.mat-slide-toggle.mat-primary .mat-ripple-element {\n  background-color: rgba(33, 150, 243, 0.12); }\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-thumb {\n  background-color: #e91e63; }\n.mat-slide-toggle.mat-warn.mat-checked:not(.mat-disabled) .mat-slide-toggle-bar {\n  background-color: rgba(233, 30, 99, 0.5); }\n.mat-slide-toggle.mat-warn:not(.mat-checked) .mat-ripple-element {\n  background-color: rgba(0, 0, 0, 0.06); }\n.mat-slide-toggle.mat-warn .mat-ripple-element {\n  background-color: rgba(233, 30, 99, 0.12); }\n.mat-disabled .mat-slide-toggle-thumb {\n  background-color: #bdbdbd; }\n.mat-disabled .mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.1); }\n.mat-slide-toggle-thumb {\n  background-color: #fafafa; }\n.mat-slide-toggle-bar {\n  background-color: rgba(0, 0, 0, 0.38); }\n.mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-primary .mat-slider-track-fill,\n.mat-primary .mat-slider-thumb,\n.mat-primary .mat-slider-thumb-label {\n  background-color: #1e88e5; }\n.mat-primary .mat-slider-thumb-label-text {\n  color: white; }\n.mat-accent .mat-slider-track-fill,\n.mat-accent .mat-slider-thumb,\n.mat-accent .mat-slider-thumb-label {\n  background-color: #3f51b5; }\n.mat-accent .mat-slider-thumb-label-text {\n  color: white; }\n.mat-warn .mat-slider-track-fill,\n.mat-warn .mat-slider-thumb,\n.mat-warn .mat-slider-thumb-label {\n  background-color: #e91e63; }\n.mat-warn .mat-slider-thumb-label-text {\n  color: white; }\n.mat-slider-focus-ring {\n  background-color: rgba(63, 81, 181, 0.2); }\n.mat-slider:hover .mat-slider-track-background,\n.cdk-focused .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.38); }\n.mat-slider-disabled .mat-slider-track-background,\n.mat-slider-disabled .mat-slider-track-fill,\n.mat-slider-disabled .mat-slider-thumb {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-disabled:hover .mat-slider-track-background {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-min-value .mat-slider-focus-ring {\n  background-color: rgba(0, 0, 0, 0.12); }\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.87); }\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb,\n.mat-slider-min-value.mat-slider-thumb-label-showing.cdk-focused .mat-slider-thumb-label {\n  background-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing) .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26);\n  background-color: transparent; }\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-slider-min-value:not(.mat-slider-thumb-label-showing):hover.mat-slider-disabled .mat-slider-thumb, .mat-slider-min-value:not(.mat-slider-thumb-label-showing).cdk-focused.mat-slider-disabled .mat-slider-thumb {\n  border-color: rgba(0, 0, 0, 0.26); }\n.mat-slider-has-ticks .mat-slider-wrapper::after {\n  border-color: rgba(0, 0, 0, 0.7); }\n.mat-slider-horizontal .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent);\n  background-image: -moz-repeating-linear-gradient(0.0001deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n.mat-slider-vertical .mat-slider-ticks {\n  background-image: repeating-linear-gradient(to bottom, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7) 2px, transparent 0, transparent); }\n.mat-step-header.cdk-keyboard-focused, .mat-step-header.cdk-program-focused, .mat-step-header:hover {\n  background-color: rgba(0, 0, 0, 0.04); }\n.mat-step-header .mat-step-label,\n.mat-step-header .mat-step-optional {\n  color: rgba(0, 0, 0, 0.38); }\n.mat-step-header .mat-step-icon {\n  background-color: #1e88e5;\n  color: white; }\n.mat-step-header .mat-step-icon-not-touched {\n  background-color: rgba(0, 0, 0, 0.38);\n  color: white; }\n.mat-step-header .mat-step-label.mat-step-label-active {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-stepper-horizontal, .mat-stepper-vertical {\n  background-color: white; }\n.mat-stepper-vertical-line::before {\n  border-left-color: rgba(0, 0, 0, 0.12); }\n.mat-stepper-horizontal-line {\n  border-top-color: rgba(0, 0, 0, 0.12); }\n.mat-tab-nav-bar,\n.mat-tab-header {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n.mat-tab-group-inverted-header .mat-tab-nav-bar,\n.mat-tab-group-inverted-header .mat-tab-header {\n  border-top: 1px solid rgba(0, 0, 0, 0.12);\n  border-bottom: none; }\n.mat-tab-label, .mat-tab-link {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-tab-label.mat-tab-disabled, .mat-tab-link.mat-tab-disabled {\n    color: rgba(0, 0, 0, 0.38); }\n.mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.87); }\n.mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(0, 0, 0, 0.38); }\n.mat-tab-group[class*='mat-background-'] .mat-tab-header,\n.mat-tab-nav-bar[class*='mat-background-'] {\n  border-bottom: none;\n  border-top: none; }\n.mat-tab-group.mat-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-primary .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-primary .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n.mat-tab-group.mat-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary .mat-ink-bar {\n  background-color: #1e88e5; }\n.mat-tab-group.mat-primary.mat-background-primary .mat-ink-bar, .mat-tab-nav-bar.mat-primary.mat-background-primary .mat-ink-bar {\n  background-color: white; }\n.mat-tab-group.mat-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-accent .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-accent .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(197, 202, 233, 0.3); }\n.mat-tab-group.mat-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent .mat-ink-bar {\n  background-color: #3f51b5; }\n.mat-tab-group.mat-accent.mat-background-accent .mat-ink-bar, .mat-tab-nav-bar.mat-accent.mat-background-accent .mat-ink-bar {\n  background-color: white; }\n.mat-tab-group.mat-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-warn .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-warn .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(248, 187, 208, 0.3); }\n.mat-tab-group.mat-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn .mat-ink-bar {\n  background-color: #e91e63; }\n.mat-tab-group.mat-warn.mat-background-warn .mat-ink-bar, .mat-tab-nav-bar.mat-warn.mat-background-warn .mat-ink-bar {\n  background-color: white; }\n.mat-tab-group.mat-background-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-background-primary .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-background-primary .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-background-primary .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(187, 222, 251, 0.3); }\n.mat-tab-group.mat-background-primary .mat-tab-header, .mat-tab-group.mat-background-primary .mat-tab-links, .mat-tab-nav-bar.mat-background-primary .mat-tab-header, .mat-tab-nav-bar.mat-background-primary .mat-tab-links {\n  background-color: #1e88e5; }\n.mat-tab-group.mat-background-primary .mat-tab-label, .mat-tab-group.mat-background-primary .mat-tab-link, .mat-tab-nav-bar.mat-background-primary .mat-tab-label, .mat-tab-nav-bar.mat-background-primary .mat-tab-link {\n  color: white; }\n.mat-tab-group.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-primary .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-primary .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-chevron {\n  border-color: white; }\n.mat-tab-group.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-primary .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-primary .mat-ripple-element, .mat-tab-nav-bar.mat-background-primary .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n.mat-tab-group.mat-background-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-background-accent .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-background-accent .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-background-accent .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(197, 202, 233, 0.3); }\n.mat-tab-group.mat-background-accent .mat-tab-header, .mat-tab-group.mat-background-accent .mat-tab-links, .mat-tab-nav-bar.mat-background-accent .mat-tab-header, .mat-tab-nav-bar.mat-background-accent .mat-tab-links {\n  background-color: #3f51b5; }\n.mat-tab-group.mat-background-accent .mat-tab-label, .mat-tab-group.mat-background-accent .mat-tab-link, .mat-tab-nav-bar.mat-background-accent .mat-tab-label, .mat-tab-nav-bar.mat-background-accent .mat-tab-link {\n  color: white; }\n.mat-tab-group.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-accent .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-accent .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-chevron {\n  border-color: white; }\n.mat-tab-group.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-accent .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-accent .mat-ripple-element, .mat-tab-nav-bar.mat-background-accent .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n.mat-tab-group.mat-background-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-group.mat-background-warn .mat-tab-link:not(.mat-tab-disabled):focus, .mat-tab-nav-bar.mat-background-warn .mat-tab-label:not(.mat-tab-disabled):focus,\n.mat-tab-nav-bar.mat-background-warn .mat-tab-link:not(.mat-tab-disabled):focus {\n  background-color: rgba(248, 187, 208, 0.3); }\n.mat-tab-group.mat-background-warn .mat-tab-header, .mat-tab-group.mat-background-warn .mat-tab-links, .mat-tab-nav-bar.mat-background-warn .mat-tab-header, .mat-tab-nav-bar.mat-background-warn .mat-tab-links {\n  background-color: #e91e63; }\n.mat-tab-group.mat-background-warn .mat-tab-label, .mat-tab-group.mat-background-warn .mat-tab-link, .mat-tab-nav-bar.mat-background-warn .mat-tab-label, .mat-tab-nav-bar.mat-background-warn .mat-tab-link {\n  color: white; }\n.mat-tab-group.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-group.mat-background-warn .mat-tab-link.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-label.mat-tab-disabled, .mat-tab-nav-bar.mat-background-warn .mat-tab-link.mat-tab-disabled {\n    color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-chevron {\n  border-color: white; }\n.mat-tab-group.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron, .mat-tab-nav-bar.mat-background-warn .mat-tab-header-pagination-disabled .mat-tab-header-pagination-chevron {\n  border-color: rgba(255, 255, 255, 0.4); }\n.mat-tab-group.mat-background-warn .mat-ripple-element, .mat-tab-nav-bar.mat-background-warn .mat-ripple-element {\n  background-color: rgba(255, 255, 255, 0.12); }\n.mat-toolbar {\n  background: whitesmoke;\n  color: rgba(0, 0, 0, 0.87); }\n.mat-toolbar.mat-primary {\n    background: #1e88e5;\n    color: white; }\n.mat-toolbar.mat-accent {\n    background: #3f51b5;\n    color: white; }\n.mat-toolbar.mat-warn {\n    background: #e91e63;\n    color: white; }\n.mat-toolbar .mat-form-field-underline,\n  .mat-toolbar .mat-form-field-ripple,\n  .mat-toolbar .mat-focused .mat-form-field-ripple {\n    background-color: currentColor; }\n.mat-toolbar .mat-form-field-label,\n  .mat-toolbar .mat-focused .mat-form-field-label,\n  .mat-toolbar .mat-select-value,\n  .mat-toolbar .mat-select-arrow,\n  .mat-toolbar .mat-form-field.mat-focused .mat-select-arrow {\n    color: inherit; }\n.mat-toolbar .mat-input-element {\n    caret-color: currentColor; }\n.mat-tooltip {\n  background: rgba(97, 97, 97, 0.9); }\n.mat-tree {\n  background: white; }\n.mat-tree-node {\n  color: rgba(0, 0, 0, 0.87); }\n.mat-snack-bar-container {\n  background: #323232;\n  color: white; }\n.mat-simple-snackbar-action {\n  color: #3f51b5; }\n/*Theme Colors*/\n/*bootstrap Color*/\n/*Light colors*/\n/*Normal Color*/\n/*Extra Variable*/\n/*******************\r\nTimeline page\r\n******************/\n.timeline {\n  position: relative;\n  padding: 20px 0 20px;\n  list-style: none;\n  max-width: 1200px;\n  margin: 0 auto; }\n.timeline:before {\n  content: \" \";\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 50%;\n  width: 3px;\n  margin-left: -1.5px;\n  background-color: #f2f4f8; }\n.timeline > li {\n  position: relative;\n  margin-bottom: 20px; }\n.timeline > li:before,\n.timeline > li:after {\n  content: \" \";\n  display: table; }\n.timeline > li:after {\n  clear: both; }\n.timeline > li:before,\n.timeline > li:after {\n  content: \" \";\n  display: table; }\n.timeline > li:after {\n  clear: both; }\n.timeline > li > .timeline-panel {\n  float: left;\n  position: relative;\n  width: 46%;\n  padding: 20px;\n  border: 1px solid rgba(120, 130, 140, 0.13);\n  border-radius: 4px;\n  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.05); }\n.timeline > li > .timeline-panel:before {\n  content: \" \";\n  display: inline-block;\n  position: absolute;\n  top: 26px;\n  right: -8px;\n  border-top: 8px solid transparent;\n  border-right: 0 solid rgba(120, 130, 140, 0.13);\n  border-bottom: 8px solid transparent;\n  border-left: 8px solid rgba(120, 130, 140, 0.13); }\n.timeline > li > .timeline-panel:after {\n  content: \" \";\n  display: inline-block;\n  position: absolute;\n  top: 27px;\n  right: -7px;\n  border-top: 7px solid transparent;\n  border-right: 0 solid #ffffff;\n  border-bottom: 7px solid transparent;\n  border-left: 7px solid #ffffff; }\n.timeline > li > .timeline-badge {\n  z-index: 10;\n  position: absolute;\n  top: 16px;\n  left: 50%;\n  width: 50px;\n  height: 50px;\n  margin-left: -25px;\n  border-radius: 50% 50% 50% 50%;\n  text-align: center;\n  font-size: 1.4em;\n  line-height: 50px;\n  color: #fff;\n  overflow: hidden; }\n.timeline > li.timeline-inverted > .timeline-panel {\n  float: right; }\n.timeline > li.timeline-inverted > .timeline-panel:before {\n  right: auto;\n  left: -8px;\n  border-right-width: 8px;\n  border-left-width: 0; }\n.timeline > li.timeline-inverted > .timeline-panel:after {\n  right: auto;\n  left: -7px;\n  border-right-width: 7px;\n  border-left-width: 0; }\n.timeline-badge.primary {\n  background-color: #7460ee; }\n.timeline-badge.success {\n  background-color: #26c6da; }\n.timeline-badge.warning {\n  background-color: #ffb22b; }\n.timeline-badge.danger {\n  background-color: #fc4b6c; }\n.timeline-badge.info {\n  background-color: #1e88e5; }\n.timeline-title {\n  margin-top: 0;\n  color: inherit;\n  font-weight: 400; }\n.timeline-body > p,\n.timeline-body > ul {\n  margin-bottom: 0; }\n.timeline-body > p + p {\n  margin-top: 5px; }\n@media (max-width: 767px) {\n  /*Timeline*/\n  ul.timeline:before {\n    left: 40px; }\n  ul.timeline > li > .timeline-panel {\n    width: calc(100% - 90px); }\n  ul.timeline > li > .timeline-badge {\n    top: 16px;\n    left: 15px;\n    margin-left: 0; }\n  ul.timeline > li > .timeline-panel {\n    float: right; }\n  ul.timeline > li > .timeline-panel:before {\n    right: auto;\n    left: -15px;\n    border-right-width: 15px;\n    border-left-width: 0; }\n  ul.timeline > li > .timeline-panel:after {\n    right: auto;\n    left: -14px;\n    border-right-width: 14px;\n    border-left-width: 0; } }\n"

/***/ }),

/***/ "./src/app/pages/timeline/timeline.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/timeline/timeline.component.ts ***!
  \******************************************************/
/*! exports provided: TimelineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimelineComponent", function() { return TimelineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TimelineComponent = /** @class */ (function () {
    function TimelineComponent() {
        this.stacked = false;
    }
    TimelineComponent.prototype.ngOnInit = function () {
    };
    TimelineComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-timeline',
            template: __webpack_require__(/*! ./timeline.component.html */ "./src/app/pages/timeline/timeline.component.html"),
            styles: [__webpack_require__(/*! ./timeline.component.scss */ "./src/app/pages/timeline/timeline.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TimelineComponent);
    return TimelineComponent;
}());



/***/ })

}]);
//# sourceMappingURL=pages-pages-module.js.map