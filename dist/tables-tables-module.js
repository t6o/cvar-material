(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tables-tables-module"],{

/***/ "./src/app/tables/basic-table/basic-table.component.html":
/*!***************************************************************!*\
  !*** ./src/app/tables/basic-table/basic-table.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content> \r\n                <mat-card-title>Basic table</mat-card-title>\r\n                <div class=\"table-rasponsive\">\r\n                <mat-table #table [dataSource]=\"dataSource\">\r\n                    <!--- Note that these columns can be defined in any order.\r\n                          The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n                    <!-- Position Column -->\r\n                    <ng-container matColumnDef=\"position\">\r\n                      <mat-header-cell *matHeaderCellDef> No. </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.position}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Name Column -->\r\n                    <ng-container matColumnDef=\"name\">\r\n                      <mat-header-cell *matHeaderCellDef> Name </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Weight Column -->\r\n                    <ng-container matColumnDef=\"weight\">\r\n                      <mat-header-cell *matHeaderCellDef> Weight </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.weight}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Symbol Column -->\r\n                    <ng-container matColumnDef=\"symbol\">\r\n                      <mat-header-cell *matHeaderCellDef> Symbol </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.symbol}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n                  </mat-table>\r\n                </div>    \r\n            </mat-card-content>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/tables/basic-table/basic-table.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/tables/basic-table/basic-table.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tables/basic-table/basic-table.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/tables/basic-table/basic-table.component.ts ***!
  \*************************************************************/
/*! exports provided: BasicTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicTableComponent", function() { return BasicTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var BasicTableComponent = /** @class */ (function () {
    function BasicTableComponent() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
    }
    BasicTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-basictable',
            template: __webpack_require__(/*! ./basic-table.component.html */ "./src/app/tables/basic-table/basic-table.component.html"),
            styles: [__webpack_require__(/*! ./basic-table.component.scss */ "./src/app/tables/basic-table/basic-table.component.scss")]
        })
    ], BasicTableComponent);
    return BasicTableComponent;
}());

var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
    { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
    { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
    { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
    { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
    { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
    { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
    { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
    { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
    { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
    { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];


/***/ }),

/***/ "./src/app/tables/filterable/filterable.component.html":
/*!*************************************************************!*\
  !*** ./src/app/tables/filterable/filterable.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content> \r\n                <mat-card-title>Filterable table</mat-card-title>\r\n                <div class=\"bg-light p-10 p-r-20 p-l-20\">\r\n                    <mat-form-field>\r\n                  <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Search (Filter)\">\r\n                </mat-form-field>\r\n                </div>\r\n                \r\n                <div class=\"table-rasponsive\">\r\n                    <mat-table #table [dataSource]=\"dataSource\">\r\n                    <!-- Position Column -->\r\n                    <ng-container matColumnDef=\"position\">\r\n                      <mat-header-cell *matHeaderCellDef> No. </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.position}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Name Column -->\r\n                    <ng-container matColumnDef=\"name\">\r\n                      <mat-header-cell *matHeaderCellDef> Name </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Weight Column -->\r\n                    <ng-container matColumnDef=\"weight\">\r\n                      <mat-header-cell *matHeaderCellDef> Weight </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.weight}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Symbol Column -->\r\n                    <ng-container matColumnDef=\"symbol\">\r\n                      <mat-header-cell *matHeaderCellDef> Symbol </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.symbol}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n                  </mat-table>\r\n                </div>    \r\n            </mat-card-content>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/tables/filterable/filterable.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/tables/filterable/filterable.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tables/filterable/filterable.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/tables/filterable/filterable.component.ts ***!
  \***********************************************************/
/*! exports provided: FilterableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterableComponent", function() { return FilterableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var FilterableComponent = /** @class */ (function () {
    function FilterableComponent() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
    }
    FilterableComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    FilterableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-basictable',
            template: __webpack_require__(/*! ./filterable.component.html */ "./src/app/tables/filterable/filterable.component.html"),
            styles: [__webpack_require__(/*! ./filterable.component.scss */ "./src/app/tables/filterable/filterable.component.scss")]
        })
    ], FilterableComponent);
    return FilterableComponent;
}());

var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
    { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
    { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
    { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
    { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
    { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
    { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
    { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
    { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
    { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
    { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];


/***/ }),

/***/ "./src/app/tables/mix/mix.component.html":
/*!***********************************************!*\
  !*** ./src/app/tables/mix/mix.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content> \r\n                <mat-card-title>Data table with sorting, pagination, and filtering.</mat-card-title>\r\n                <div class=\"bg-light p-10 p-r-20 p-l-20\">\r\n                    <mat-form-field>\r\n                        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\r\n                    </mat-form-field>\r\n                </div>    \r\n                <div class=\"table-rasponsive\">\r\n                    <mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n                    <!-- ID Column -->\r\n                    <ng-container matColumnDef=\"id\">\r\n                      <mat-header-cell *matHeaderCellDef mat-sort-header> ID </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let row\"> {{row.id}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Progress Column -->\r\n                    <ng-container matColumnDef=\"progress\">\r\n                      <mat-header-cell *matHeaderCellDef mat-sort-header> Progress </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let row\"> {{row.progress}}% </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Name Column -->\r\n                    <ng-container matColumnDef=\"name\">\r\n                      <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Color Column -->\r\n                    <ng-container matColumnDef=\"color\">\r\n                      <mat-header-cell *matHeaderCellDef mat-sort-header> Color </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let row\" [style.color]=\"row.color\"> {{row.color}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                    <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\r\n                    </mat-row>\r\n                  </mat-table>\r\n\r\n                  <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\r\n                </div>    \r\n            </mat-card-content>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/tables/mix/mix.component.scss":
/*!***********************************************!*\
  !*** ./src/app/tables/mix/mix.component.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tables/mix/mix.component.ts":
/*!*********************************************!*\
  !*** ./src/app/tables/mix/mix.component.ts ***!
  \*********************************************/
/*! exports provided: MixComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MixComponent", function() { return MixComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MixComponent = /** @class */ (function () {
    function MixComponent() {
        this.displayedColumns = ['id', 'name', 'progress', 'color'];
        // Create 100 users
        var users = [];
        for (var i = 1; i <= 100; i++) {
            users.push(createNewUser(i));
        }
        // Assign the data to the data source for the table to render
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](users);
    }
    /**
     * Set the paginator and sort after the view init since this component will
     * be able to query its view for the initialized paginator and sort.
     */
    MixComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    MixComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], MixComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], MixComponent.prototype, "sort", void 0);
    MixComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-mix',
            template: __webpack_require__(/*! ./mix.component.html */ "./src/app/tables/mix/mix.component.html"),
            styles: [__webpack_require__(/*! ./mix.component.scss */ "./src/app/tables/mix/mix.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MixComponent);
    return MixComponent;
}());

/** Builds and returns a new User. */
function createNewUser(id) {
    var name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
        NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
    return {
        id: id.toString(),
        name: name,
        progress: Math.round(Math.random() * 100).toString(),
        color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
    };
}
/** Constants used to fill up our data base. */
var COLORS = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
    'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
var NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];


/***/ }),

/***/ "./src/app/tables/pagination/pagination.component.html":
/*!*************************************************************!*\
  !*** ./src/app/tables/pagination/pagination.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content> \r\n                <mat-card-title>Table with pagination</mat-card-title>\r\n                <div class=\"table-rasponsive\">\r\n                    <mat-table #table [dataSource]=\"dataSource\">\r\n\r\n                    <!-- Position Column -->\r\n                    <ng-container matColumnDef=\"position\">\r\n                      <mat-header-cell *matHeaderCellDef> No. </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.position}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Name Column -->\r\n                    <ng-container matColumnDef=\"name\">\r\n                      <mat-header-cell *matHeaderCellDef> Name </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Weight Column -->\r\n                    <ng-container matColumnDef=\"weight\">\r\n                      <mat-header-cell *matHeaderCellDef> Weight </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.weight}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <!-- Symbol Column -->\r\n                    <ng-container matColumnDef=\"symbol\">\r\n                      <mat-header-cell *matHeaderCellDef> Symbol </mat-header-cell>\r\n                      <mat-cell *matCellDef=\"let element\"> {{element.symbol}} </mat-cell>\r\n                    </ng-container>\r\n\r\n                    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                    <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n                  </mat-table>\r\n\r\n                  <mat-paginator #paginator\r\n                                 [pageSize]=\"10\"\r\n                                 [pageSizeOptions]=\"[5, 10, 20]\">\r\n                  </mat-paginator>\r\n                </div>    \r\n            </mat-card-content>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/tables/pagination/pagination.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/tables/pagination/pagination.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tables/pagination/pagination.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/tables/pagination/pagination.component.ts ***!
  \***********************************************************/
/*! exports provided: PaginationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaginationComponent", function() { return PaginationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaginationComponent = /** @class */ (function () {
    function PaginationComponent() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
    }
    /**
     * Set the paginator after the view init since this component will
     * be able to query its view for the initialized paginator.
     */
    PaginationComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginator"])
    ], PaginationComponent.prototype, "paginator", void 0);
    PaginationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-basictable',
            template: __webpack_require__(/*! ./pagination.component.html */ "./src/app/tables/pagination/pagination.component.html"),
            styles: [__webpack_require__(/*! ./pagination.component.scss */ "./src/app/tables/pagination/pagination.component.scss")]
        })
    ], PaginationComponent);
    return PaginationComponent;
}());

var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
    { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
    { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
    { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
    { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
    { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
    { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
    { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
    { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
    { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
    { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];


/***/ }),

/***/ "./src/app/tables/sortable/sortable.component.html":
/*!*********************************************************!*\
  !*** ./src/app/tables/sortable/sortable.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap=\"wrap\">\r\n    <div fxFlex.gt-sm=\"100\" fxFlex.gt-xs=\"100\" fxFlex=\"100\">\r\n        <mat-card>\r\n            <mat-card-content> \r\n                <mat-card-title>Sortable table</mat-card-title>\r\n                <div class=\"table-rasponsive\">\r\n                    <mat-table #table [dataSource]=\"dataSource\" matSort style=\"max-height: 500px; overflow:auto;\">\r\n\r\n                        <!-- Position Column -->\r\n                        <ng-container matColumnDef=\"position\">\r\n                          <mat-header-cell *matHeaderCellDef mat-sort-header> No. </mat-header-cell>\r\n                          <mat-cell *matCellDef=\"let element\"> {{element.position}} </mat-cell>\r\n                        </ng-container>\r\n\r\n                        <!-- Name Column -->\r\n                        <ng-container matColumnDef=\"name\">\r\n                          <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                          <mat-cell *matCellDef=\"let element\"> {{element.name}} </mat-cell>\r\n                        </ng-container>\r\n\r\n                        <!-- Weight Column -->\r\n                        <ng-container matColumnDef=\"weight\">\r\n                          <mat-header-cell *matHeaderCellDef mat-sort-header> Weight </mat-header-cell>\r\n                          <mat-cell *matCellDef=\"let element\"> {{element.weight}} </mat-cell>\r\n                        </ng-container>\r\n\r\n                        <!-- Symbol Column -->\r\n                        <ng-container matColumnDef=\"symbol\">\r\n                          <mat-header-cell *matHeaderCellDef mat-sort-header> Symbol </mat-header-cell>\r\n                          <mat-cell *matCellDef=\"let element\"> {{element.symbol}} </mat-cell>\r\n                        </ng-container>\r\n\r\n                        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n                      </mat-table>\r\n                </div>    \r\n            </mat-card-content>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/tables/sortable/sortable.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/tables/sortable/sortable.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tables/sortable/sortable.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/tables/sortable/sortable.component.ts ***!
  \*******************************************************/
/*! exports provided: SortableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SortableComponent", function() { return SortableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SortableComponent = /** @class */ (function () {
    function SortableComponent() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
    }
    /**
     * Set the sort after the view init since this component will
     * be able to query its view for the initialized sort.
     */
    SortableComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.sort = this.sort;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], SortableComponent.prototype, "sort", void 0);
    SortableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sortable',
            template: __webpack_require__(/*! ./sortable.component.html */ "./src/app/tables/sortable/sortable.component.html"),
            styles: [__webpack_require__(/*! ./sortable.component.scss */ "./src/app/tables/sortable/sortable.component.scss")]
        })
    ], SortableComponent);
    return SortableComponent;
}());

var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
    { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
    { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
    { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
    { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
    { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
    { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
    { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
    { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
    { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
    { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];


/***/ }),

/***/ "./src/app/tables/tables.module.ts":
/*!*****************************************!*\
  !*** ./src/app/tables/tables.module.ts ***!
  \*****************************************/
/*! exports provided: TablesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablesModule", function() { return TablesModule; });
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _demo_material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../demo-material-module */ "./src/app/demo-material-module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _tables_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tables.routing */ "./src/app/tables/tables.routing.ts");
/* harmony import */ var _basic_table_basic_table_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./basic-table/basic-table.component */ "./src/app/tables/basic-table/basic-table.component.ts");
/* harmony import */ var _filterable_filterable_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./filterable/filterable.component */ "./src/app/tables/filterable/filterable.component.ts");
/* harmony import */ var _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pagination/pagination.component */ "./src/app/tables/pagination/pagination.component.ts");
/* harmony import */ var _sortable_sortable_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sortable/sortable.component */ "./src/app/tables/sortable/sortable.component.ts");
/* harmony import */ var _mix_mix_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./mix/mix.component */ "./src/app/tables/mix/mix.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var TablesModule = /** @class */ (function () {
    function TablesModule() {
    }
    TablesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_tables_routing__WEBPACK_IMPORTED_MODULE_7__["TablesRoutes"]),
                _demo_material_module__WEBPACK_IMPORTED_MODULE_4__["DemoMaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]
            ],
            declarations: [
                _basic_table_basic_table_component__WEBPACK_IMPORTED_MODULE_8__["BasicTableComponent"],
                _filterable_filterable_component__WEBPACK_IMPORTED_MODULE_9__["FilterableComponent"],
                _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_10__["PaginationComponent"],
                _sortable_sortable_component__WEBPACK_IMPORTED_MODULE_11__["SortableComponent"],
                _mix_mix_component__WEBPACK_IMPORTED_MODULE_12__["MixComponent"]
            ],
        })
    ], TablesModule);
    return TablesModule;
}());



/***/ }),

/***/ "./src/app/tables/tables.routing.ts":
/*!******************************************!*\
  !*** ./src/app/tables/tables.routing.ts ***!
  \******************************************/
/*! exports provided: TablesRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablesRoutes", function() { return TablesRoutes; });
/* harmony import */ var _basic_table_basic_table_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic-table/basic-table.component */ "./src/app/tables/basic-table/basic-table.component.ts");
/* harmony import */ var _filterable_filterable_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filterable/filterable.component */ "./src/app/tables/filterable/filterable.component.ts");
/* harmony import */ var _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pagination/pagination.component */ "./src/app/tables/pagination/pagination.component.ts");
/* harmony import */ var _sortable_sortable_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sortable/sortable.component */ "./src/app/tables/sortable/sortable.component.ts");
/* harmony import */ var _mix_mix_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./mix/mix.component */ "./src/app/tables/mix/mix.component.ts");





var TablesRoutes = [
    {
        path: '',
        children: [{
                path: 'basictable',
                component: _basic_table_basic_table_component__WEBPACK_IMPORTED_MODULE_0__["BasicTableComponent"]
            }, {
                path: 'filterable',
                component: _filterable_filterable_component__WEBPACK_IMPORTED_MODULE_1__["FilterableComponent"]
            }, {
                path: 'pagination',
                component: _pagination_pagination_component__WEBPACK_IMPORTED_MODULE_2__["PaginationComponent"]
            }, {
                path: 'sortable',
                component: _sortable_sortable_component__WEBPACK_IMPORTED_MODULE_3__["SortableComponent"]
            }, {
                path: 'mix',
                component: _mix_mix_component__WEBPACK_IMPORTED_MODULE_4__["MixComponent"]
            }]
    }
];


/***/ })

}]);
//# sourceMappingURL=tables-tables-module.js.map